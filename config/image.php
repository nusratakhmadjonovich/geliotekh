<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    //agar faqat bir tomonlama resize xoxlasa 2-si null qilishi kerak
    'upload_photos_params' => [
        'profile' =>[
            'original' => '',
            '32x32' => [
                'type' => 'fit',
                'width' => 32,
                'height' => 32
            ],
            '200x200' => [
                'type' => 'fit',
                'width' => 200,
                'height' => 200
            ]
        ],

        'journal_edition' =>[
            'original' => ''
        ],

        'journal_edition_article' =>[
            'original' => '',
            '150x84' => [
                'type' => 'fit',
                'width' => 150,
                'height' => 84
            ]
        ],

        'categories'=>[
            'original' => ''
        ],

        'blog_post'=>[
            'original'=>''
        ]
    ]

];
