-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 07 2018 г., 09:06
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `geliotekhnika`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_article_id` int(11) NOT NULL,
  `journal_edition` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `article_categories`
--

CREATE TABLE `article_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `name_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `article_categories`
--

INSERT INTO `article_categories` (`id`, `status`, `name_ru`, `name_en`, `description_ru`, `description_en`, `photo`, `created_at`, `created_by`, `updated_at`, `updated_by`, `is_deleted`, `deleted_at`, `deleted_by`) VALUES
(1, 1, 'Гелиоустановки, их применение.', 'Гелиоустановки, их применение.', 'Методика и результаты расчетов или измерений солнечной радиации применительно к различным гелиотехническим устройствам.', 'Методика и результаты расчетов или измерений солнечной радиации применительно к различным гелиотехническим устройствам.', 'fe71ba542b73364af601a2e8e263348288601d04.png', '2018-05-05 11:36:33', NULL, '2018-05-05 12:09:38', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `article_reviewer`
--

CREATE TABLE `article_reviewer` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `blocks`
--

CREATE TABLE `blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `block_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block_data_ru` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `block_data_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blocks`
--

INSERT INTO `blocks` (`id`, `block_key`, `block_data_ru`, `block_data_en`, `status`) VALUES
(1, 'footer_1', '<h6>Адрес</h6>\r\n<p class=\"footer_address\">\r\n    ул. Яхё Гулямов 77\r\n</p>\r\n<p class=\"footer_address\">\r\n    г.Ташкент, Узбекистан\r\n</p>', '<h6>Адрес</h6>\r\n<p class=\"footer_address\">\r\n    ул. Яхё Гулямов 77\r\n</p>\r\n<p class=\"footer_address\">\r\n    г.Ташкент, Узбекистан\r\n</p>', 1),
(2, 'footer_2', '<h6>Контакт</h6>\r\n<p class=\"footer_address \">\r\n    <i class=\"fas fa-phone\"></i>   +998 97 777 77 77\r\n</p>\r\n<p class=\"footer_address\">\r\n    <i class=\"fas fa-fax\"></i>   +998 71 277 07 07\r\n</p>\r\n<p class=\"footer_address\">\r\n    <i class=\"fas fa-envelope\"></i>   geliotecnika@gmail.com\r\n</p>\r\n<div class=\"social_set\">\r\n<ul class=\"list-unstyled\">\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_facebook.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_twitter.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_rss.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_message.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n</ul>\r\n</div>', '<h6>Контакт</h6>\r\n<p class=\"footer_address \">\r\n    <i class=\"fas fa-phone\"></i>   +998 97 777 77 77\r\n</p>\r\n<p class=\"footer_address\">\r\n    <i class=\"fas fa-fax\"></i>   +998 71 277 07 07\r\n</p>\r\n<p class=\"footer_address\">\r\n    <i class=\"fas fa-envelope\"></i>   geliotecnika@gmail.com\r\n</p>\r\n<div class=\"social_set\">\r\n<ul class=\"list-unstyled\">\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_facebook.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_twitter.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_rss.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n    <li class=\"d-inline\">\r\n        <a href=\"#\">\r\n            <img src=\"app/dist/icons/ic_message.png\" alt=\"\" title=\"\">\r\n        </a>\r\n    </li>\r\n</ul>\r\n</div>', 1),
(3, 'footer_3', '<h6>Издатель</h6>\r\n<p class=\"footer_address\">\r\n    Международный Научный журнал\r\n</p>\r\n<p class=\"footer_address\">\r\n    Геиотехника. ISSN 0130-0997\r\n</p>\r\n<p class=\"footer_address\">\r\n    Главный редактор: Фамилия Имя Отчество\r\n</p>', '<h6>Издатель</h6>\r\n<p class=\"footer_address\">\r\n    Международный Научный журнал\r\n</p>\r\n<p class=\"footer_address\">\r\n    Геиотехника. ISSN 0130-0997\r\n</p>\r\n<p class=\"footer_address\">\r\n    Главный редактор: Фамилия Имя Отчество\r\n</p>', 1),
(4, 'footer_copyright', '© 2018 Академия Наук Республика Узбекистан Научный международный журнал “Гелиотехника”', '© 2018 Академия Наук Республика Узбекистан Научный международный журнал “Гелиотехника”', 1),
(5, '', '', '', -1),
(6, 'footer_bottom', 'Журнал зарегистрирован в Республике Узбекистан в сфере гелиотехники.  Свидетельство ПИ № ФС77-28742 от 5 июля 2007 года', 'Журнал зарегистрирован в Республике Узбекистан в сфере гелиотехники.  Свидетельство ПИ № ФС77-28742 от 5 июля 2007 года', 1),
(7, 'slider_1', '<h2>Международный научный журнал</h2>\r\n<h1>Гелиотехника</h1>\r\n<p>ISSN 0130-0997</p>', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ru` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blog_category`
--

INSERT INTO `blog_category` (`id`, `slug`, `name_ru`, `name_en`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'news', 'Новосты', 'News', 1, NULL, '2018-05-05 11:12:28', '2018-05-05 11:12:57'),
(2, 'events', 'МЕРОПРИЯТИЯ', 'EVENTS', 1, NULL, '2018-05-06 04:05:33', '2018-05-06 04:05:52');

-- --------------------------------------------------------

--
-- Структура таблицы `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anons_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `anons_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ru` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `publish_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `title_ru`, `title_en`, `anons_ru`, `anons_en`, `description_ru`, `description_en`, `category_id`, `photo`, `status`, `publish_at`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '«Гелиотехника» (русская версия) в базу данных East View', '', 'В настоящий момент идет процесс включения международного научного журнала «Гелиотехника» (русская версия) в базу данных East View (одна из самых больших коллекций электронных изданий, полнотекстовых и библиографических баз данных в России).', '', '<p>В настоящий момент идет процесс включения международного научного журнала &laquo;Гелиотехника&raquo; (русская версия) в базу данных East View (одна из самых больших коллекций электронных изданий, полнотекстовых и библиографических баз данных в России).</p>', '', 1, 'd068e9b7764553010142fa7c7a3ebfe9bbebc584.jpg', 1, '2018-05-06 02:18:00', NULL, '2018-05-05 11:14:44', '2018-05-06 02:18:18'),
(2, 'Издания в электронную библиотеку (базу данных) Узбекистана.', '', 'С компанией ООО «E-LINE PRESS» подписан вусторонний договор о включении издания в электронную библиотеку (базу данных) Узбекистана.', '', '<p>С компанией ООО &laquo;E-LINE PRESS&raquo; подписан вусторонний договор о включении издания в электронную библиотеку (базу данных) Узбекистана.</p>', '', 1, '3ff1df09fe6b5bfb291ab179fa088ce1e533fc3a.jpg', 1, '2018-05-06 02:16:00', NULL, '2018-05-06 00:14:55', '2018-05-06 02:17:52'),
(3, 'Экономика и экология гелиотехники, экология', '', 'Узэкспоцентр', '', '', '', 2, '', 1, '2018-05-06 08:07:00', NULL, '2018-05-06 04:06:33', '2018-05-06 08:08:08');

-- --------------------------------------------------------

--
-- Структура таблицы `editors_teams`
--

CREATE TABLE `editors_teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `fio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `gallerys`
--

CREATE TABLE `gallerys` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `seo_title` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(170) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `journals`
--

CREATE TABLE `journals` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `journals`
--

INSERT INTO `journals` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, '', -1, '2018-05-05 11:49:01', '2018-05-05 11:49:01'),
(2, 'Гелиотехника', 1, '2018-05-05 11:52:07', '2018-05-05 11:52:15');

-- --------------------------------------------------------

--
-- Структура таблицы `journal_editions`
--

CREATE TABLE `journal_editions` (
  `id` int(10) UNSIGNED NOT NULL,
  `journal_id` int(10) UNSIGNED NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `description_ru` longtext COLLATE utf8mb4_unicode_ci,
  `description_en` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiving_articles` date DEFAULT NULL,
  `placement_electronic_monographs` date DEFAULT NULL,
  `distribution_print_copies_monographs` date DEFAULT NULL,
  `mailing_editions` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `journal_editions`
--

INSERT INTO `journal_editions` (`id`, `journal_id`, `year`, `number`, `description_ru`, `description_en`, `file`, `photo`, `receiving_articles`, `placement_electronic_monographs`, `distribution_print_copies_monographs`, `mailing_editions`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 2, '2017', 1, NULL, NULL, NULL, 'a58fb3cfbab1ff34dda20e2d66e1a28bc4a06f50.png', NULL, NULL, NULL, NULL, 1, 0, '2018-05-05 11:52:43', '2018-05-05 11:53:04'),
(2, 2, '2017', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2018-05-05 11:53:30', '2018-05-05 11:55:30'),
(3, 2, '2018', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2018-05-05 11:53:46', '2018-05-05 11:56:59'),
(4, 2, '2017', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2018-05-05 11:53:59', '2018-05-05 11:54:06');

-- --------------------------------------------------------

--
-- Структура таблицы `journal_edition_articles`
--

CREATE TABLE `journal_edition_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `journal_edition_id` int(11) NOT NULL,
  `article_category_id` int(11) NOT NULL,
  `title_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ru` longtext COLLATE utf8mb4_unicode_ci,
  `description_en` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_chargerable` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Main', '2018-05-05 11:34:39', '2018-05-05 11:34:39');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `label_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_ru` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` int(10) UNSIGNED NOT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `label_en`, `label_ru`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'Главная', '/', 0, 0, NULL, 1, 0, '2018-05-05 11:35:23', '2018-05-06 05:05:31'),
(2, 'About', 'О журнале', 'page/2', 0, 1, NULL, 1, 0, '2018-05-06 05:04:04', '2018-05-06 06:36:10'),
(3, 'Reviewers', 'Рецензенты', 'page/9', 0, 5, NULL, 1, 0, '2018-05-06 05:04:36', '2018-05-06 12:07:15'),
(5, 'Authors', 'Авторам', NULL, 0, 2, NULL, 1, 0, '2018-05-06 05:07:22', '2018-05-06 06:40:45'),
(6, 'Contacts', 'Контакты', NULL, 0, 6, NULL, 1, 0, '2018-05-06 05:07:35', '2018-05-06 12:07:51'),
(7, 'Eng', 'Правила', 'page/8', 5, 3, NULL, 1, 1, '2018-05-06 05:08:20', '2018-05-06 07:04:02'),
(8, 'Indicators', 'Показатели', 'page/10', 5, 4, NULL, 1, 1, '2018-05-06 12:07:07', '2018-05-06 12:07:15');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_022230_create_blog_category_table', 1),
(4, '2016_06_01_022325_create_blog_posts_table', 1),
(5, '2016_06_02_001721_create_pages_contents_table', 1),
(6, '2016_06_14_003311_create_gallerys_table', 1),
(7, '2017_08_11_073824_create_menus_wp_table', 1),
(8, '2017_08_11_074006_create_menu_items_wp_table', 1),
(9, '2018_02_09_182939_create_roles_table', 1),
(10, '2018_02_25_071735_create_articles_table', 1),
(11, '2018_03_05_063010_create_rbac_groups', 1),
(12, '2018_03_05_063043_create_rbac_permissions', 1),
(13, '2018_03_05_063221_create_rbac_users_groups_rel', 1),
(14, '2018_03_05_063236_create_rbac_groups_permissions_rel', 1),
(15, '2018_03_05_063443_create_user_article_authors', 1),
(16, '2018_03_05_063455_create_user_articles', 1),
(17, '2018_03_05_063637_create_article_categories', 1),
(18, '2018_03_08_095541_add_role_fk_to_users', 1),
(19, '2018_03_14_223335_create_user_article_meta', 1),
(20, '2018_03_15_180423_create_journals_table', 1),
(21, '2018_03_16_193347_create_journal_edition_table', 1),
(22, '2018_03_23_170722_create_editors_teams', 1),
(23, '2018_03_26_013241_create_journal_edition_articles', 1),
(24, '2018_04_01_110454_create_article_reviewer_table', 1),
(25, '2018_04_01_112037_create_reviewer_article_table', 1),
(26, '2018_04_10_070939_create_reviewer_response_table', 1),
(27, '2018_04_22_181053_create_blocks_table', 1),
(28, '2018_04_24_044417_create_settings_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ru` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `publish_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title_ru`, `title_en`, `description_ru`, `description_en`, `status`, `publish_at`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', -1, '0000-00-00 00:00:00', NULL, '2018-05-05 10:57:42', '2018-05-05 10:57:42'),
(2, 'О журнале', 'About the Journal', '<p style=\"text-align:justify\">Учредителем международного научного журнала<strong> &laquo;Гелиотехника&raquo;</strong> является Академия Наук Республики Узбекистан. Журнал издается с 1965 г., перерегистрирован 12 декабря 2006 г. Агентством информации и связи Республики Узбекистан (сертификат №0077 и №004780-10). Английская версия журнала <strong>&laquo;</strong><strong>Applied Solar Energy&raquo;</strong> (ISSN 1934-9424).</p>\r\n\r\n<p style=\"text-align:justify\">Журнал <strong>&laquo;Гелиотехника&raquo;</strong> является одним из известных в СНГ научным журналом в области солнечной энергетики (СЭ) и других возобновляемых источников энергии (ВИЭ). Основное назначение журнала заключается, прежде всего, в публикации сообщений о крупных научных исследованиях, имеющих приоритетный характер по различным аспектам СЭ и широкого спектра проблем ВИЭ в целом.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Язык издания</strong>. Русский и английский.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Цель и задачи журнала.</strong> Ознакомление широкого круга научных сотрудников, специалистов, научных работников, магистров и докторантов научно-исследовательских институтов, а также ученых в области естественных наук информациями о последних разработках, достижений и новостей в области использования солнечной энергии и других ВИЭ.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Специальность</strong>. В журнале публикуются оригинальные научные статьи, в области использования солнечной энергии и других возобновляемых источников энергии.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Уникальные особенности журнала. </strong>Журнал охватывает все основные направления исследований и разработок по использованию солнечной энергии: Прямое преобразование солнечной энергии в электрическую энергию; Гелиоустановки и их применение; Концентраторы солнечной энергии; Солнечные электростанции; Гелиотехническое материаловедение; Солнечное тепло &ndash; хладоснабжение, аккумулирование; Солнечная радиация; Возобновляемые источники энергии; Микро гелиоэнергетика; Экономика и экология гелиотехники; Краткие сообщения; Новости гелиотехники (хроника); Некролог.</p>\r\n\r\n<p style=\"text-align:justify\">Практикуется публикация специальных выпусков и обзоров, посвященных отдельным научно-техническим проблемам по тематике журнала, а также материалов международных конгрессов, симпозиумов и конференций.</p>\r\n\r\n<p style=\"text-align:justify\">В области <strong><em>солнечной энергетики</em></strong> журнал является <strong><em>основным узбекским научным изданием</em></strong>, в котором публикуются статьи соискателей ученых степеней по техническим наукам.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Регионы распространения</strong> &ndash; в Республике Узбекистан, в США издательством <strong><em>Allerton Press Inс</em></strong>. переиздается на английском языке и распространяется в более 30-х странах мира.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Источники финансирования </strong>&ndash; за счет подписки в Республике и средств <em><strong>Allerton Press Inс.</strong></em></p>\r\n\r\n<p style=\"text-align:justify\">В 2017 году компанией Clarivate Analytics, журнал<strong> &laquo;</strong><strong>Applied Solar Energy&raquo; </strong>был признан лучшим научным журналом<strong> </strong>Средней Азии и получил награду <strong>Web</strong><strong> </strong><strong>of</strong><strong> </strong><strong>Science</strong><strong> </strong><strong>Awards</strong><strong> 2017</strong>.</p>', '<p style=\"text-align:justify\">The founder of the international scientific journal <strong>Geliotekhnika</strong> is the Academy of Sciences of the Republic of Uzbekistan. The journal has been published since 1965, re-registered on December 12, 2006 by the Information and Communication Agency of the Republic of Uzbekistan (Certificate No. 0077 and No. 004780-10). The English version of the journal &quot;<strong>Applied Solar Energy</strong>&quot; (ISSN 1934-9424).</p>\r\n\r\n<p style=\"text-align:justify\">The journal &quot;<strong>Geliotekhnika</strong>&quot; is one of the well-known in the CIS scientific journal in the field of solar energy (SE) and other renewable energy sources (RES). The main purpose of the journal is, first of all, in the publication of reports on major scientific research, which have priority in various aspects of SE and a wide range of RES problems in general.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Language of publication.</strong> Russian and English.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>The purpose and objectives of the journal.</strong> Familiarization of a wide range of scientific workers, specialists, scientists, masters and doctoral students of scientific research institutes, as well as scientists in the field of natural sciences with information on the latest developments, achievements and news in the field of solar energy and other RES.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Specialty.</strong> The journal publishes original scientific articles on the use of solar energy and other renewable energy sources.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Unique features of the journal.</strong> The journal covers all the main areas of research and development on the use of solar energy: Direct conversion of solar energy into electrical energy; Solar installations and their application; Concentrators of solar energy; Solar power plants; Solar engineering materials science; Solar heat - cold supply; Solar radiation; Renewable energy sources; Micro solar power engineering; Economics and ecology of solar engineering; Brief communications; News of solar engineering (chronicle); Obituary.</p>\r\n\r\n<p style=\"text-align:justify\">The publication of special issues and reviews devoted to individual scientific and technical problems on the subject of the journal, as well as materials of international congresses, symposiums and conferences is practiced.</p>\r\n\r\n<p style=\"text-align:justify\">In the field of solar energy, the journal is the main Uzbek scientific publication, which publishes articles by applicants for academic degrees in engineering.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Regions of distribution</strong> - in the Republic of Uzbekistan, in the USA by <strong><em>Allerton Press In.</em></strong> is reprinted in English and distributed in more than 45 countries of the world.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Sources of funding</strong> - through subscription in the Republic and funds <strong><em>Allerton Press In</em></strong>.</p>\r\n\r\n<p style=\"text-align:justify\">In 2017, the journal &quot;<strong>Applied Solar Energy</strong>&quot; was recognized by Clarivate Analytics, as the best scientific journal of Central Asia and was awarded the <strong>Web of Science Awards 2017</strong>.</p>', 1, '2018-05-06 16:52:00', NULL, '2018-05-05 10:58:23', '2018-05-06 11:52:52'),
(3, 'wadwad', 'dawdwad', '<p>awdwad</p>', '<p>awdwa</p>', 1, '2018-05-05 16:10:00', '2018-05-05 11:11:30', '2018-05-05 11:10:28', '2018-05-05 11:11:30'),
(4, '', '', '', '', -1, '0000-00-00 00:00:00', NULL, '2018-05-06 03:55:43', '2018-05-06 03:55:43'),
(5, 'Опубликовать статью', 'Publish article', '', '', 1, '2018-05-06 08:56:00', NULL, '2018-05-06 03:56:13', '2018-05-06 03:56:38'),
(6, 'Индекситирование', 'Индекситирование', '', '', 1, '2018-05-06 08:56:00', NULL, '2018-05-06 03:56:47', '2018-05-06 03:56:54'),
(7, '', '', '', '', -1, '0000-00-00 00:00:00', NULL, '2018-05-06 07:00:40', '2018-05-06 07:00:40'),
(8, 'Правила', 'Rules', '<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>Правила приема и оформления статей для международного журнала &laquo;гелиотехника&raquo;</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">&laquo;Гелиотехника&raquo; &mdash; международный научный журнал, в котором публикуются последние достижения науки и техники в следующих областях.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>1.</strong><strong> Солнечная радиация</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Методика и результаты расчетов или измерений солнечной радиации применительно к различным гелиотехническим устройствам.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>2.</strong><strong> Гелиоустановки и их применение</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Реализованные в натуре гелиотехнические установки различного назначения, как экспериментального, так и практического характера, результаты измерений их параметров или расчетов. Характеристики выпускаемого гелиотехнического оборудования, рекомендации по его применению. Установки и системы, имеющие реальное практическое применение, их характеристики.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>3.</strong><strong> Солнечные электростанции</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Больших мощностей, основанные на различных принципах преобразования солнечной энергии в электрическую.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>4.</strong><strong> Прямое преобразование солнечной энергии в электрическую с помощью фото- и термоэлектрических преобразований. </strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Выходные параметры и характеристики фото- и термоэлементов, батарей и установок на их основе. Методы измерений параметров и расчетные способы определения их характеристик.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>5.</strong><strong> Концентраторы солнечной энергии</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Методы расчета, опытно-конструкторские и технологические работы по созданию концентраторов, методы и результаты измерения характеристик, применение концентраторов.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>6. </strong><strong>Солнечные тепло- и хладоснабжение, аккумулирование</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Солнечные коллекторы, установки и системы солнечного тепло- и хладоснабжения, аккумулирования; методы и результаты их рас&shy;чета, испытаний и оптимизации; применение коллекторов, установок и систем.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>7. </strong><strong>Гелиотехническое материаловедение</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Получение и использование новых материалов в гелиотехнике, исследование свойств материалов с помощью солнечных установок, в том числе солнечных печей.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>8. </strong><strong>Возобновляемые источники энергии</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Нетрадиционные, альтернативные источники энергии, включая, солнечные, ветровые, гидравлические, биомассы, термальные, а также комбинированные.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>9. </strong><strong>Экономика и экология гелиотехники</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Технико-экономические характеристики солнечных установок, экологическая оценка использования солнечной энергии.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>10. </strong><strong>Краткие сообщения</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Краткие статьи и сообщения по отдельным вопросам использования солнечной энергии.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>11.</strong><strong> Новости гелиотехники (хроника)</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Сообщения о конференциях, семинарах и симпозиумах по использованию солнечной энергии, их результатах, о нормативных документах и новой литературе в области гелиотехники, а также о новых созданных крупных установках и экспериментальных стендах.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>Не принимаются:</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">а) статьи по фото- и термоэлектрической тематике, касающиеся исключительно вопросов физики твердого тела и полупроводниковой технологии;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">б) статьи о материалах, полученных с помощью солнечных установок, относящиеся исключительно к области материаловедения. Если исследования выполнены на солнечной или дуговой отражательных печах, то должна быть конкретизована специфика данного способа обогрева, измерения и регулирования температуры.</p>\r\n\r\n<p style=\"margin-right:-0.05pt; text-align:justify\">&nbsp;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\"><strong>Статьи, направленные в редакцию журнала &laquo;Гелиотехника&raquo;, должны соответствовать указанной выше тематике и отвечать следующим требованиям.</strong></p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">1. Статья должна быть написана на русском или английском языках объемом не боле 6 страниц машинописного текста формат А4, научные сообщения &ndash; 1-2 страницы, набранные в текстовом редакторе Microsoft Word в формате .doc или .docx, Times New Roman, кегль 12 pt, выравнивание по ширине страницы.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Текст печатается на одной стороне страницы через 1-1,5 интервала с полями сверху, снизу и с левой стороны не менее 2,5 см, с правой &ndash; 1см, страницы нумеруются единой нумерацией, включая таблицы и литературу, которые размещаются в соответствующих местах текста.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">2. В тексте необходимо давать ссылки на таблицы, рисунки и литературные источники с соответствующей нумерацией. Таблицы и её графы должны иметь краткие заголовки, написанные полностью, без сокращений.&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">3. Рисунки должны быть нарисованы (или сканированы) в графическом редакторе Word с группированием, а также иметь номер и названия, расположенные ниже поля рисунка.&nbsp;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">4. Формулы должны быть тщательно выверены и пронумерованы в порядке их появления в тексте.&nbsp;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">5. Размерность всех приведенных величин должна соответствовать международной системе единиц измерений.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">6. На первой странице статьи указываются УДК (вверху слева), затем заглавными буквами жирным шрифтом &ndash; инициалы и фамилия автора/авторов (справа) и далее название статьи жирным шрифтом (точка в конце не ставится) и затем после 2-х интервалов &ndash; аннотации косым шрифтом не более 7 строк, далее после 2-х интервалов печатается текст статьи обычным шрифтом.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">7. В конце статьи приводится используемая автором/авторами литература. Источники литературы располагаются подряд с указанием номера в квадратных скобках, далее указывается ФИО (фамилия и инициалы), затем всё через разделительные точки: - названия журнала (монографии, учебника, сборника, конференции (город, страна, дата), автореферата диссертации, патента и т.д.), - год, - том, - номер/номера страниц (или их количество для монографий, сборников и т.д.).&nbsp;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">8. После списка литературы указываются институт или организация (название полностью), представившие статью (в левом углу) и дата представления (в правом углу).</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">9. На отдельной странице указываются полные сведения об авторе/авторах, включая фамилию, имя, отчество, ученую степень и звания, место работы и должность, а также контактные адрес и телефоны автора.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">10. Рукопись статьи должна быть подписана всеми авторами и представлена в укомплектованном виде: иметь направление от учреждения, в котором выполнена работа, акт экспертизы (1 экз.), оформленный в соответствии с существующими требованиями (для РУз и стран СНГ). В редакцию журнала сдаются 2 экз. статьи твердой копии и электронный вариант.&nbsp;</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">11. Статьи следует отправлять по адресу:</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Узбекистан, 100084, г. Ташкент, ул. Бадамзар Йули 2Б, Физико-технический институт АН РУз</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">Тел.: (+99871) 233-12-71; e-mail: avezov@uzsci.net</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">12. Статьи, не отвечающие данным требованиям, редколлегией не рассматриваются и авторам не возвращаются.</p>\r\n\r\n<p class=\"MsoPlainText\" style=\"margin-right:-0.05pt; text-align:justify\">13. Отдельные оттиски авторам не выдаются; автор может заказать в редакции необходимое количество экземпляров журнала за месяц до выхода его очередного номера.</p>', '<p><strong>Rules of admission and submission of articles for the international journal of &quot;Applied Solar Energy&quot;</strong></p>\r\n\r\n<p>&quot;Applied Solar Energy&quot; is an international scientific journal in which the latest achievements of science and technology are published in the following fields.</p>\r\n\r\n<p><strong>1. Solar radiation</strong></p>\r\n\r\n<p>Method and results of calculations or measurements of solar radiation in relation to various solar engineering installations.</p>\r\n\r\n<p><strong>2. Solar installations and their application</strong></p>\r\n\r\n<p>Realized in nature solar engineering installations for various purposes, both experimental and practical, the results of measurements of their parameters or calculations. Characteristics of the released solar equipment, recommendations for its use. Installations and systems that have real practical application, their characteristics.</p>\r\n\r\n<p><strong>3. Solar power plants</strong></p>\r\n\r\n<p>Large capacity, based on various principles of converting solar energy into electricity.</p>\r\n\r\n<p><strong>4. Direct conversion of solar energy into electrical energy by means of photo- and thermoelectric transformations.</strong></p>\r\n\r\n<p>Output parameters and characteristics of photo- and thermoelements, batteries and installations based on them. Methods for measuring parameters and calculation methods for determining their characteristics.</p>\r\n\r\n<p><strong>5. Solar energy concentrators</strong></p>\r\n\r\n<p>Calculation methods, development and technological work on the creation of concentrators, methods and characteristics measurement results, concentrators application.</p>\r\n\r\n<p><strong>6. Solar heat and cold supply</strong></p>\r\n\r\n<p>Solar collectors, installations and systems of solar heat- and cold supply; methods and results of their calculation, testing and optimization; application of collectors, installations and systems.</p>\r\n\r\n<p><strong>7. Solar engineering materials science</strong></p>\r\n\r\n<p>Receiving and using new materials in solar engineering, researching the properties of materials with the help of solar installations, including solar furnaces.</p>\r\n\r\n<p><strong>8. Renewable energy sources</strong></p>\r\n\r\n<p>Non-traditional, alternative energy sources, including, solar, wind, hydraulic, biomass, thermal, and also combined.</p>\r\n\r\n<p><strong>9. Economics and ecology of solar engineering</strong></p>\r\n\r\n<p>Technical and economic characteristics of solar installations, environmental assessment of the use of solar energy.</p>\r\n\r\n<p><strong>10. Brief communications</strong></p>\r\n\r\n<p>Brief articles and reports on selected issues related to the use of solar energy.</p>\r\n\r\n<p><strong>11. News of solar engineering (chronicle)</strong></p>\r\n\r\n<p>Reports on conferences, seminars and symposia on the use of solar energy, their results, on regulatory documents and new literature in the field of solar engineering, as well as on new created large installations and experimental stands.</p>\r\n\r\n<p><strong>Not accepted:</strong></p>\r\n\r\n<p>a) articles on photo and thermoelectric topics, dealing exclusively with solid-state physics and semiconductor technology;</p>\r\n\r\n<p>b) articles on materials obtained with the help of solar installations, which relate exclusively to the field of materials science. If the studies are performed on solar or arc-reflecting furnaces, the specifics of this method of heating, measuring and controlling the temperature must be specified.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The articles sent to the editorial office of the journal &ldquo;Applied Solar Energy&rdquo; must comply with the above topics and meet the following requirements.</strong></p>\r\n\r\n<p>1. The article should be written in Russian or English languages with a maximum of 14 pages of typewritten text A4 format, scientific reports - 1-2 pages, typed in a Microsoft Word text editor in .doc or .docx format, Times New Roman, 12 pt size, alignment to the width of the page.</p>\r\n\r\n<p>The text is printed on one side of the page after 1-1.5 intervals with margins at the top, bottom and left sides not less than 2.5 sm, from the right - 1 sm, pages are numbered in a single numbering, including tables and literature, which are placed in the appropriate places in the text.</p>\r\n\r\n<p>2. In the text it is necessary to give references to tables, figures and references with the corresponding numbering. Tables and its graphs should have short headers written entirely, without abbreviations.</p>\r\n\r\n<p>3. Drawings should be drawn (or scanned) in the Word grouped graphical editor, and also have a number and names located below the picture field.</p>\r\n\r\n<p>4. Formulas must be carefully verified and numbered in the order in which they appear in the text.</p>\r\n\r\n<p>5. The dimensionality of all the given values should correspond to the international system of measurements units.</p>\r\n\r\n<p>6. On the first page of the article, in capital letters are indicated, the initials and surname of the author / authors (right) and then the title of the article in bold type (no point at the end) and then after 2 intervals - abstract oblique font no more than 7 lines, then after 2 intervals the text of the article is printed in normal type.</p>\r\n\r\n<p>7. At the end of the article is given the literature used by the author/authors. References are arranged in a row with the number in square brackets, followed by the full name (surname and initials), then all through the separating points: - the title of the journal (monograph, textbook, collection, conference (city, country, date), thesis abstract, patent and etc.), - year, - volume, - issue/number of pages (or their number for monographs, collections, etc.).</p>\r\n\r\n<p>8. After the list of references are indicated the institution or organization (the full name) that submitted the article (in the left corner) and the date of submission (in the right corner).</p>\r\n\r\n<p>9. On a separate page are indicated the full information about the author/authors, including the last name, first name, middle name, academic degree and rank, place of work and position, as well as contact addresses and phone numbers of the author.</p>\r\n\r\n<p>10. Articles should be sent to:</p>\r\n\r\n<p>Uzbekistan, 100084, Tashkent, Badamzar Yuli 2B str., Physical-Technical Institute of the Academy of Sciences of the Republic of Uzbekistan</p>\r\n\r\n<p>Tel.: (+99871) 233-42-42; e-mail: avezov@uzsci.net</p>\r\n\r\n<p>12. Articles that do not meet these requirements, the editorial board are not considered and authors are not returned.</p>\r\n\r\n<p>13. Separate reprints of the authors are not issued; the author can order the necessary number of copies of the journal in the editorial office a month before the publication of its next issue.</p>', 1, '2018-05-06 16:56:00', NULL, '2018-05-06 07:03:04', '2018-05-06 12:00:19'),
(9, 'Рецензенты', 'Reviewers', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"table\">\r\n	<thead>\r\n		<tr>\r\n			<th>Местные рецензенты</th>\r\n			<th>Зарубежные рецензенты</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>А.С. Саидов</td>\r\n			<td>О.С. Попель (ОИВТ РАН, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Р.Р. Авезов</td>\r\n			<td>С.Е. Фрид (ОИВТ РАН, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>У.Х. Газиев</td>\r\n			<td>С.Н. Трушевский (ФГБНУ ВИЭСХ, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>А.Ю. Лейдерман</td>\r\n			<td>Д.С. Стребков (ФГБНУ ВИЭСХ, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Н.Р.Авезова</td>\r\n			<td>В.А. Майоров (ФГБНУ ВИЭСХ, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>М.В. Кремков</td>\r\n			<td>В.В. Харченко (ФГБНУ ВИЭСХ, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ю.К. Рашидов</td>\r\n			<td>Л.Д. Сагинов (ФГБНУ ВИЭСХ, Россия)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>М.Н. Турсунов</td>\r\n			<td>Dr. Beddiaf Zaidi (Badji Mokhtar University, Algeria)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ш.И. Клычев</td>\r\n			<td>Prof. Zhen Fang (Co-Editor-in-Chief, <a href=\"http://lifesciencejournalspublishers.com/index.php?subid=186172&amp;option=com_acymailing&amp;ctrl=url&amp;urlid=38&amp;mailid=48\">Journal of Technology Innovations in Renewable Energy</a>, China)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>И.Г. Атабаев</td>\r\n			<td>Prof. Klaus Vajen (Kassel University, Germany)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>А.А. Абдурахманов</td>\r\n			<td>Prof. Giovanni V. Fracastoro (Polito di Torino, Italy)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Р.Р. Кабулов</td>\r\n			<td>Prof. Wang Zhifeng (Inst. of Elect. Eng, CAS, China)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Н.А. Матчанов</td>\r\n			<td>Ph.D. Feng Kang (Nankai University, China)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ж.С. Ахатов</td>\r\n			<td>Dr. Tangellapalli Srinivas (VIT University,India)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Т.М. Разыков</td>\r\n			<td>Ph.D. Evangelos Bellos (Nat. Tech. Univ. of Athens, Greece)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>В.Г. Дыскин</td>\r\n			<td>Prof. Ahmed Mezrhab (Univ. Mohamed Premier, Marocco)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>А.Г. Комилов</td>\r\n			<td>Prof. Manuel Blanco (CSIRO, Australia)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>У.А. Таджиев</td>\r\n			<td>Dr. Lourdes Ramirez Santigosa (CIEMAT, Spain)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Р.А. Захидов</td>\r\n			<td>Dr. M. Eswaramoorthy (ACS Coll. of Eng., India)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>М.К. Бахадырханов</td>\r\n			<td>Asst. Prof. Geetanjali Raghav (UPES, India)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>М. Кенисарин</td>\r\n			<td>Dr. Abed A. S. Alshqirate (Al-Balqa&#39; Applied University, Jordan)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ш.А. Файзиев</td>\r\n			<td>Dr. I.S. Yahia (King Khalid University, Saudi Arabia)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ш.А. Мирсагатов</td>\r\n			<td>Dr. Pooran Koli (J.N.V. University, India)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Е. Аббасов</td>\r\n			<td>Ilya Astrouski (Brno Univ. of Tech., Czech Republic)</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"table\">\r\n	<thead>\r\n		<tr>\r\n			<th>Local reviewers</th>\r\n			<th>Foreign reviewers</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>А.С. Саидов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>О.С. Попель (ОИВТ РАН, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Р.Р. Авезов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>С.Е. Фрид (ОИВТ РАН, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>У.Х. Газиев</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>С.Н. Трушевский (ФГБНУ ВИЭСХ, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>А.Ю. Лейдерман</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Д.С. Стребков (ФГБНУ ВИЭСХ, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Н.Р.Авезова</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>В.А. Майоров (ФГБНУ ВИЭСХ, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>М.В. Кремков</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>В.В. Харченко (ФГБНУ ВИЭСХ, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Ю.К. Рашидов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Л.Д. Сагинов (ФГБНУ ВИЭСХ, Россия)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>М.Н. Турсунов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. Beddiaf Zaidi (Badji Mokhtar University, Algeria)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Ш.И. Клычев</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Prof. Zhen Fang (Co-Editor-in-Chief, <a href=\"http://lifesciencejournalspublishers.com/index.php?subid=186172&amp;option=com_acymailing&amp;ctrl=url&amp;urlid=38&amp;mailid=48\">Journal of Technology Innovations in Renewable Energy</a>, China)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>И.Г. Атабаев</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Prof. Klaus Vajen (Kassel University, Germany)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>А.А. Абдурахманов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Prof. Giovanni V. Fracastoro (Polito di Torino, Italy)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Р.Р. Кабулов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Prof. Wang Zhifeng (Inst. of Elect. Eng, CAS, China)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Н.А. Матчанов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Ph.D. Feng Kang (Nankai University, China)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Ж.С. Ахатов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. Tangellapalli Srinivas (VIT University,India)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Т.М. Разыков</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Ph.D. Evangelos Bellos (Nat. Tech. Univ. of Athens, Greece)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>В.Г. Дыскин</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Prof. Ahmed Mezrhab (Univ. Mohamed Premier, Marocco)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>А.Г. Комилов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Prof. Manuel Blanco (CSIRO, Australia)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>У.А. Таджиев</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. Lourdes Ramirez Santigosa (CIEMAT, Spain)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Р.А. Захидов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. M. Eswaramoorthy (ACS Coll. of Eng., India)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>М.К. Бахадырханов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Asst. Prof. Geetanjali Raghav (UPES, India)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>М. Кенисарин</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. Abed A. S. Alshqirate (Al-Balqa&#39; Applied University, Jordan)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Ш.А. Файзиев</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. I.S. Yahia (King Khalid University, Saudi Arabia)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Ш.А. Мирсагатов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Dr. Pooran Koli (J.N.V. University, India)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:141.8pt\">\r\n			<p>Е. Аббасов</p>\r\n			</td>\r\n			<td style=\"width:12.0cm\">\r\n			<p>Ilya Astrouski (Brno Univ. of Tech., Czech Republic)</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2018-05-06 17:09:00', NULL, '2018-05-06 11:13:26', '2018-05-06 12:09:23'),
(10, 'Показатели', 'Indicators', '<p style=\"margin-left:0cm; margin-right:-7.15pt; text-align:justify\"><span style=\"color:black\">Английская версия журнала &laquo;Гелиотехника&raquo; - &laquo;</span><span style=\"color:black\">Applied</span> <span style=\"color:black\">Solar</span> <span style=\"color:black\">Energy</span><span style=\"color:black\">&raquo;, издается 4 раза в год и имеет наукометрические показатели Глобальных индексов цитирования - </span><span style=\"color:black\">Journal</span> <span style=\"color:black\">Metrics</span><span style=\"color:black\">: </span><span style=\"color:black\">Source</span> <span style=\"color:black\">Normalized</span> <span style=\"color:black\">Impact</span> <span style=\"color:black\">per</span> <span style=\"color:black\">Paper</span><span style=\"color:black\"> (</span><span style=\"color:black\">SNIP</span><span style=\"color:black\">): 0.761 (2016 г.); </span><span style=\"color:black\">SCImago</span> <span style=\"color:black\">Journal</span> <span style=\"color:black\">Rank</span><span style=\"color:black\"> (</span><span style=\"color:black\">SJR</span><span style=\"color:black\">): 0.238 (2016 г.); </span><span style=\"color:black\">ResearchGate</span> <span style=\"color:black\">Journal</span> <span style=\"color:black\">Impact</span><span style=\"color:black\">: 0.53 (2016 г.).</span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:-7.15pt; text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><strong>Количество показателей статей за последнее 4 года</strong></p>\r\n\r\n<div>\r\n<table border=\"0\" cellspacing=\"0\" class=\"table\">\r\n	<thead>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2014 год</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2015 год</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2016 год</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2017 год</p>\r\n			</td>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">местные авторы (зарубежные авторы)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">местные авторы (зарубежные авторы)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">местные авторы (зарубежные авторы)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">местные авторы (зарубежные авторы)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№1</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">12 (4)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">13 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">9 (5)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">14 (6)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№2</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">12 (7)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">17 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">20 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">13 (12)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№3</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">13 (5)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">12 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">16 (7)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">11 (10)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№4</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">17 (5)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">20 (8)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">14 (9 )</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">14 (2)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">Общее количество статей</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">54 (21)/61% (39%)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">62 (26)/58%(42%)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">59 (27)/54%(46%)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">52 (30)/43%(57%)</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>', '<p style=\"text-align:center\"><strong>The number of indicators of articles over the last 4 years</strong></p>\r\n\r\n<table border=\"0\" cellspacing=\"0\" class=\"table\">\r\n	<thead>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2014</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2015</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2016</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">2017</p>\r\n			</td>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">local authors (foreign authors)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">local authors (foreign authors)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">local authors (foreign authors)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">local authors (foreign authors)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№1</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">12 (4)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">13 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">9 (5)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">14 (6)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№2</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">12 (7)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">17 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">20 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">13 (12)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№3</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">13 (5)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">12 (6)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">16 (7)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">11 (10)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">№4</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">17 (5)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">20 (8)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">14 (9 )</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">14 (2)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\">Total number of articles</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">54 (21)/61% (39%)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">62 (26)/58%(42%)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">59 (27)/54%(46%)</p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">52 (30)/43%(57%)</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2018-05-06 17:03:00', NULL, '2018-05-06 12:01:55', '2018-05-06 12:06:38');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_groups`
--

CREATE TABLE `rbac_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `rbac_groups`
--

INSERT INTO `rbac_groups` (`id`, `name`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `is_deleted`, `deleted_at`, `deleted_by`) VALUES
(1, 'Super administrator', 1, '2018-05-05 10:57:27', NULL, '2018-05-05 10:57:27', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_groups_permissions_rel`
--

CREATE TABLE `rbac_groups_permissions_rel` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `rbac_groups_permissions_rel`
--

INSERT INTO `rbac_groups_permissions_rel` (`group_id`, `permission_id`, `created_at`, `created_by`) VALUES
(1, 1, '2018-05-05 15:57:27', NULL),
(1, 2, '2018-05-05 15:57:27', NULL),
(1, 3, '2018-05-05 15:57:27', NULL),
(1, 4, '2018-05-05 15:57:27', NULL),
(1, 5, '2018-05-05 15:57:27', NULL),
(1, 6, '2018-05-05 15:57:27', NULL),
(1, 7, '2018-05-05 15:57:27', NULL),
(1, 8, '2018-05-05 15:57:27', NULL),
(1, 9, '2018-05-05 15:57:27', NULL),
(1, 10, '2018-05-05 15:57:27', NULL),
(1, 11, '2018-05-05 15:57:27', NULL),
(1, 12, '2018-05-05 15:57:27', NULL),
(1, 13, '2018-05-05 15:57:27', NULL),
(1, 14, '2018-05-05 15:57:27', NULL),
(1, 15, '2018-05-05 15:57:27', NULL),
(1, 16, '2018-05-05 15:57:27', NULL),
(1, 17, '2018-05-05 15:57:27', NULL),
(1, 18, '2018-05-05 15:57:27', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_permissions`
--

CREATE TABLE `rbac_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `rbac_permissions`
--

INSERT INTO `rbac_permissions` (`id`, `name`, `code`, `module`, `updated_at`, `updated_by`) VALUES
(1, 'Доступ', 'access-administrator', 'Администратор', '2018-05-05 15:57:27', NULL),
(2, 'Создать', 'create-administrator', 'Администратор', '2018-05-05 15:57:27', NULL),
(3, 'Удалить', 'delete-administrator', 'Администратор', '2018-05-05 15:57:27', NULL),
(4, 'Доступ', 'access-user', 'Пользователь', '2018-05-05 15:57:27', NULL),
(5, 'Создать', 'create-user', 'Пользователь', '2018-05-05 15:57:27', NULL),
(6, 'Удалить', 'delete-user', 'Пользователь', '2018-05-05 15:57:27', NULL),
(7, 'Доступ', 'access-group', 'Группа', '2018-05-05 15:57:27', NULL),
(8, 'Создать', 'create-group', 'Группа', '2018-05-05 15:57:27', NULL),
(9, 'Удалить', 'delete-group', 'Группа', '2018-05-05 15:57:27', NULL),
(10, 'Доступ', 'access-journal', 'Журнал', '2018-05-05 15:57:27', NULL),
(11, 'Создать', 'create-journal', 'Журнал', '2018-05-05 15:57:27', NULL),
(12, 'Удалить', 'delete-journal', 'Журнал', '2018-05-05 15:57:27', NULL),
(13, 'Доступ', 'access-edition', 'Выпуск', '2018-05-05 15:57:27', NULL),
(14, 'Создать', 'create-edition', 'Выпуск', '2018-05-05 15:57:27', NULL),
(15, 'Удалить', 'delete-edition', 'Выпуск', '2018-05-05 15:57:27', NULL),
(16, 'Доступ', 'access-article', 'Статья', '2018-05-05 15:57:27', NULL),
(17, 'Создать', 'create-article', 'Статья', '2018-05-05 15:57:27', NULL),
(18, 'Удалить', 'delete-article', 'Статья', '2018-05-05 15:57:27', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_users_groups_rel`
--

CREATE TABLE `rbac_users_groups_rel` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `rbac_users_groups_rel`
--

INSERT INTO `rbac_users_groups_rel` (`user_id`, `group_id`, `created_at`, `created_by`) VALUES
(1, 1, '2018-05-05 15:57:27', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `reviewer_article`
--

CREATE TABLE `reviewer_article` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_meta_id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cheking_and_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reviewer_response`
--

CREATE TABLE `reviewer_response` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_meta_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `result` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Admin'),
(2, 'user', 'Member');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_editor` tinyint(4) NOT NULL DEFAULT '0',
  `is_editors_chairman` tinyint(4) NOT NULL DEFAULT '0',
  `editor_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_reviewer` tinyint(4) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `fio`, `photo`, `verify_token`, `is_editor`, `is_editors_chairman`, `editor_title`, `is_reviewer`, `role_id`, `status`, `remember_token`, `created_at`, `created_by`, `updated_at`, `updated_by`, `is_deleted`, `deleted_at`, `deleted_by`) VALUES
(1, 'admin@gmail.com', '$2y$10$e/i.Fz/SSOqCW3spR8imROp5EtS0p4mbCPsSSTxnX1Hzvu/kGcFFy', 'Admin', NULL, '0', 0, 0, NULL, 0, 1, 1, NULL, '2018-05-05 10:57:27', NULL, '2018-05-05 10:57:27', NULL, 0, NULL, NULL),
(2, 'member@gmail.com', '$2y$10$ir5SyDiuHOzA/Fh0uhyeJOVYgKxzZqgOh9nTOQjuz2oJk4/cvykJy', 'Member', NULL, '0', 0, 0, NULL, 0, 2, 1, NULL, '2018-05-05 10:57:27', NULL, '2018-05-05 10:57:27', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_articles`
--

CREATE TABLE `user_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `rejected_at` timestamp NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_accept` int(11) NOT NULL DEFAULT '0',
  `accept_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_article_authors`
--

CREATE TABLE `user_article_authors` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `fio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main_author` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_article_meta`
--

CREATE TABLE `user_article_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_receive` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_categories`
--
ALTER TABLE `article_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_reviewer`
--
ALTER TABLE `article_reviewer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_posts_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `editors_teams`
--
ALTER TABLE `editors_teams`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gallerys`
--
ALTER TABLE `gallerys`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `journal_editions`
--
ALTER TABLE `journal_editions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `journal_edition_articles`
--
ALTER TABLE `journal_edition_articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_foreign` (`menu`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Индексы таблицы `rbac_groups`
--
ALTER TABLE `rbac_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rbac_groups_permissions_rel`
--
ALTER TABLE `rbac_groups_permissions_rel`
  ADD PRIMARY KEY (`group_id`,`permission_id`),
  ADD KEY `rbac_groups_permissions_rel_permission_id_foreign` (`permission_id`);

--
-- Индексы таблицы `rbac_permissions`
--
ALTER TABLE `rbac_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rbac_users_groups_rel`
--
ALTER TABLE `rbac_users_groups_rel`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `rbac_users_groups_rel_group_id_foreign` (`group_id`);

--
-- Индексы таблицы `reviewer_article`
--
ALTER TABLE `reviewer_article`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviewer_response`
--
ALTER TABLE `reviewer_response`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_articles`
--
ALTER TABLE `user_articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_article_authors`
--
ALTER TABLE `user_article_authors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_article_meta`
--
ALTER TABLE `user_article_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `article_categories`
--
ALTER TABLE `article_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `article_reviewer`
--
ALTER TABLE `article_reviewer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `editors_teams`
--
ALTER TABLE `editors_teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gallerys`
--
ALTER TABLE `gallerys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `journals`
--
ALTER TABLE `journals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `journal_editions`
--
ALTER TABLE `journal_editions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `journal_edition_articles`
--
ALTER TABLE `journal_edition_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `rbac_groups`
--
ALTER TABLE `rbac_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `rbac_permissions`
--
ALTER TABLE `rbac_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `reviewer_article`
--
ALTER TABLE `reviewer_article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reviewer_response`
--
ALTER TABLE `reviewer_response`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `user_articles`
--
ALTER TABLE `user_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user_article_authors`
--
ALTER TABLE `user_article_authors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user_article_meta`
--
ALTER TABLE `user_article_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD CONSTRAINT `blog_posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`);

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `rbac_groups_permissions_rel`
--
ALTER TABLE `rbac_groups_permissions_rel`
  ADD CONSTRAINT `rbac_groups_permissions_rel_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `rbac_groups` (`id`),
  ADD CONSTRAINT `rbac_groups_permissions_rel_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `rbac_permissions` (`id`);

--
-- Ограничения внешнего ключа таблицы `rbac_users_groups_rel`
--
ALTER TABLE `rbac_users_groups_rel`
  ADD CONSTRAINT `rbac_users_groups_rel_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `rbac_groups` (`id`),
  ADD CONSTRAINT `rbac_users_groups_rel_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
