<?php

namespace App\Policies;

use App\Models\User;

class ArticlePolicy
{

    /**
     * Determine if the given model can be viewed by the user.
     *
     * @param User $user
     * @param  $model
     *
     * @return bool
     */
    public function read(User $user)
    {
        return $user->hasPermission('access-article');
    }

    /**
     * Determine if the given model can be edited by the user.
     *
     * @param User $user
     * @param  $model
     *
     * @return bool
     */
    public function create(User $user, $model)
    {
        return $user->hasPermission('create-article');
    }

    /**
     * Determine if the given model can be deleted by the user.
     *
     * @param User $user
     * @param  $model
     *
     * @return bool
     */
    public function delete(User $user, $model)
    {
        return $user->hasPermission('delete-article');
    }
}
