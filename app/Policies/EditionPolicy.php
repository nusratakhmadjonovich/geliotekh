<?php

namespace App\Policies;

use App\Models\User;

class EditionPolicy
{

    /**
     * Determine if the given model can be viewed by the user.
     *
     * @param User $user
     * @param  $model
     *
     * @return bool
     */
    public function read(User $user)
    {
        return $user->hasPermission('access-edition');
    }

    /**
     * Determine if the given model can be edited by the user.
     *
     * @param User $user
     * @param  $model
     *
     * @return bool
     */
    public function create(User $user, $model)
    {
        return $user->hasPermission('create-edition');
    }

    /**
     * Determine if the given model can be deleted by the user.
     *
     * @param User $user
     * @param  $model
     *
     * @return bool
     */
    public function delete(User $user, $model)
    {
        return $user->hasPermission('delete-edition');
    }
}
