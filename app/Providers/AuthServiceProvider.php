<?php

namespace App\Providers;

use App\Models\Admin\Administrators;
use App\Models\Admin\Groups;
use App\Models\Admin\JournalEditionArticles;
use App\Models\Admin\JournalEditions;
use App\Models\Admin\Journals;
use App\Models\User;
use App\Policies\AdministratorPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\CmsPolicy;
use App\Policies\EditionPolicy;
use App\Policies\JournalPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Policies\GroupsPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class  => UserPolicy::class,
        Administrators::class  => AdministratorPolicy::class,
        Groups::class  => GroupsPolicy::class,
        Journals::class  => JournalPolicy::class,
        JournalEditions::class  => EditionPolicy::class,
        JournalEditionArticles::class  => ArticlePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
