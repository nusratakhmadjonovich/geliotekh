<?php

namespace App\Providers;

use App\Models\Admin\Menus;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('menu', function() {
            return new Menus();
        });

        $this->app->make('App\Http\Controllers\Admin\MenuController');

        //$this->loadViewsFrom(__DIR__ . '/Views', 'wmenu');
        //view('admin.menu.menu');
    }
}
