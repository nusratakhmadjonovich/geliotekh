<?php
namespace App\Libraries;

use App\Models\Admin\MenuItems;
use App\Models\Admin\Menus;
use Illuminate\Support\Facades\URL;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class MenuManager
{
    public $key;

    public static function build($key){
        $menu = Menus::where('name', '=', $key)->first();
        $links = MenuItems::where('menu', '=', $menu->id)->where('parent', '=', 0)->orderBy('sort', 'ASC')->get();
        $array_links = ($links ? self::makeArray($links) : '');
        if($array_links)
            return self::makeMenu($array_links);
        else
            return '';
    }

    protected static function makeMenu($menu_items){
        $items = '';
        foreach($menu_items as $item){
            if($item['items']) {
                $items.= '<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle '.$item['class'].'" href="'.$item['url'].'"  data-toggle="dropdown" 
                                   aria-haspopup="true" aria-expanded="false">'.$item['label'].
                            '</a>';
                            $items .= self::makeMenuChild($item['items']);
                $items.='</li>';
            } else
                $items.= '<li class="nav-item"><a class="nav-link '.(URL::current() == $item['url'] ? 'top_menu_active':'').' '.$item['class'].'" href="'.$item['url'].'">'.$item['label'].'</a></li>';
        }
        $ul = '<ul class="col-xl-9 col-lg-9 col-md-12 navbar-nav mr-auto">'.$items.'</ul>';
        return $ul;
    }

    protected static function makeMenuChild($menu_items){
        $items = '';
        foreach($menu_items as $item){
            $items.= '<li class="dropdown-item"><a class="'.(URL::current() == $item['url'] ? 'top_menu_active':'').' '.$item['class'].'" href="'.$item['url'].'">'.$item['label'].'</a></li>';
        }
        $ul = '<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">'.$items.'</ul>';
        return $ul;
    }
    /**
     * @param $links
     * @return array
     */
    protected static function makeArray($links){
        $menu = [];
        foreach($links as $link){
            $one_link['label'] = self::get_label($link);
            $one_link['class'] = $link->class;
            $one_link['parent'] = $link->parent;
            $one_link['url'] = self::make_url($link->link);
            $one_link['items'] = [];
            $childs = MenuItems::where('parent', '=', $link->id)->orderBy('sort')->get();

            if($childs){
                $one_link['items'] = self::makeArray($childs);
            }
            $menu[] = $one_link;
        }
        return $menu;
    }

    /**
     * @param $str string
     * @return string
     */
    protected static function make_url($str){
        if(empty($str))
            return '';

        if(preg_match("/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", $str))
            return $str;
        else
            return lang_url($str);
    }

    /**
     * @param $str string
     * @return string
     */
    protected static function get_label($link){
        if(LaravelLocalization::getCurrentLocale() == 'ru')
            return $link->label_ru;
        elseif(LaravelLocalization::getCurrentLocale() == 'en')
            return $link->label_en;
    }
}
