<?php
namespace App\Libraries;

use App\Models\Admin\Blocks;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class BlockManager
{
    public static function get($key){
        $block = Blocks::where('block_key', '=', $key)->first();
        if(LaravelLocalization::getCurrentLocale() == 'ru')
            return $block->block_data_ru;
        elseif(LaravelLocalization::getCurrentLocale() == 'en')
            return $block->block_data_en;
        else
            return '';
    }
}
