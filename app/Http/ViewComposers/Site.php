<?php

namespace App\Http\ViewComposers;

use App\Libraries\MenuManager;
use App\Models\Admin\MenuItems;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;


class Site
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = Auth::user();

        $menu = MenuManager::build('Main');

        $view->with([
            'menu' => $menu,
            'user' => $user
        ]);
    }
}
