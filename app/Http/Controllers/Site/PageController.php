<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site\Pages;

class PageController extends Controller
{
    public function index($id)
    {
        $page = Pages::published()->where('id', '=', $id)->first();
        if(!$page) abort('404');
        return view('site.page.index', compact('page'));
    }
}
