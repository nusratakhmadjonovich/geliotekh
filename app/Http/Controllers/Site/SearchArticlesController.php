<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class SearchArticlesController extends Controller
{
    public function index()
    {
        return view('site.search_articles.index');
    }
}
