<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use App\Models\Site\Posts;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index(Request $request)
    {
        $posts = Posts::active()->where('category_id', '=', 2)->paginate(10);
        if ($request->ajax()) {
            return view('presult', compact('tags'));
        }
        return view('site.events.index', compact('posts'));
    }


    public function view($id)
    {
        $post = Posts::published()->where('id', '=', $id)->first();
        if(!$post) abort('404');

        return view('site.events.view', compact('post'));
    }
}
