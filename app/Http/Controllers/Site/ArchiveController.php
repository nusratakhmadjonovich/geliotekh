<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site\ArticleCategories;
use App\Models\Site\Articles;
use App\Models\Site\JournalEditions;
use Illuminate\Http\Request;

class ArchiveController extends Controller
{
    public function index(Request $request)
    {
        $years = JournalEditions::active()->select('year')->distinct()->orderBy('year', 'DESC')->get();
        $editions = JournalEditions::active()
            ->where(function ($query) use ($request) {
                if ($request->get('year'))
                    $query->where('year', '=', $request->get('year'));

            })->orderBy('year', 'DESC')->paginate(12);

        return view('site.archive.index', ['years'=>$years, 'editions'=>$editions]);
    }

    public function view($id)
    {
        $edition = JournalEditions::active()->where('id', '=', $id)->first();
        $articles = Articles::active()->get();
        $categories = ArticleCategories::active()->get();

        return view('site.archive.view', ['edition' => $edition, 'articles'=>$articles, 'categories'=>$categories]);
    }
}
