<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site\ArticleCategories;
use App\Models\Site\Articles;
use App\Models\Site\JournalEditions;

use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
        $articles = Articles::active()
            ->where(function ($query) use ($request) {
                if ($request->get('edition'))
                    $query->where('journal_edition_id', '=', $request->get('edition'));

                if ($request->get('category'))
                    $query->where('article_category_id', '=', $request->get('category'));

                if ($request->get('title'))
                    $query->where('title_' . LaravelLocalization::getCurrentLocale(), 'like', '%' . $request->get('title') . '%');

                if ($request->get('author'))
                    $query->where('authors', 'like', '%' . $request->get('author') . '%');
            })->paginate(20);

        $editions = JournalEditions::active()->orderBy('year', 'DESC')->orderBy('number', 'DESC')->get();
        $categories = ArticleCategories::active()->get();
        return view('site.articles.index', ['articles' => $articles, 'editions' => $editions, 'categories' => $categories]);
    }

    public function view($id)
    {
        $article = Articles::active()->where('id', '=', $id)->first();
        return view('site.articles.view', ['article' => $article]);
    }
}
