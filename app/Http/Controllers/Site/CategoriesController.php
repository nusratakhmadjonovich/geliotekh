<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site\ArticleCategories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $categories = ArticleCategories::active()->paginate(12);
        return view('site.categories.index', ['categories'=>$categories]);
    }
}
