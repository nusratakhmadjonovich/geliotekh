<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use App\Models\Site\Posts;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $posts = Posts::published()->where('category_id', '=', 1)->paginate(10);
        if ($request->ajax()) {
            return view('presult', compact('tags'));
        }
        return view('site.posts.index', compact('posts'));
    }


    public function view($id)
    {
        $post = Posts::published()->where('id', '=', $id)->first();
        if(!$post) abort('404');

        return view('site.posts.view', compact('post'));
    }
}
