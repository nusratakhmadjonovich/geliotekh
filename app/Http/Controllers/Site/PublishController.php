<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class PublishController extends Controller
{
    public function index()
    {
        return view('site.publish.index');
    }
}
