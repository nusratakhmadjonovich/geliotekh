<?php

namespace App\Http\Controllers;

use App\Models\Sub_Author;
use App\Models\User_Article;


class UserController extends Controller
{

    public function getUser()
    {
        return view('user.user');
    }

    public function getArticles()
    {
        return view('user.userArticles');
    }

    public function getLanguages()
    {
        return view('user.languages');
    }
}
