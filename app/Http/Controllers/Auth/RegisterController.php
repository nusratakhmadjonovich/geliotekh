<?php

namespace App\Http\Controllers\Auth;

use App\Models\Role;
use App\Models\User;
use App\Notifications\SendConfirmation;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistration()
    {
        return view('auth.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    protected function create(array $data)
    {
        /** @var  $user User */
        $user = User::create([
            'fio' => $data['fio'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => $data['status'],
            'verify_token' => Uuid::uuid4(),
            'role_id' => Role::ROLE_USER
        ]);

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all())->validate();

        if ($validator && $validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = $this->create(array_merge($request->all(), ['status' => self::STATUS_NEW_REQUEST]));

      //  $user->notify(new SendConfirmation($user->verify_token));


        return redirect()->route('login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fio' => 'string|max:255',
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('users')->where(function ($query) {
                    $query->whereIn('status', [1, 0]);
                    $query->where('is_deleted', '=', 0);
                }),
            ],
            'password' => 'required|string|min:6|confirmed',
            'terms_of_firm' => 'required',
        ]);
    }

    public function confirm($token)
    {
        $user = User::where('verify_token', $token)->first();
        if (!$user) {
            return redirect('auth/login')->with('error-message', trans('auth.confirmation_token_not_found'));
        }

        $user->status = 1;
        if ($user->save()) {
            return redirect('auth/login')->with('success-message', trans('auth.confirmation_provided'));
        }
    }
}
