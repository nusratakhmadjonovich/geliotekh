<?php
/**
 * creater by Nusrat To'ykhulov
 *creator laptop is Dell
 */
namespace App\Http\Controllers\Auth;

use App\Libraries\UploadManager;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        return view('auth.profile.profile',compact('user'));
    }

    public function update_profile(Request $request)
    {
        $this->validate($request,[
            'fio'=>'required',
            'email'=>'required',
            'photo'=>'image|mimes:jpg,png,jpeg|max:1000'
        ]);

        $requestData = $request->all();

        $user = Auth::user();

        if ($request->file('photo')) {

            $requestData['photo'] = UploadManager::uploadPhoto('profile', $user->id, $request->file('photo'), $user->photo);
        }

        $user->update($requestData);

        return redirect('auth/profile')->with('success-message',__('message.data_message'));
    }

    public function removePhoto($id){

        $user = User::findOrFail($id);

        if($user->photo) {
            UploadManager::deletePhoto('profile', $user->id, $user->photo);
            $user->update(['photo' => '']);
        }
        return redirect('auth/profile')->with('success-message','Фотография успешно удалено !');
    }

    public function change_password()
    {
        return view('auth.profile.password');
    }

    public function update_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:5',
            'new_password' => 'required|min:5',
            'confirm_password' => 'required|min:5|required_with:new_password|same:new_password'
        ]);

        $user = Auth::user();

        if (Hash::check(Input::get('password'),$user->password))
        {
            $user->password = bcrypt($request->new_password);

            $user->update();

            return redirect('auth/password')->with('success-message',__('message.password_message'));

        }else
        {
            return redirect('auth/password')->with('danger-message',__('message.error_password'));
        }
    }
}
