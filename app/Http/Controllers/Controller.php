<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOACTIVE = 0;
    const STATUS_DRAFT = -1;
    const STATUS_NEW_REQUEST = -2;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

}
