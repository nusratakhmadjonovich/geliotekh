<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Journals;
use Yajra\Datatables\Datatables;
use Alert;

class JournalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.journals.index');
    }

    public function get_list(Request $request)
    {
        $journals = Journals::nodraft();
        return Datatables::of($journals)
            ->addColumn('actions', function ($journal) {
                return '
                    <a href="'. url('/admin/journals/' . $journal->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('name')) {
                    $query->where('name', 'like', "%".request('name')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $journal = new Journals();
        $journal->status = self::STATUS_DRAFT;
        $journal->save();
        return redirect('admin/journals/'.$journal->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $journal = Journals::findorfail($id);
        return view('admin.journals.show',compact('journal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $journal = Journals::findorfail($id);
        $isNewRecord = (int)($journal->status == self::STATUS_DRAFT);
        return view('admin.journals.edit', compact(['journal','isNewRecord']) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $journal = Journals::findorfail($id);
        $journal->status = self::STATUS_ACTIVE;
        $journal->name = $request->name;
        $journal->save();

        return redirect('/admin/journals/' . $journal->id . '/edit')->with('success-message', 'Thanks!');

    }
}
