<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\JournalEditionArticles;
use App\Models\Admin\JournalEditions;
use App\Http\Controllers\Controller;
use App\Models\User;

class AdminController extends Controller
{
    public function getDashboard()
    {
        $users_count = User::nodraft()->nodeleted()->users()->count();
        $editions_count = JournalEditions::nodraft()->count();
        $articles_count = JournalEditionArticles::nodraft()->count();
        return view('admin.dashboard', compact(['users_count','editions_count','articles_count']));
    }
}
