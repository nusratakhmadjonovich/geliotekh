<?php

namespace App\Http\Controllers\Admin;

use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Setting;
use Yajra\Datatables\Datatables;

class SettingController extends Controller
{
    public function index()
    {
        return view('admin.setting.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_list()
    {
        $settings = Setting::active();

        return Datatables::of($settings)
            ->addColumn('actions', function ($setting) {
                return '<a href="'. url('/admin/setting/' . $setting->id . '/edit') .'" title="Edit Admin"><button class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>';
            })
            ->rawColumns(['actions'])
            ->filter(function ($setting) {
                if (request()->filled('title')) {
                    $setting->where('title', 'like', "%".request('title')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = new Setting();
        $setting->status = self::STATUS_DRAFT;
        $setting->save();
        return redirect('admin/setting/'.$setting->id.'/edit');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::findorfail($id);
        $isNewRecord = (int)($setting->status == -1);
        return view('admin.setting.edit', ['find_setting'=>$setting,'isNewRecord'=>$isNewRecord]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'setting_key'=>'required|max:255',
            'setting_value'=>'required|max:255'
        ]);

        $setting = Setting::findorfail($id);
        $requestData = $request->all();
        $requestData['status'] = self::STATUS_ACTIVE;
        $setting->update($requestData);


        return redirect('/admin/setting/' . $setting->id . '/edit')->with('success-message', 'Thanks!');
    }
}
