<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Libraries\Upload;
use Illuminate\Http\Request;
use App\Models\Admin\Pages;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.index');
    }

    public function get_list(Request $request)
    {
        $posts = Pages::published();

        return Datatables::of($posts)
            ->addColumn('actions', function ($post) {
                return '
                    <a href="'. url('/admin/pages/' . $post->id) .'" title="Показать"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                    <a href="'. url('/admin/pages/' . $post->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    <form method="POST" action="'. url('/admin/pages' . '/' . $post->id).'" accept-charset="UTF-8" style="display:inline">
                        '.method_field("DELETE").'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->addColumn('url', function ($post) {
                return 'page/'.$post->id;
            })
            ->rawColumns(['actions', 'url'])
            ->filter(function ($query) {
                if (request()->filled('name_ru')) {
                    $query->where('name_ru', 'like', "%".request('name_ru')."%");
                }
            })
            ->make(true);
    }

    public function create()
    {
        $post = new Pages();
        $post->status = self::STATUS_DRAFT;
        $post->save();
        return redirect('admin/pages/'.$post->id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Pages::findorfail($id);
        $isNewRecord = (int)($post->status == self::STATUS_DRAFT);

        return view('admin.pages.edit', compact(['categories', 'post', 'isNewRecord']) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_ru' => 'required',
            'title_en' => 'required'
        ]);

        $post = Pages::findOrFail($id);
        $requestData = $request->all();
        $requestData['publish_at'] = date('Y-m-d H:i:s', strtotime($request->publish_at));
        $requestData['status'] = self::STATUS_ACTIVE;
        $post->update($requestData);

        return redirect('/admin/pages/' . $post->id . '/edit')->with('success-message', 'Thanks!') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = Pages::findOrFail($id);

        return view('admin.pages.show', compact('post'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Pages::where('id', '=', $id)->update(['deleted_at' => date('c')]);
        return redirect('admin/pages')->with('flash_message', 'Admin deleted!');
    }
}
