<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\FileManager;
use App\Libraries\UploadManager;
use App\Models\Admin\Journals;
use App\Models\Admin\JournalEditions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Alert;


class JournalEditionsController extends Controller
{
    public function index()
    {
        return view('admin.journals_editions.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_list(Request $request)
    {
        $journal_editions = JournalEditions::nodraft()->orderBy('year', 'DESC')->orderBy('number', 'DESC')->with('journal');

        return Datatables::of($journal_editions)
            ->addColumn('actions', function ($journal_edition) {
                return '
                    <a href="' . url('/admin/journal/editions/' . $journal_edition->id . '/edit') . '" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('year')) {
                    $query->where('year', '=',  request('year'));
                }

                if (request()->filled('number')) {
                    $query->where('number', '=', request('number'));
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $journal_edition = new JournalEditions();
        $journal_edition->status = self::STATUS_DRAFT;
        $journal_edition->save();
        return redirect('/admin/journal/editions/' . $journal_edition->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $journal_edition = JournalEditions::where('id', '=', $id)->first();
        return view('admin.journals_editions.show', compact('journal_edition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $journal_edition = JournalEditions::findorfail($id);
        $journals = Journals::where('status', '=', self::STATUS_ACTIVE)->get();
        $isNewRecord = (int)($journal_edition->status == self::STATUS_DRAFT);
        return view('admin.journals_editions.edit', compact(['journal_edition', 'journals', 'isNewRecord']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $journal_edition = JournalEditions::findorfail($id);

        $this->validate($request, [
            'year' => 'required|max:255',
            'number' => 'required|max:255',
            'photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'file' => 'mimes:pdf|max:20480'
        ]);

        $requestData = $request->all();

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('journal_edition', $journal_edition->id, $request->file('photo'), $journal_edition->photo);
        }

        if ($request->file('file')) {
            $requestData['file'] = UploadManager::upload('journal_edition', $journal_edition->id, $request->file('file'), $journal_edition->file);
        }

        $requestData['status'] = self::STATUS_ACTIVE;
        $journal_edition->update($requestData);

        return redirect('/admin/journal/editions/' . $journal_edition->id . '/edit')->with('success-message', 'Journal edition successfully edited!') ;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $destroy = JournalEditions::where('id', '=', $id)->update(['is_deleted' => '1']);

        return redirect('/admin/journal/edition');

    }

    public function getAjaxEditons(Request $request){
        return JournalEditions::where('status', '=', 1)->where('journal_id', '=', $request->journal_id)->pluck('name', 'id');
    }

    public function removeFile($id)
    {
        $removeFile = JournalEditions::findorfail($id);
        if($removeFile->photo) {
            UploadManager::delete('journal_edition', $removeFile->id, $removeFile->photo);
            $removeFile->update(['photo' => '']);
        }
        return back()->with('success-message', 'Фотография удалено успешно');
    }

    public function removeDoc($id)
    {
        $removeDoc = JournalEditions::findorfail($id);
        if ($removeDoc->file)
        {
            UploadManager::delete('journal_edition', $removeDoc->id, $removeDoc->file);
            $removeDoc->update(['file' => '']);
        }
        return back()->with('success-message', 'файл удалено успешно');
    }
}
