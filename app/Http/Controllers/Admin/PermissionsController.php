<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Permissions;
use App\Models\Admin\Setting;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.permissions.index');
    }

    public function get_list(Request $request)
    {
        $permissions = Permissions::query();

        return Datatables::of($permissions)
            ->addColumn('actions', function ($permission) {
                return '<a href="'. url('/admin/permissions/' . $permission->id . '/edit') .'" title="Edit Admin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('name')) {
                    $query->where('name', 'like', "%".request('name')."%");
                }
            })
            ->make(true);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return Response
     */
    public function edit(Permissions $permission)
    {
        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Permission  $permission
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'code' => 'required|max:255'
        ]);
        $requestData = $request->all();

        $permission = Permissions::findOrFail($id);
        $permission->update($requestData);

        return redirect('admin/permissions')->with('flash_message', 'Admin updated!');
    }

    public function setting()
    {
        return view('admin.setting.index');
    }

    public function setting_list()
    {
        $permissions = Setting::all();

        return Datatables::of($permissions)
            ->addColumn('actions', function ($permission) {
                return '<a href="'. url('/admin/permissions/' . $permission->id . '/edit') .'" title="Edit Admin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('name')) {
                    $query->where('name', 'like', "%".request('name')."%");
                }
            })
            ->make(true);
    }

}
