<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\FileManager;
use App\Libraries\UploadManager;
use App\Models\Admin\ArticleCategories;
use App\Models\Admin\JournalEditions;
use App\Models\Admin\JournalEditionArticles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class JournalEditionArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('admin.journal_edition_articles.index');
    }

    public function get_list(Request $request)
    {
        $articles = JournalEditionArticles::nodraft()->where('is_deleted','!=',1);

        return Datatables::of($articles)
            ->addColumn('actions', function ($article) {
                return '
                    <a href="'. url('/admin/journal/edition/articles/' . $article->id) .'" title="Показать"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                    <a href="'. url('/admin/journal/edition/articles/'.$article->id.'/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    
                    <form method="POST" action="'. url('/admin/journal/edition/articles/'.$article->id).'" accept-charset="UTF-8" style="display:inline">
                        '.method_field("DELETE") .'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('name')) {
                    $query->where('title_ru', 'like', "%".request('name')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new JournalEditionArticles();
        $article->status = self::STATUS_DRAFT;
        $article->save();
        return redirect('/admin/journal/edition/articles/' . $article->id . '/edit');

        //return view('admin.journals_editions.create',compact('journal'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $find_journal_edition_articles = JournalEditionArticles::find($id);
        return view('admin.journal_edition_articles.show',compact('find_journal_edition_articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = JournalEditionArticles::findorfail($id);
        $editions = JournalEditions::nodraft()->orderBy('year', 'DESC')->orderBy('number', 'DESC')->get();
        $categories = ArticleCategories::nodraft()->orderBy('id', 'ASC')->get();
        $isNewRecord = (int)($article->status == -1);
        return view('admin.journal_edition_articles.edit', compact(['article','isNewRecord', 'editions', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = JournalEditionArticles::findorfail($id);

        $this->validate($request, [
            'journal_edition_id' => 'required|max:255',
            'title_ru' => 'required|max:255',
            'title_en' => 'max:255',
            'description_ru' => 'required',
            'file' => 'mimes:pdf|max:10240',
            'photo' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $requestData = $request->all();

        if ($request->file('file')) {
            $requestData['file'] = UploadManager::upload('journal_edition_article', $article->id, $request->file('file'), $article->file);
        }

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('journal_edition_article', $article->id, $request->file('photo'), $article->photo);
        }

        $article->update($requestData);

        return redirect('/admin/journal/edition/articles/' . $article->id . '/edit')->with('success-message', 'Journal edition successfully edited!') ;

    }

    public function removePhoto($id){
        $article = JournalEditionArticles::findOrFail($id);
        if($article->photo) {
            UploadManager::delete('journal_edition_article', $article->id, $article->photo);
            $article->update(['photo' => '']);
        }
        return back()->with('success-message', 'Фотография удалено успешно');
    }

    public function removeFile($id){
        $article = JournalEditionArticles::findOrFail($id);
        if($article->file) {
            UploadManager::delete('journal_edition_article', $article->id, $article->file);
            $article->update(['file' => '']);
        }
        return back()->with('success-message', 'Файл удалено успешно');
    }

    public function destroy($id)
    {
        $destroy = JournalEditionArticles::where('id','=',$id)->update(['is_deleted'=>1]);
        return  redirect()->back();
    }
}
