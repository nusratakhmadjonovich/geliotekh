<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_list(Request $request)
    {
        $users = Companies::nodraft()->nodeleted();
        return DataTables::of($users)
            ->addColumn('status', function ($user) {
                if($user->status == 1)
                    return '<span class="label label-success">Актив</span>';
                else
                    return '<span class="label label-danger">Неактив</span>';
            })
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/company/' . $user->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    
                    <form method="POST" action="'. url('/admin/company' . '/' . $user->id).'" accept-charset="UTF-8" style="display:inline">
                        '.method_field("DELETE").'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->rawColumns(['actions', 'status'])
            ->filter(function ($query) {
                if (request()->filled('status')) {
                    $query->where('status', '=', request('status'));
                }

                if (request()->filled('name')) {
                    $query->where('name', 'like', "%".request('name')."%");
                }

            })
            ->make(true);
    }


    public function index()
    {
        return view('admin.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = new Companies();
        $company->status = self::STATUS_DRAFT;
        $company->save();

        return redirect('/admin/company/'.$company->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Companies::findOrFail($id);
        $isNewRecord = (int)($company->status == Companies::STATUS_DRAFT);

        return view('admin.companies.edit', compact(['company', 'isNewRecord']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Companies::findorfail($id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'ip_address' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $requestData = $request->all();

        $company->update($requestData);

        return redirect('/admin/company/' . $company->id . '/edit')->with('success-message', 'Компания успешно редактирована!') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Companies::where('id', '=', $id)->update(['is_deleted' => 1]);
        return redirect('admin/company')->with('success-message', 'Компания успешно удалено!');
    }
}
