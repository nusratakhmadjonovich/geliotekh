<?php
namespace App\Http\Controllers\Admin;
use App\Http\Requests;
use App\Models\Members\UserArticles;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Admin\Administrators;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Alert;

class UserArticlesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.articles.index',compact('admins'));
    }

    public function get_list(Request $request)
    {

        $users = UserArticles::nodraft()->with('users_articles');

        return Datatables::of($users)
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/users/articles/' . $user->id) .'" title="View Article"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                    <a href="'. url('/admin/review/articles/'.$user->id.'/edit') .'" title="send to reviewer"><button class="btn btn-primary btn-sm"><i class="fa fa-send-o" aria-hidden="true"></i> </button></a>
                    <form method="POST" action="'. url('/admin/users/articles/rejected/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-success btn-sm" title="Reject Article" onclick="return confirm(&quot;are you sure rejec article ?  &quot;)"><i class="fa  fa-toggle-down" aria-hidden="true"></i> </button>
                    </form>
                    <a href="'.url('/admin/users/articles/print/'.$user->id).'" onclick="return confirm(&quot; are you sure accept article ? &quot;)" title="Accept Article"><button class="btn btn-success btn-sm"><i class="fa fa-object-ungroup" aria-hidden="true"></i></button></a>
                    <form method="POST" action="'. url('/admin/users/articles/destroy/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.method_field('DELETE') .'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Article" onclick="return confirm(&quot;are you sure delete article ? &quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
        
               
                    ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('id')) {
                    $query->where('id', '=', request('id'));
                }

                if (request()->filled('title')) {
                    $query->where('title', 'like', "%".request('title')."%");
                }

                if (request()->filled('description')) {
                    $query->where('description', 'like', "%".request('description')."%");
                }

                if (request()->filled('location')) {
                    $query->where('created_at', 'like', "%".request('location')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = new Administrators;
        $user->role_id = Role::ROLE_ADMIN;
        $user->status = -1;
        $user->created_by  = Auth::user()->id;
        $user->save();

        return redirect('/admin/admins/'.$user->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fio' => 'required|max:255',
            'email' => 'required|email'
        ]);

        $requestData = $request->all();
        Administrators::create($requestData);
        return redirect('admin/admins')->with('flash_message', 'Admin added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $find_id = UserArticles::findorfail($id);
        $articles = UserArticles::where('id','=',$id)->first();
        return view('admin.articles.show', compact('articles'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $admin = Administrators::findOrFail($id);
        $isNewRecord = (int)($admin->status == -1);

        return view('admin.admins.edit', compact(['admin', 'isNewRecord']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fio' => 'required|max:255',
            'email' => 'required|email'
        ]);
        $requestData = $request->all();
        $requestData['role_id'] = Role::ROLE_ADMIN;

        $admin = Administrators::findOrFail($id);
        $admin->update($requestData);

        return redirect('admin/admins')->with('flash_message', 'Admin updated!');
    }

    public function reject_show($id)
    {
        $find_id = UserArticles::findOrFail($id);
        $articles = UserArticles::where('id','=',$id)->first();
        return view('admin.articles.reject.show', compact('articles'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        dd($id);
      $article = UserArticles::where('id', '=', $id)->update(['is_deleted'=>1]);

      return redirect()->back();
    }

    public function destroy_article($id)
    {
        dd($id);
    }

    public function rejected()
    {
        return view('admin.articles.reject.index');
    }

    public function rejected_list(Request $request)
    {
        $users = UserArticles::query()
            ->where('is_deleted','=',0)
            ->where('is_rejected','=',1);
        return Datatables::of($users)
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/users/articles/reject/' . $user->id) .'" title="Show Reject"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>

                    <form method="POST" action="'. url('/en/admin/users/articles/destroy/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.method_field('DELETE') .'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Admin" onclick="return confirm(&quot;Confirmdelete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('id')) {
                    $query->where('id', '=', request('id'));
                }

                if (request()->filled('title')) {
                    $query->where('title', 'like', "%".request('title')."%");
                }

                if (request()->filled('description')) {
                    $query->where('description', 'like', "%".request('description')."%");
                }

                if (request()->filled('location')) {
                    $query->where('created_at', 'like', "%".request('location')."%");
                }
            })
            ->make(true);
    }

    public function article_rejected(Request $request,$id)
    {
        $user_article = UserArticles::where('id','=',$id)->update(['is_rejected'=>1]);
        Alert::success('Article Is Rejected Successfully !');
        return view('admin.articles.index');
    }

    public function send_print($id)
    {
        $users = Auth::user()->id;
        $find_article = UserArticles::where('id','=',$id)
            ->update(['is_accept'=>1,'accept_by'=>$users]);
        return redirect('/admin/articles/index');
    }

    public function accept_gel_list()
    {

        $users = UserArticles::selectacceptarticle();
        return Datatables::of($users)
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/users/articles/accept/' . $user->id) .'" title="Show Articles"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>

                    <form method="POST" action="'. url('/admin/users/articles/destroy/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.method_field('DELETE') .'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Articles" onclick="return confirm(&quot;Confirmdelete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                    <form method="POST" action="'. url('/admin/users/articles/rejected/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-success btn-sm" title="Reject Articles" onclick="return confirm(&quot;ConfirmRejected ?&quot;)"><i class="fa  fa-toggle-down" aria-hidden="true"></i> </button>
                    </form>
                    <a href="'.url('/admin/users/articles/print/'.$user->id).'" onclick="return confirm(&quot;are you sure Print Articles ?&quot;)" title="send publishing articles"><button class="btn btn-success btn-sm"><i class="fa fa-object-ungroup" aria-hidden="true"></i></button></a>
                    ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('id')) {
                    $query->where('id', '=', request('id'));
                }

                if (request()->filled('title')) {
                    $query->where('title', 'like', "%".request('title')."%");
                }

                if (request()->filled('description')) {
                    $query->where('description', 'like', "%".request('description')."%");
                }

                if (request()->filled('location')) {
                    $query->where('created_at', 'like', "%".request('location')."%");
                }
            })
            ->make(true);

    }

    public function accept_show($id)
    {
        $find_id = UserArticles::findOrFail($id);
        $articles = UserArticles::where('id','=',$id)->first();
        return view('admin.articles.accept.show', compact('articles'));
    }

    public function accept_articles()
    {
        return view('admin.articles.accept.index');
    }

    public function all_articles_get_list(Request $request)
    {
        $users = UserArticles::nodraft()
        ->where('is_deleted','=',0)
        ;
        return Datatables::of($users)
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/users/articles/all/' . $user->id) .'" title="Show Article"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>

                    <form method="POST" action="'. url('/admin/users/articles/destroy/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.method_field('DELETE') .'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Articles" onclick="return confirm(&quot;Confirmdelete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                    <form method="POST" action="'. url('/admin/users/articles/rejected/' . $user->id) .'" accept-charset="UTF-8" style="display:inline">
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-success btn-sm" title="Reject Articles" onclick="return confirm(&quot;ConfirmRejected ?&quot;)"><i class="fa  fa-toggle-down" aria-hidden="true"></i> </button>
                    </form>
                    <a href="'.url('/admin/users/articles/print/'.$user->id).'" onclick="return confirm(&quot;are you sure Print Articles ?&quot;)" title="send publishing articles"><button class="btn btn-success btn-sm"><i class="fa fa-object-ungroup" aria-hidden="true"></i></button></a>
                    ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('id')) {
                    $query->where('id', '=', request('id'));
                }

                if (request()->filled('title')) {
                    $query->where('title', 'like', "%".request('title')."%");
                }

                if (request()->filled('description')) {
                    $query->where('description', 'like', "%".request('description')."%");
                }

                if (request()->filled('location')) {
                    $query->where('created_at', 'like', "%".request('location')."%");
                }
            })
            ->make(true);
    }

    public function all_show($id)
    {
        $find_id = UserArticles::findOrFail($id);
        $articles = UserArticles::where('id','=',$id)->first();
        return view('admin.articles.all.show', compact('articles'));
    }

    public function all_atricles()
    {
        return view('admin.articles.all.index');
    }

}
