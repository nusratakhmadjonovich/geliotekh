<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Groups;
use App\Models\Admin\GroupsPermissions;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Admin\Permissions;
use Yajra\Datatables\Datatables;


class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, User $user)
    {

        $this->authorize('read', Groups::class);
        return view('admin.groups.index');
    }

    public function get_list(Request $request)
    {
        $groups = Groups::nodraft();

        return Datatables::of($groups)
            ->addColumn('actions', function ($group) {
                $actions = '<a href="'. url('/admin/groups/' . $group->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>';

                if($group->id != 1){
                    $actions .= '
                        <form method="POST" action="'. url('/admin/groups' . '/' . $group->id).'" accept-charset="UTF-8" style="display:inline">
                            '.method_field('DELETE') .'
                            '.csrf_field() .'
                            <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                        </form>
                    ';
                };
                return $actions;
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('name')) {
                    $query->where('name', 'like', "%".request('name')."%");
                }
            })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */

    public function create()
    {
        $group = new Groups;
        $group->status = self::STATUS_DRAFT;
        $group->save();

        return redirect('/admin/groups/'.$group->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required|max:255'
		]);

        $requestData = $request->all();

        Groups::create($requestData);

        return redirect('admin/groups')->with('flash_message', 'Admin added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $group = Groups::findOrFail($id);

        return view('admin.groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $group = Groups::findOrFail($id);
        $isNewRecord = (int)($group->status == -1);
        $permissions = Permissions::all_permissions();
        $selected_permissions = GroupsPermissions::selected_permissions($id);
        return view('admin.groups.edit', compact(['group', 'isNewRecord', 'permissions', 'selected_permissions']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);

        $requestData = $request->all();

        $group = Groups::findOrFail($id);
        $group->update($requestData);

        $group->permissions()->sync($request->permissions);

        return redirect('admin/groups')->with('flash_message', 'Admin updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Groups::where('id', '=', $id)->update(['is_deleted' => 1]);
        return redirect('admin/groups')->with('flash_message', 'Admin deleted!');
    }
}
