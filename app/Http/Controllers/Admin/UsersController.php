<?php


namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Libraries\FileManager;
use App\Libraries\UploadManager;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;

class UsersController extends Controller
{
    public function index()
    {
        return view('admin.users.index');
    }

    public function get_list(Request $request, $type=null)
    {
        $filters = $request->get('filter');
        $users = User::nodraft()->nodeleted()->users();

        return Datatables::of($users)
            ->editColumn('is_editor', '@if($is_editor) Да @else Нет @endif')
            ->editColumn('is_reviewer', '@if($is_reviewer) Да @else Нет @endif')

            ->addColumn('status', function ($user) {
                if($user->status == 1)
                    return '<span class="label label-success">Актив</span>';
                elseif($user->status == -2)
                    return '<span class="label label-warning">Не потдвержден</span>';
                elseif($user->status == 0)
                    return '<span class="label label-danger">Неактив</span>';
            })
            ->addColumn('actions', function ($user) {
                return '
                <a href="'. url('/admin/users/' . $user->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                        
                <form method="POST" action="'. url('/admin/users' . '/' . $user->id).'" accept-charset="UTF-8" style="display:inline">
                    '.method_field("DELETE").'
                            '.csrf_field() .'
                    <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirmdelete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                </form>
            ';
            })
            ->rawColumns(['actions', 'status'])
            ->filter(function ($query) {
                if (request()->filled('status')) {
                    $query->where('status', '=', request('status'));
                }
                if (request()->filled('fio')) {
                    $query->where('fio', 'like', "%".request('fio')."%");
                }

                if (request()->filled('email')) {
                    $query->where('email', 'like', "%".request('email')."%");
                }
                if (request()->filled('is_editor')) {
                    $query->where('is_editor', '=', "1");
                }
                if (request()->filled('is_reviewer')) {
                    $query->where('is_reviewer', '=', "1");
                }
            })
            ->make(true);
    }


    public function create()
    {
        $user = new User;
        $user->role_id = Role::ROLE_USER;
        $user->status = self::STATUS_DRAFT;
        $user->created_by  = Auth::user()->id;
        $user->save();

        return redirect('admin/users/'.$user->id.'/edit/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $isNewRecord = (int)($user->status == -1);
        return view('admin.users.edit', compact(['user', 'isNewRecord']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'fio' => 'required|max:255',
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('users')->ignore($user->id)->where(function ($query) {
                    $query->whereIn('status', [1, 0]);
                    $query->where('is_deleted', '=', 0);
                }),
            ],
            'password' => 'nullable|string|min:6|confirmed',
            'photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required'
        ]);

        $requestData = $request->all();

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('profile', $user->id, $request->file('photo'), $user->photo);
        }

        if($request->password) {
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }

        if(!$request->is_editor){
            $requestData['is_editors_chairman'] = 0;
            $requestData['editor_title'] = NULL;
        }


        $user->update($requestData);

        return redirect('admin/users')->with('success-message', 'Данные успешно обновлено!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findorfail($id);

        return view('admin.users.show', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        User::where('id', '=', $id)->update(['is_deleted' => 1]);
        return redirect('admin/users')->with('success-message', 'Пользователь успесно удалено!');
    }

    public function removePhoto($id){
        $user = User::findOrFail($id);
        if($user->photo) {
            UploadManager::delete('profile', $user->id, $user->photo);
            $user->update(['photo' => '']);
        }
        return back()->with('success-message', 'Фотография удалено успешно');
    }
}
