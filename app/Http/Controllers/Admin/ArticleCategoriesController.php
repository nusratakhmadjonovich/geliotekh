<?php
namespace App\Http\Controllers\Admin;

use App\Libraries\UploadManager;
use App\Models\Admin\ArticleCategories;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\ArticleCategoriesI18n;
use Illuminate\Http\Request;
use App\Models\Role;
use Yajra\Datatables\Datatables;
use Alert;


class ArticleCategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    public function get_list()
    {
        $categories = ArticleCategories::nodraft();

        return Datatables::of($categories)
            ->addColumn('actions', function ($category) {
                return '
                    <a href="'. url('/admin/categories/' . $category->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('name_ru')) {
                    $query->where('name_ru', 'like', "%".request('name_ru')."%");
                }

                if (request()->filled('name_en')) {
                    $query->where('name_en', 'like', "%".request('name_en')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $category = new ArticleCategories;
        $category->status = self::STATUS_DRAFT;
        $category->save();

        return redirect('/admin/categories/'.$category->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $category = ArticleCategories::findOrFail($id);
        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, $lang = 'ru')
    {
        $category = ArticleCategories::findOrFail($id);
        $isNewRecord = (int)($category->status == -1);
        return view('admin.categories.edit', compact(['category', 'isNewRecord']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $category = ArticleCategories::findOrFail($id);

        $this->validate($request, [
            'name_ru' => 'required|max:255',
            'name_en'=>'required|max:255',
            'description_ru'=>'required',
            'description_en'=>'required',
        ]);

        $requestData = $request->all();
        $requestData['status'] = self::STATUS_ACTIVE;

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('categories', $category->id,  $request->file('photo'), $category->photo);
        }

        $category->update($requestData);
        return redirect('admin/categories')->with('flash_message', 'Category updated!');
    }

    public function removeFile($id)
    {
        $removeFile = ArticleCategories::findorfail($id);
        if($removeFile->photo) {
            UploadManager::delete('categories', $removeFile->id, $removeFile->photo);
            $removeFile->update(['photo' => '']);
        }
        return back()->with('success-message', 'Фотография удалено успешно');
    }
}
