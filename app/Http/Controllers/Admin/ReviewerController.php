<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\User_Article;
use Illuminate\Http\Request;
use App\Models\Reviewer_Response;
use App\Http\Controllers\Controller;
use App\Models\Reviewer_Article;
use App\Models\Admin\Administrators;
use App\Models\User_Article_Meta;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Alert;
use Illuminate\Support\Str;


class ReviewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_list(Request $request)
    {
        $users = User_Article::nodraft()
            ->where('status','!=',-1)
            ->orderBy('id','DESC');
        return Datatables::of($users)
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/send/revievwer/' . $user->id) .'" title="View Admin"><button class="btn btn-success btn-sm"><i class="fa fa-send" aria-hidden="true"></i></button></a>
                    <a href="'. url('admin/reviewers/'.$user->id.'/edit') .'" title="Edit Models\Admin"><button class="btn btn-primary btn-sm"><i class="fa fa-send-o" aria-hidden="true"></i> </button></a>
                    <form method="POST" action="'. url('/admin/journal/edition/'.$user->id).'" accept-charset="UTF-8" style="display:inline">
                        '.method_field("DELETE").'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Admin" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('fio')) {
                    $query->where('fio', 'like', "%".request('fio')."%");
                }

                if (request()->filled('email')) {
                    $query->where('email', 'like', "%".request('email')."%");
                }
            })
            ->make(true);
    }



    public function index()
    {
        return view('admin.reviwers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $review = new Reviewer_Article();
        $review->status = -1;
        $review->save();
        return redirect('/admin/reviewers/'.$review->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $find_id = Reviewer_Article::findorfail($id);
        return view('admin.reviwers.show',['find_reviewer'=>$find_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_reviewer = User_Article::findorfail($id);
        $find_users = User::all()->where('role_id','=',2);
        $reviewer = Reviewer_Article::where('article_meta_id','=',$id)->get();
        $latest_meta_file = User_Article_Meta::where('article_id','=',$id)
            ->orderBy('id','DESC')->first();
        return view('admin.articles.review.edit',['edit_reviewer'=>$edit_reviewer,'find_users'=>$find_users,'reviewer'=>$reviewer,'latest_meta_file'=>$latest_meta_file]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $reviewer = User_Article_Meta::find($id);
            $reviewer->reviewer_user()->sync($request->users);
            $find_reviewer = Reviewer_Article::where('article_meta_id','=',$id)->update(['cheking_and_date'=>$request->cheking_date]);
         return redirect('admin/reviewers/'.$id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send($id)
    {
        $find_review = User_Article::where('id','=',$id)->first();
        $find_review_file = Reviewer_Article::where('article_meta_id','=',$find_review->article_file->id)->get();
        $find_users = User::all()->where('role_id','=',2);
        return view('admin.reviwers.store',compact('find_users','find_review','find_review_file'));
    }

    public function patch_update($id,Request $request)
    {


        foreach ($request->users as $reviewerlar)
        {
            $reviewer = new Reviewer_Article();
            $reviewer->article_meta_id = $request->article_meta_id;
            $reviewer->user_id = $reviewerlar;
            $reviewer->title = "NULL";
            $reviewer->cheking_and_date = $request->check_and_date;
            $reviewer->save();
                $find_user = User::find($reviewerlar);
                Reviewer_Article::send_each_article($find_user->email);
        }

        return redirect('admin/reviewers');
    }

    public function info_index()
    {

        $article_meta = User_Article_Meta::all()
            ->where('status','!=',-1);
        return view('admin.articles_infoes.index',['article_meta'=>$article_meta]);
    }

    public function info_show($id)
    {
        $reviewer_response = Reviewer_Response::find($id);
        $find_user = Administrators::where('id','=',$reviewer_response->user_id)->first();
        return view('admin.articles_infoes.show',['reviewer_response'=>$reviewer_response,'find_user'=>$find_user]);
    }
}
