<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Blocks;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class BlocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.blocks.index');
    }

    public function get_list(Request $request)
    {
        $blocks = Blocks::active();

        return Datatables::of($blocks)
            ->addColumn('actions', function ($block) {
                $actions = '<a href="'. url('/admin/blocks/' . $block->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>';

                return $actions;
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('block_key')) {
                    $query->where('block_key', 'like', "%".request('block_key')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */

    public function create()
    {
        $block = new Blocks();
        $block->status = self::STATUS_DRAFT;
        $block->save();

        return redirect('/admin/blocks/'.$block->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $group = Blocks::findOrFail($id);

        return view('admin.blocks.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $block = Blocks::findOrFail($id);
        $isNewRecord = (int)($block->status == -1);
        return view('admin.blocks.edit', compact(['block', 'isNewRecord']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'block_key' => 'required|max:255'
        ]);

        $block = Blocks::findOrFail($id);

        $requestData = $request->all();
        $requestData['status'] = self::STATUS_ACTIVE;
        $block->update($requestData);

        return redirect('admin/blocks')->with('flash_message', 'Admin updated!');
    }
}
