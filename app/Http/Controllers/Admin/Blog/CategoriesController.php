<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Article_Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Categories;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Datatables;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.blog.categories.index');
    }

    public function get_list(Request $request)
    {
        $categories = Categories::active();

        return Datatables::of($categories)
            ->addColumn('image',function ($image){
                return "<img src='".URL::to('/img1/')."/article.jpg"."'>";
            })
            ->addColumn('actions', function ($category) {
                return '
                    <a href="'. url('/admin/blog/categories/' . $category->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                ';
            })
            ->rawColumns(['actions'])
            ->filter(function ($query) {
                if (request()->filled('slug')) {
                    $query->where('slug', 'like', "%".request('slug')."%");
                }
            })
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Categories();
        $category->status = self::STATUS_DRAFT;
        $category->save();
        return redirect('admin/blog/categories/'.$category->id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Categories::findorfail($id);
        $isNewRecord = (int)($category->status == self::STATUS_DRAFT);
        return view('admin.blog.categories.edit', compact(['category','isNewRecord']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'slug' => 'required|max:255',
            'name_ru'=>'required',
            'name_en'=>'required'
        ]);
        $category = Categories::findorfail($id);
        $category->status = self::STATUS_ACTIVE;
        $category->slug = $request->slug;
        $category->name_ru = $request->name_ru;
        $category->name_en = $request->name_en;
        $category->save();
        return redirect('/admin/blog/categories/' . $category->id . '/edit')->with('success-message', 'Thanks!') ;

    }
}
