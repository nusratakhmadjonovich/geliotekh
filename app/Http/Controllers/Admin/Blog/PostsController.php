<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Libraries\UploadManager;
use Illuminate\Http\Request;
use App\Models\Admin\Blog\Posts;
use App\Http\Controllers\Controller;
use App\Models\Admin\Blog\Categories;
use Yajra\Datatables\Datatables;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.blog.posts.index');
    }

    public function get_list(Request $request)
    {
        $posts = Posts::nodraft()->get();
        return Datatables::of($posts)
            ->addColumn('status', function ($user) {
                if($user->status == 1)
                    return '<span class="label label-success">Актив</span>';
                else
                    return '<span class="label label-danger">Неактив</span>';
            })
            ->addColumn('actions', function ($post) {
                return '
                    <a href="'. url('/admin/blog/posts/' . $post->id) .'" title="Показать"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                    <a href="'. url('/admin/blog/posts/' . $post->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                   
                ';
            })
            ->rawColumns(['actions', 'status'])
            ->filter(function ($query) {
                if (request()->filled('title_ru')) {
                    $query->where('title_ru', 'like', "%".request('title_ru')."%");
                }
            })
            ->make(true);
    }

    public function create()
    {
        $post = new Posts();
        $post->status = self::STATUS_DRAFT;
        $post->save();
        return redirect('admin/blog/posts/'.$post->id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Posts::findorfail($id);
        $isNewRecord = (int)($post->status == self::STATUS_DRAFT);
        $categories = Categories::active()->get();

        return view('admin.blog.posts.edit', ['categories' => $categories, 'post' => $post, 'isNewRecord' => $isNewRecord] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required|integer',
            'title_ru' => 'required|string|max:255',
            'anons_ru' => 'required'
        ]);

        $post = Posts::findOrFail($id);
        $requestData = $request->all();
        $requestData['publish_at'] = date('Y-m-d H:i:s', strtotime($request->publish_at));

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('blog_post', $post->id,  $request->file('photo'), $post->photo);
        }

        $post->update($requestData);

        return redirect('/admin/blog/posts/' . $post->id . '/edit')->with('success-message', 'Thanks!') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = Posts::findOrFail($id);

        return view('admin.blog.posts.show', compact('post'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Posts::where('id', '=', $id)->update(['deleted_at' => date('c')]);
        return redirect('admin/blog/posts')->with('flash_message', 'Admin deleted!');
    }

    public function removeFile($id)
    {
        $removeFile = Posts::findorfail($id);
        if($removeFile->photo) {
            UploadManager::delete('blog_post', $removeFile->id, $removeFile->photo);
            $removeFile->update(['photo' => '']);
        }
        return back()->with('success-message', 'Фотография удалено успешно');
    }
}
