<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

use App\Libraries\FileManager;
use App\Libraries\UploadManager;
use App\Models\Admin\Administrators;
use App\Models\Admin\Groups;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;

class AdministratorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
         return view('admin.admins.index');
    }

    public function get_list(Request $request)
    {
        $users = Administrators::nodraft()->nodeleted()->admins();

        return Datatables::of($users)
            ->addColumn('status', function ($user) {
                if($user->status == 1)
                    return '<span class="label label-success">Актив</span>';
                else
                    return '<span class="label label-danger">Неактив</span>';
            })
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/admins/' . $user->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    
                    <form method="POST" action="'. url('/admin/admins' . '/' . $user->id).'" accept-charset="UTF-8" style="display:inline">
                        '.method_field("DELETE").'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->rawColumns(['actions', 'status'])
            ->filter(function ($query) {
                if (request()->filled('status')) {
                    $query->where('status', '=', request('status'));
                }

                if (request()->filled('fio')) {
                    $query->where('fio', 'like', "%".request('fio')."%");
                }

                if (request()->filled('email')) {
                    $query->where('email', 'like', "%".request('email')."%");
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = new Administrators;
        $user->role_id = Role::ROLE_ADMIN;
        $user->status = self::STATUS_DRAFT;
        $user->created_by  = Auth::user()->id;
        $user->save();

        return redirect('/admin/admins/'.$user->id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $admin = Administrators::findOrFail($id);
        $isNewRecord = (int)($admin->status == self::STATUS_DRAFT);
        $groups = Groups::nodraft()->get();

        return view('admin.admins.edit', compact(['admin', 'isNewRecord', 'groups']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $admin = Administrators::findOrFail($id);

        $this->validate($request, [
			'fio' => 'required|string|max:255',
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('users')->ignore($admin->id)->where(function ($query) {
                    $query->whereIn('status', [1, 0]);
                    $query->where('is_deleted', '=', 0);
                }),
            ],
            'password' => 'nullable|string|min:6|confirmed',
            'photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required'
		]);

        $requestData = $request->all();

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('profile', $admin->id, $request->file('photo'), $admin->photo);
        }

        if($request->password) {
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }

        $admin->update($requestData);
        $admin->groups()->sync($request->group_id);

        return redirect('admin/admins')->with('success-message', 'Данные успешно обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Administrators::where('id', '=', $id)->update(['is_deleted' => 1]);
        return redirect('admin/admins')->with('success-message', 'Администратор успешно удалено!');
    }

    public function removePhoto($id){
        $admin = Administrators::findOrFail($id);
        if($admin->photo) {
            UploadManager::delete('profile', $admin->id, $admin->photo);
            $admin->update(['photo' => '']);
        }
        return back()->with('success-message', 'Фотография удалено успешно');
    }
}
