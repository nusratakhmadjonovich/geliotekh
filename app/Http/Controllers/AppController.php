<?php

namespace App\Http\Controllers;

use App\Models\Admin\Administrators;

use App\Models\Site\ArticleCategories;
use App\Models\Site\Articles;
use App\Models\Site\Posts;
use App\Models\Reviewer_Article;
use App\Models\Reviewer_Response;
use App\Models\Sub_Author;
use Illuminate\Http\Request;
use App\Models\Article_Category;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function index()
    {
        $categories = ArticleCategories::active()
            ->where('is_deleted','!=',1)
            ->limit(6)
            ->orderBy('created_at', 'ASC')
            ->get()
        ;
        $news = Posts::published()->where('category_id', '=', 1)->limit(2)->get();
        $events = Posts::active()->where('category_id', '=', 2)->limit(6)->get();
        $most_cited_articles = Articles::active()->mostCited()->limit(4)->orderBy('id', 'DESC')->get();
        return view('index',['categories'=>$categories, 'news'=>$news, 'events'=>$events, 'most_cited_articles'=>$most_cited_articles]);
    }

    public function articles()
    {
        $select_categories = ArticleCategories::where('status','!=',-1)
        ->where('is_deleted','!=',1)
        ->get();
        return view('members.article.index',compact('select_categories'));
    }

    public function user_show_articles()
    {
        $user = Administrators::findorfail(Auth::user()->id);
        $review = Reviewer_Article::where('user_id','=',$user->id)->get();
        $review_response = Reviewer_Response::where('user_id','=',$user->id)->get();
        return view('reviewers.index',['review'=>$review,'user_id'=>$user->id,'review_response'=>$review_response]);
    }

    public function user_reviewer_ideas(Request $request,$user_id,$article_meta_id)
    {
        $this->validate($request,[
            'comment'=> 'required|min:16',

        ]);
        $reviewer = Reviewer_Response::where('user_id','=',$user_id)
            ->where('article_meta_id','=',$article_meta_id)
            ->first();
        if (empty($reviewer))
        {
            $reviewer_response = new Reviewer_Response();
            $reviewer_response->article_meta_id = $article_meta_id;
            $reviewer_response->user_id = $user_id;
            $reviewer_response->result = $request->result;
            $reviewer_response->comment = $request->comment;
            $reviewer_response->save();

        }else
        {
            $reviewer = Reviewer_Response::where('user_id','=',$user_id)
                ->where('article_meta_id','=',$article_meta_id)->update(['result'=>$request->result,'comment'=>$request->comment]);
        }

        return redirect()->back()->with('success-message', 'Thanks!') ;

    }

    public function user_articles_index()
    {
        return view('reviewers.index');
    }

    public function list_group_item($id)
    {
        $user = Administrators::findorfail(Auth::user()->id);
        $article_meta = Reviewer_Article::findorfail($id);
        $find_authors = Sub_Author::where('article_id','=',$article_meta->article_meta_id)->get();
        return view('reviewers.show',['article_meta'=>$article_meta,'user_id'=>$user->id,'find_authors'=>$find_authors]);
    }

    public function signout()
    {
        Auth::logout();
        return redirect('/');
    }

    private function orderBy($string, $string1)
    {
    }

}
