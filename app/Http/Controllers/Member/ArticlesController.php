<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Alert;
use App\Libraries\UploadManager;
use App\Models\Admin\ArticleCategories;
use App\Models\User;
use App\Models\Sub_Author;
use App\Models\Members\UserArticles;
use App\Models\User_Article_Meta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ArticlesController extends Controller
{

    public function demo()
    {
        $user_articles = new UserArticles();
        $user_articles->status = -1;
        $user_articles->save();
        return redirect('/member/create/article/'.$user_articles->id);
    }

    public function articles($id)
    {
        $find_user_article = UserArticles::findorfail($id);
        $select_categories = ArticleCategories::where('status','!=',-1)
            ->where('is_deleted','!=',1)
            ->get();
        return view('members.article.index',compact(['select_categories','find_user_article']));
    }

    public  function saveFile(Request $request){
        dd($request);
        $this->validate($request,[
            'title' => 'required|max:255',
            'description'=>'required|max:350',
            'author_name'=>'required',
            'file'=> 'required|max:10000',
            'author_email'=>'required|email'
        ]);


        if ($request->checked == true)
        {
           /* $file = $request->file('file')->getClientOriginalName();
            $input = public_path('file').'/'.$file;
            $request->file('file')->move(public_path('file'), $request->file('file')->getClientOriginalName());
           */
            $articles = new UserArticle();
            if ($request->file('file')) {
                $requestData['file'] = UploadManager::upload('user_article', Auth::user()->id, $request->file('file'), $article->file);
            }
            $articles->category_id = $request->category_id;
            $articles->title = $request->title;
            $articles->description = $request->description;
            $articles->user_id = Auth::user()->id;
            $articles->is_rejected = 0;
            $articles->rejected_at = time();
            $articles->is_deleted = 0;
            $articles->save();
            $sub_article_meta = new User_Article_Meta();
            $sub_article_meta->article_id = $articles->id;
            $sub_article_meta->file =  Str::substr($articles->path,7);
            $sub_article_meta->comment = $articles->title;
            $sub_article_meta->is_receive = 0;
            $sub_article_meta->save();

            $main_author = new Sub_Author();
            $main_author->fio = $request->author_name;
            $main_author->article_id = $articles->id;
            $main_author->email = $request->author_email;
            $main_author->phone = "6786695222222";
            $main_author->is_main_author = 1;
            $main_author->save();

            if (count($request->authors) > 0) {
                foreach($request->authors as $key=>$author){
                    if (empty($author['name']) || empty($author['email']))
                    {
                        return redirect()->back();
                    }else
                    {

                        $sub_author = new Sub_Author();
                        $sub_author->fio = $author['name'];
                        $sub_author->email = $author['email'];
                        $sub_author->article_id = $articles->id;
                        $sub_author->phone = "78945678";
                        $sub_author->is_main_author = 0;
                        $sub_author->save();
                    }
                }
            }
        }else
        {
            return redirect()->back();
        }

        return redirect()->route('member.create.article');
    }

    public function getShowArticles()
    {
           Alert::success('Good job!');
        $s = Auth::user()->id;
        $show = User::findorfail($s);
        return view('user.showPostFromOwnUsers',compact('show'));
    }

    public function history()
    {
        $articles = UserArticles::where('user_id','=',Auth::user()->id)->orderBy('created_at','DESC')->get();
        return view('article.history',['articles'=>$articles]);
    }

    public function read($id)
    {
            dd(User_Article::findorfail($id));
    }

    public function send($id)
    {
        $article = User_Article::findorfail($id);
        return view('article.send',['article'=>$article]);
    }

    public function send_file(Request $request ,$id)
    {
        $this->validate($request,[
            'file'=>'required'
        ]);
        $file = $request->file('file')->getClientOriginalName();
        $input = public_path('file').'/'.$file;
        $request->file('file')->move(public_path('file'), $request->file('file')->getClientOriginalName());
        $find_article = User_Article::where('id','=',$id)->first();
        $find_article_meta = User_Article_Meta::where('article_id','=',$id)->first();

        $find_article->path = "/file/".$file;
        $find_article->is_rejected = 0;
        $find_article->update();

        $article_meta = new User_Article_Meta();
        $article_meta->article_id = $id;
        $article_meta->file = Str::substr($find_article->path,7);
        $article_meta->comment = $find_article_meta->comment;
        $article_meta->is_receive = '0';
        $article_meta->is_deleted = 0;
        $article_meta->status = 0;
        $article_meta->save();
        return redirect('user/article/send/'.$id)->with('success-message','send articles file');
    }
}
