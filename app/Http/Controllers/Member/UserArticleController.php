<?php

namespace App\Http\Controllers\Member;

use App\Libraries\UploadManager;
use App\Models\Admin\ArticleCategories;
use App\Models\Members\SubAuthor;
use App\Models\Members\UserArticleMeta;
use App\Models\Members\UserArticles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new UserArticles();
        $article->status = -1;
        $article->save();
        return redirect('/member/article/'.$article->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = UserArticles:: findorfail($id);
        $find_authors = SubAuthor::where('article_id','=',$id)->get();
        return view('members.article.show',compact(['article','find_authors']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $find_article = UserArticles::findorfail($id);
        $select_categories = ArticleCategories::where('status','!=',-1)
            ->where('is_deleted','!=',1)
            ->get();
        return view('members.article.index',compact(['select_categories','find_article']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'description'=>'required|max:350',
            'author_name'=>'required',
            'file'=> 'required|max:10000',
            'author_email'=>'required|email'
        ]);

        if ($request->checked == true)
        {
            $find_article = UserArticles::findorfail($id);
            if ($request->file('file')) {
             $find_article->path= UploadManager::upload('user_article', $find_article->id, $request->file('file'), $find_article->file);
            }
            $find_article->category_id = $request->category_id;
            $find_article->title = $request->title;
            $find_article->description = $request->description;
            $find_article->user_id = Auth::user()->id;
            $find_article->is_rejected = 0;
            $find_article->status = self::STATUS_ACTIVE;
            $find_article->rejected_at = time();
            $find_article->is_deleted = 0;
            $find_article->update();

            $sub_article_meta = new UserArticleMeta();
            $sub_article_meta->article_id = $find_article->id;
            $sub_article_meta->file =  UploadManager::upload('user_article', $find_article->id, $request->file('file'), $find_article->file);
            $sub_article_meta->comment = $find_article->title;
            $sub_article_meta->is_receive = 0;
            $sub_article_meta->save();

            $main_author = new SubAuthor();
            $main_author->fio = $request->author_name;
            $main_author->article_id = $find_article->id;
            $main_author->email = $request->author_email;
            $main_author->phone = "6786695222222";
            $main_author->is_main_author = 1;
            $main_author->save();

            if (count($request->authors) > 0) {
                foreach($request->authors as $key=>$author){
                    if (empty($author['name']) || empty($author['email']))
                    {
                        return redirect()->back();
                    }else
                    {

                        $sub_author = new SubAuthor();
                        $sub_author->fio = $author['name'];
                        $sub_author->email = $author['email'];
                        $sub_author->article_id = $find_article->id;
                        $sub_author->phone = "78945678";
                        $sub_author->is_main_author = 0;
                        $sub_author->save();
                    }
                }
            }
        }else
        {
            return redirect()->back();
        }

        return redirect('/member/article/'.$find_article->id)->with('success-message','Эта статья успешно загружена !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function history()
    {
        $articles = UserArticles::where('user_id','=',Auth::user()->id)->orderBy('created_at','DESC')->get();
        return view('members.article.history',['articles'=>$articles]);
    }
}
