<?php
    if (! function_exists('lang_url')) {
        function lang_url($url) {
            return \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getLocalizedURL(null, $url);
        }
    }
?>