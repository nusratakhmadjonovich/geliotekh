<?php

namespace App\Models\Site;

use App\Models\MainModel;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Posts extends MainModel
{
    protected $table = 'blog_posts';

    public function scopeNodraft($query)
    {
        return $query->where('status', '!=', self::STATUS_DRAFT)
            ->whereNull('deleted_at');
    }

    public function scopePublished($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE)
            ->where('publish_at', '<', date('c'))
            ->whereNull('deleted_at');
    }

    public function scopeActive($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at');
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Admin\Blog\Categories');
    }

    public function getTitleAttribute()
    {
        $title = 'title_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

    public function getAnonsAttribute()
    {
        $title = 'anons_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

    public function getDescriptionAttribute()
    {
        $title = 'description_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }
}
