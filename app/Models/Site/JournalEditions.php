<?php

namespace App\Models\Site;

use App\Models\MainModel;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class JournalEditions extends MainModel
{
    protected $table = 'journal_editions';

    public function scopeActive($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE);
    }

    public function getTitleAttribute()
    {
        return $this->attributes['year'].' ('.$this->attributes['number'].')';
    }

    public function getDescriptionAttribute()
    {
        $description = 'description_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$description];
    }


    public function journal()
    {
        return $this->belongsTo('App\Models\Admin\Journals','journal_id','id');
    }

    public function articles()
    {
        return $this->hasMany('App\Models\Site\Articles', 'journal_edition_id');
    }
}