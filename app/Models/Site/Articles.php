<?php

namespace App\Models\Site;

use App\Models\MainModel;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Articles extends MainModel
{
    protected $table = 'journal_edition_articles';

    public function scopeActive($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE);
    }

    public function scopeMostCited($query)
    {
        return $query->where('most_cited', '=', 1);
    }

    public function edition()
    {
        return $this->belongsTo('App\Models\Site\JournalEditions', 'journal_edition_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Site\ArticleCategories', 'article_category_id');
    }

    public function getTitleAttribute()
    {
        $title = 'title_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

    public function getDescriptionAttribute()
    {
        $title = 'description_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }
}
