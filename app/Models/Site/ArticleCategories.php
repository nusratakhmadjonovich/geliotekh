<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ArticleCategories extends Model
{
    protected $table = 'article_categories';

    public function scopeActive($query)
    {
        return $query->where('status', '=', 1);
    }

    public function articles()
    {
        return $this->hasMany('App\Models\Site\Articles', 'article_category_id');
    }

    public function getNameAttribute()
    {
        $title = 'name_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

    public function getDescriptionAttribute()
    {
        $title = 'description_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }
}
