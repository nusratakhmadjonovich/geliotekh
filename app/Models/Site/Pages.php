<?php

namespace App\Models\Site;

use App\Models\MainModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Pages extends MainModel
{
    use SoftDeletes;

    protected $table = 'pages';

    protected $fillable = [
        'title_ru',
        'title_en',
        'description_ru',
        'description_en',
        'status',
        'publish_at'
    ];

    public function scopePublished($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at');
    }

    public function getTitleAttribute()
    {
        $title = 'title_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

    public function getDescriptionAttribute()
    {
        $title = 'description_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }
}
