<?php

namespace App\Models;

use App\Models\Admin\Groups;
use App\Models\Admin\GroupsPermissions;
use App\Models\Admin\Permissions;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    public function scopeNoDraft($query)
    {
        return $query->where('status', '!=', -1);
    }
    public function articles()
    {
        return $this->hasMany('App\Models\Members\UserArticles','user_id');
    }
    public function scopeNoDeleted($query)
    {
        return $query->where('is_deleted', '=', 0);
    }

    public function scopeUsers($query){
        return $query->where('role_id', '=', Role::ROLE_USER);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fio', 'email','lang','password', 'role_id', 'is_editor', 'is_reviewer', 'is_editors_chairman', 'editor_title', 'status', 'photo', 'verify_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];


    public function isAdmin()
    {
        return $this->role_id == Role::ROLE_ADMIN;
    }

    public function isUser()
    {
        return $this->role_id == Role::ROLE_USER;
    }

    public function hasPermission($permission, $requireAll = false)
    {
        /*if (is_array($permission)) {
            foreach ($permission as $permissionName) {
                $hasPermission = $this->hasPermission($permissionName);

                if ($hasPermission && !$requireAll) {
                    return true;
                } elseif (!$hasPermission && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the permissions were found.
            // If we've made it this far and $requireAll is TRUE, then ALL of the permissions were found.
            // Return the value of $requireAll.
            return $requireAll;
        }*/

        return Groups::hasGroupPermission($permission);
    }

    //relations
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Models\Admin\Groups','rbac_users_groups_rel', 'user_id', 'group_id');
    }

   /* public function articles()
    {
        return $this->hasMany('App\Models\Members\UserArticle','user_id','id');
    }*/

    //auto boot
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->created_by = (Auth::check() ? Auth::user()->id : NULL);
        });

        self::created(function($model){
            $model->created_at = time();
        });

        self::updating(function($model){
            $model->updated_by = (Auth::check() ? Auth::user()->id : NULL);
        });

        self::updated(function($model){
            $model->updated_at = time();
        });

        self::deleting(function($model){
            $model->is_deleted = 1;
            $model->deleted_by = (Auth::check() ? Auth::user()->id : NULL);
        });

        self::deleted(function($model){
            $model->deleted_at = time();
        });
    }
}
