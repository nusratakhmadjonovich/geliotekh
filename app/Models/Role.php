<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;

    public static function lists($string, $string1)
    {

    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User','user_role','role_id','user_id');
    }
}
