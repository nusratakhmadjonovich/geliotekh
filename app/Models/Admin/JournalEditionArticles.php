<?php

namespace App\Models\Admin;

use App\Models\MainModel;

class JournalEditionArticles extends MainModel
{
    protected $table = 'journal_edition_articles';

    protected $fillable = [
        'title_ru', 'title_en', 'description_ru', 'description_en', 'photo', 'file', 'status', 'is_chargerable', 'most_cited', 'journal_edition_id', 'article_category_id', 'authors','keywords_ru','keywords_en'
    ];

    public function scopeNodraft($query)
    {
        return $query->where('status', '!=', self::STATUS_DRAFT);
    }

    public function journal_edition()
    {
        return $this->belongsTo('App\Models\Admin\JournalEditions');
    }
}
