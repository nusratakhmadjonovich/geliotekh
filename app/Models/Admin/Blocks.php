<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Blocks extends Model
{
    protected $table = 'blocks';

    protected $softDelete = true;
    public $timestamps = false;
    protected $fillable = ['block_key', 'block_data_ru', 'block_data_en','status'];

    public function scopeActive($query)
    {
        return $query->where('status', '!=', -1);
    }
}
