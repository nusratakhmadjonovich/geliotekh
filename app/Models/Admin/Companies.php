<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $table = 'companies';

    const STATUS_DRAFT = -1;

    protected $fillable = [
        'name','description','start_date','end_date','status','is_deleted','ip_address'
    ];

    protected $softDelete = true;

    public function scopeNoActive($query)
    {
        return $query->where('status', '!=', -1)->where('is_deleted', '!=', 0);
    }

    public function scopeNoDraft($query)
    {
        return $query->where('status', '!=',Companies::STATUS_DRAFT);
    }

    public function scopeNoDeleted($query)
    {
        return $query->where('is_deleted', '=', 0);
    }


}
