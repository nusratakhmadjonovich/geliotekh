<?php

namespace App\Models\Admin\Blog;

use App\Models\MainModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends MainModel
{
    use SoftDeletes;

    protected $table = 'blog_category';

    public function scopeActive($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE);
    }
}
