<?php

namespace App\Models\Admin\Blog;

use App\Models\MainModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Posts extends MainModel
{
    use SoftDeletes;

    protected $table = 'blog_posts';

    protected $dates = ['publish_at'];

    protected $fillable = [
        'category_id',
        'title_ru',
        'title_en',
        'anons_ru',
        'anons_en',
        'description_ru',
        'description_en',
        'publish_at',
        'status',
        'photo'
    ];

    public function scopeNodraft($query)
    {
        return $query->where('status', '!=', self::STATUS_DRAFT)
            ->whereNull('deleted_at');
    }

    public function scopePublished($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE)
            ->where('publish_at', '<', date('c'))
            ->whereNull('deleted_at');
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Admin\Blog\Categories');
    }
}
