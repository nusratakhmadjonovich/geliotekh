<?php

namespace App\Models\Admin;

use App\Models\MainModel;

class Setting extends MainModel
{
    protected $table = 'settings';

    protected $fillable = [
        'title', 'setting_key', 'setting_value', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status','=', self::STATUS_ACTIVE);
    }
}
