<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Groups;

class Permissions extends Model
{

    protected $table = 'rbac_permissions';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];

    /**
     * Get all permission grouped by module
     * return array
     */
    public static function all_permissions()
    {
        $permissions = Permissions::all();
        $perms = [];
        foreach ($permissions as $item) {
            $k['id'] = $item->id;
            $k['name'] = $item->name;
            $perms[$item->module][] = $k;
        }
        return $perms;
    }

    public static function getUserPermissions()
    {
        $groups = UsersGroups::getGroupsByUser();
        $permissions = GroupsPermissions::getPermissionsByGroups($groups);
    }


    public function groups()
    {
        return $this->belongsToMany('App\Models\Admin\Groups', 'rbac_groups_permissions_rel', 'permission_id', 'group_id');
    }
}
