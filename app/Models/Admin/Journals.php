<?php

namespace App\Models\Admin;

use App\Models\MainModel;

class Journals extends MainModel
{
    public function scopeNoDraft($query)
    {
        return $query->where('status', '!=', self::STATUS_DRAFT);
    }

    public function journalEditions()
    {
        return $this->hasMany('App\Models\Admin\JournalEditions');
    }
}
