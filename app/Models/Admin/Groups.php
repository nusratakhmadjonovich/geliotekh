<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Groups extends Model
{
    protected $table = 'rbac_groups';

    protected $softDelete = true;

    protected $fillable = ['name'];

    public function scopeNoDraft($query)
    {
        return $query->where('status', '!=', -1);
    }

    /**
     * Is Group allowed
     * Check if group is allowed to do specified action, admin always allowed
     * @param int $permission Permission id to check
     * @param int|string|bool $group_par Group id or name to check, or if FALSE checks all user groups
     * @return bool
     */
    public static function hasGroupPermission($permission)
    {

        $permission = Permissions::select('id')->where('code', $permission)->first();
        if(!$permission)
            return FALSE;

        $user_group = UsersGroups::select('group_id')->where('user_id', Auth::user()->id)->first();
        if(!$user_group)
            return FALSE;

        $group_permission_count = GroupsPermissions::where('group_id', $user_group->group_id)->where('permission_id', $permission->id)->count();

        if ($group_permission_count > 0) {
            return TRUE;
        }
        return FALSE;
    }


    public function permissions()
    {
        return $this->belongsToMany('App\Models\Admin\Permissions', 'rbac_groups_permissions_rel', 'group_id', 'permission_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'rbac_users_groups_rel', 'group_id', 'user_id');
    }
}
