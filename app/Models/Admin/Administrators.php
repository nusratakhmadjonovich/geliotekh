<?php

namespace App\Models\Admin;

use App\Models\Role;
use App\Models\User;

class Administrators extends User
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $softDelete = true;

    public function scopeNoActive($query)
    {
        return $query->where('status', '!=', -1)->where('is_deleted', '!=', 0);
    }

   /* public function articles()
    {
        return $this->hasMany('App\Models\Members\UserArticles','user_id');
    }*/

    public function scopeNoDeleted($query)
    {
        return $query->where('is_deleted', '=', 0);
    }

    public function scopeAdmins($query){
        return $query->where('role_id', '=', Role::ROLE_ADMIN);
    }

    public function user_article_meta()
    {
        return $this->belongsToMany('App\Models\User_Article_Meta','reviewer_article','user_id','article_meta_id');
    }

    public function reviewer_user_article()
    {
        return $this->hasMany('App\Models\Reviewer_Article','user_id');
    }
}
