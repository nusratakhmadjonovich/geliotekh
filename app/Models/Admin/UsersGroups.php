<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class UsersGroups extends Model
{
    protected $table = 'rbac_users_groups_rel';

    public static function getGroupsByUser()
    {
        return self::where('user_id', Auth::user()->id)->all();
    }

    public static function user_group_connection($user_id, $group_id){
        UsersGroups::where('user_id', $user_id)->delete();
        $ug = new UsersGroups();
        $ug->user_id = $user_id;
        $ug->group_id = $group_id;
        $ug->save();
    }
}