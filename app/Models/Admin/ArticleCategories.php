<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ArticleCategories extends Model
{
    protected $table = 'article_categories';

    protected $fillable = [
        'name_ru', 'name_en', 'description_ru', 'description_en', 'photo', 'status'
    ];

    public function scopeNoDraft($query)
    {
        return $query->where('status', '!=', -1);
    }
}
