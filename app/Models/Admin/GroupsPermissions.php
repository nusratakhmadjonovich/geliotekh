<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupsPermissions extends Model
{
    protected $table = 'rbac_groups_permissions_rel';

    public static function getPermissionsByGroups($groups)
    {
        foreach($groups as $group){
            GroupsPermissions::where('user_id', Auth::user()->id);
        }

    }

    public static function groups_permissions_connection($group_id, $permissions){
        GroupsPermissions::where('group_id', $group_id)->delete();
        if(isset($permissions)){
            foreach($permissions as $item){
                $gp = new GroupsPermissions();
                $gp->group_id = $group_id;
                $gp->permission_id = $item;
                $gp->save();
            }
        }
    }

    public static function selected_permissions($group_id){
        return GroupsPermissions::where('group_id', $group_id)->get();
    }

}
