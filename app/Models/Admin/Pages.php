<?php

namespace App\Models\Admin;

use App\Models\MainModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends MainModel
{
    use SoftDeletes;

    protected $table = 'pages';

    protected $fillable = [
        'title_ru',
        'title_en',
        'description_ru',
        'description_en',
        'status',
        'publish_at'
    ];

    public function scopePublished($query)
    {
        return $query->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at');
    }
}
