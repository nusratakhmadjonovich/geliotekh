<?php

namespace App\Models\Admin;

use App\Models\MainModel;

class JournalEditions extends MainModel
{
    protected $table = 'journal_editions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'journal_id', 'year', 'number', 'receiving_articles', 'date_edition', 'placement_electronic_monographs', 'distribution_print_copies_monographs', 'mailing_editions', 'description_ru', 'description_en', 'photo', 'file' ,'status'
    ];

    public function scopeNoDraft($query)
    {
        return $query->where('status', '!=', self::STATUS_DRAFT);
    }

    public function journal()
    {
        return $this->belongsTo('App\Models\Admin\Journals','journal_id','id');
    }

    public function journal_edition_article()
    {
        return $this->hasMany('App\Models\Admin\JournalEditionArticles');
    }
}