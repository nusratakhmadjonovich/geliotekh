<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainModel extends Model
{
    const STATUS_DRAFT = -1;
    const STATUS_ACTIVE = 1;

}