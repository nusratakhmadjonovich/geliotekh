<?php

namespace App\Models\Members;

use Illuminate\Database\Eloquent\Model;

class SubAuthor extends Model
{
    protected $table = 'user_article_authors';
    public function user()
    {
        return $this->belongsTo('App\Models\Http\Models\User');
    }

    public function user_articles()
    {
        return $this->belongsTo('App\Models\Members\UserArticles','article_id');
    }

}
