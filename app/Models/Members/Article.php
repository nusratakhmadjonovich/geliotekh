<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function users_articles()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function sub_authors()
    {
        return $this->hasMany('App\Models\Sub_Author');
    }

}
