<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviewer_Response extends Model
{
    protected $table = 'reviewer_response';

    public function article_meta()
    {
        return $this->belongsTo('App\Models\User_Article_Meta','id','article_meta_id');
    }

}
