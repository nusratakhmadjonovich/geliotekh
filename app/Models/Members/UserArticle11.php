<?php

namespace App\Models\Members;

use Illuminate\Database\Eloquent\Model;

class UserArticle extends Model
{
  protected $table = 'user_articles';

    public function users_articles()
    {
        return $this->belongsTo('App\Models\User','id','user_id');
    }
    public function authors()
    {   
        return $this->hasMany('App\Models\Sub_Author','article_id','id');
    }
    public function article_file()
    {
        return $this->hasMany('App\Models\User_Article_Meta','article_id');
    }

    public function scopeNoDraft($query)
    {
        return $query->where('status','!=',-1);
    }

    public function scopeSelectAcceptArticle($data)
    {
        return $data
            ->where('status','!=',-1)
            ->where('is_deleted','=',0)
            ->where('is_accept','=',1)
            ->where('is_rejected','=',0)
            ;
    }

}
