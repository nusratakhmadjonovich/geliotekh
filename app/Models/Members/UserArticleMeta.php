<?php

namespace App\Models\Members;

use Illuminate\Database\Eloquent\Model;

class UserArticleMeta extends Model
{
    protected $table = 'user_article_meta';

    public function reviewer_article()
    {
        return $this->hasOne('App\Models\Reviewer_Article','article_meta_id');
    }

    public function reviewer_user()
    {
        return $this->belongsToMany('App\Models\Admin\Administrators','reviewer_article','article_meta_id','user_id');
    }

    public function article_meta_reviewer_response()
    {
        return $this->hasMany('App\Models\Reviewer_Response','article_meta_id','id');
    }


}
