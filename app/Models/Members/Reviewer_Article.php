<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviewer_Article extends Model
{
    protected $table = 'reviewer_article';

    public static function send_each_article($email)
    {
        mail($email,'admin@gmail.com','new article for rewievers ');
        return true;
    }
        public function article_meta()
    {
        return $this->hasMany('App\Models\User_Article_Meta','id','article_meta_id');
    }
}

