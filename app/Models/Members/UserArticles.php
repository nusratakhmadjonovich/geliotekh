<?php

namespace App\Models\Members;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserArticles extends Model
{
  protected $table = 'user_articles';

  protected $fillable =[''];
    public function users_articles()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function authors()
    {   
        return $this->hasMany('App\Models\SubAuthor','article_id','id');
    }
    public function article_file()
    {
        return $this->hasMany('App\Models\User_Article_Meta','article_id');
    }

    public function scopeNoDraft($query)
    {
        return $query->where('status','!=',-1)
            ->where('status','!=',-1)
            ->where('is_deleted','=',0)
            ->where('is_rejected','=',0)
            ->where('is_accept','=',0)
            ->orderBy('id','DESC');
    }

    public function scopeSelectAcceptArticle($data)
    {
        return $data
            ->where('status','!=',-1)
            ->where('is_deleted','=',0)
            ->where('is_accept','=',1)
            ->where('is_rejected','=',0)
            ;
    }

}
