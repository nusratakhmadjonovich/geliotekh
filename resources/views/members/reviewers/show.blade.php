@extends('layouts.app')

@section('title')
    show article
@endsection

@section('content')

    <div class="col-md-offset-2 col-md-8">
        <a href="{{ route('user.article.reviewer') }}" class="btn btn-success pull-left">back</a>
        @include('includes.messages') @include('includes.error_messages')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
        <div class="col-md-offset-3 col-md-6 well">
            <a href="{{ asset($article_meta->article_meta->file) }}" target="_blank" class="pull-right btn btn-success btn-sm"><i class="fa fa"></i>upload file</a>
            <p>
                <label for="" class="bg-info">Upload File Meta Dates: </label>
                {{ $article_meta->cheking_and_date }}
            </p>
            <p>
                <label for="" class="bg-success">Upload File Comment: </label>
                {{ $article_meta->article_meta->comment}}
            </p>

                <label for="" class="bg-success">Authors</label>
                <hr>
                @foreach($find_authors as $article_authors)
                    {{ $article_authors->fio }}
                    <br>
                    {{ $article_authors->email }}
                <hr>
                @endforeach
            <br>
            <hr>
            <form action="{{ url('/admin/reviewer/user/'.$user_id.'/article/'.$article_meta->id) }} }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="" class="pull-right">Result*</label>
                        </div>
                        <div class="col-md-8">
                            <select name="result" id="result" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="" class="pull-right">Comment*</label>
                        </div>
                        <div class="col-md-8">
                            <textarea name="comment" class="form-control" id="" cols="30" placeholder="please comment..." rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <button type="submit" class="btn btn-success pull-right"> send </button>
                </div>
                <br>
            </form>
            <hr>
        </div>

@endsection