@extends('layouts.app')

@section('title')

@stop

@section('style')
    <style type="text/css">
        .list-group > .list-group-item:first-child
        {
            background-color: #0b93d5;
            color: white;
        }
    </style>
@endsection

@section('content')
    <h1 class="text-center"> Reviewer articles</h1>

    <div class="col-md-3">

        <h2>List Author of Articles </h2>
        <div class="list-group">
            @foreach($review as $item)
                    <a href="{{ route('list.group.article.meta',['id'=>$item->id]) }}" class="list-group-item ">{{$item->article_meta->comment}} </a>
            @endforeach
        </div>

    </div>

    <div class="col-md-offset-2 col-md-8">
        @include('includes.messages') @include('includes.error_messages')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@stop