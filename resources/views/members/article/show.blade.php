@extends('layouts.auth')

@section('title')
    Показать статью
@stop

@section('content')
        <h1 class="text-center">Показать статью</h1>
    <div class="row">
        <div class="col-xs-3 col-md-3"></div>
        <div class="col-xs-6 col-md-6">
            @include('includes.messages')
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('/') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-eye" aria-hidden="true"></i> назад</button></a>
                            <a href="{{ url('/member/article/create') }}" title="Back"><button class="btn btn-primary btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>новая статья</button></a>
                        </div>
                    </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <br>
                        <table class="table">
                            <tbody>

                            <tr>
                                <th> заглавие</th>
                                <td> {{ $article->title }} </td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>описание</th>
                                <td>{{ $article->description}}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th> файл </th>
                                <td>
                                    <a href="{{ App\Libraries\UploadManager::getFile('user_article', $article->id, $article->path)}}" target="_blank"> download article file </a>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <th> имя пользователя </th>
                                <td> <strong>{{ $article->users_articles->fio}}</strong> </td>
                                <td></td>
                            </tr>
                            @foreach( $find_authors as $author)
                                @if($author->is_main_author == 1)
                            <tr>
                                <th>главный автор</th>
                                    <td> {{ $author->fio  }} </td>
                                    <td>{{ $author->email }}</td>
                            </tr>
                                    @else
                                    <tr>
                                        <th> суб-автор </th>
                                        <td> {{ $author->fio  }} </td>
                                        <td>{{ $author->email }}</td>
                                    </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
