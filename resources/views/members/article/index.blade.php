@extends('layouts.auth')

@section('title')

@stop

@section('content')

    @include('sweet::alert')

    <div class="content-wrapper" style="height: 840px;">
        @if ($errors->any())
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
        @endif
        <br>
        <div class="clearfix">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="box box-primary" style="color: black;">
                        <div class="box-header with-border">
                            <a href="{{ url('/') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-eye" aria-hidden="true"></i> назад</button></a>
                            <h3 class="box-title text-center">Представление статьи</h3>
                            @include('includes.messages')
                        </div><!-- /.box-header -->
                        <form action="{{ url('/member/article/'.$find_article->id) }}" id="my_opinion" method="POST" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('title') ? 'has-error':'' }}">
                                    <label for="exampleInputPassword1"><strong>заглавие</strong></label>
                                    <input type="text" name="title" class="form-control" id="exampleInputPassword1" placeholder="заглавие..." value="{{ Request::old('title') }}">
                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('category_id') ? 'has-error':'' }}">
                                    <label for="exampleInputPassword1"><strong>категория</strong></label>
                                    <select name="category_id" id="category" class="form-control">
                                        @foreach($select_categories as $select_category)
                                            <option value="1">{{ $select_category->name_ru }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('category_id') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('description') ? 'has-error':'' }}">
                                    <label for="exampleInputPassword2"><strong>описание</strong></label>
                                    <textarea name="description" class="form-control" id="exampleInputPassword2" cols="30" rows="10" placeholder="описание...">{{ old('description') }}</textarea>
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>

                                <div class="form-group {{ $errors->has('file') ? 'has-error' :'' }}">
                                    <label for="exampleInputFile"><strong>Вход в файл</strong></label>
                                    <input type="file" name="file" id="exampleInputFile" accept=".docx, .rtf, .pdf" value="{{ Request::old('file') }}">
                                    <p class="help-block"><strong>где файл загружается</strong></p>
                                    <span class="text-danger">{{ $errors->first('file') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('author_name') ? 'has-error' :'' }}">
                                    <label for="author_name"><strong>автор имя</strong></label>
                                    <input type="text" name="author_name" class="form-control" placeholder="автор..." value="{{ Request::old('author_name') }}">
                                    <span class="text-danger">{{ $errors->first('author_name') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('author_email')? 'has-error':'' }}">
                                    <label for="author_email"><strong>электронная почта автора</strong> </label>
                                    <input type="email" name="author_email" class="form-control" placeholder="author..." value="{{ Request::old('author_email') }}">
                                    <span class="text-danger">{{ $errors->first('author_email') }}</span>
                                </div>

                                    <div class="form-group">
                                        <p id="create_item">
                                        </p>
                                        <label> <strong>создать новый подавтор</strong></label>
                                        <button type="button" onclick="create_sub_author()" id="create_sub_user" class="btn btn-info">создать новый подавтор</button>
                                    </div>
                                <div class="row">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="checked"> <strong>Проверить меня</strong>
                                        </label>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="row">
                                <div class="box-footer" style="margin-bottom: 50px;">
                                    <button type="submit" class="btn btn-primary">Отправить</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div><!-- /.box -->
                    <br>
                </div>
            </div >
        </div>
    </div>

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#create_sub_user').click(function(){
                var s = Math.floor(Math.random() * 1999);
                $("#create_item").append(
                    '<div class="well" id="salom"><b id="exit_sub_user" onclick="exit_sub_user()" style="cursor: pointer;" class="pull-right">X</b><br><input type="text" class="form-control" name="authors['+ s +'][name]" id="sub_author_name" placeholder="sub user name ..."><br><input type="email" name="authors['+ s +'][email]" class="form-control" id="sub_author_email" placeholder="sub user email..."><p></p></div>'
                );
            });
        });

        function salom()
        {
            alert(document.getElementById('sub_author_name').name.length);
        }

        function exit_sub_user()
        {
            $('#salom').remove();
        }
    </script>
@endsection

@stop