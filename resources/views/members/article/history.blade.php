@extends('layouts.auth')

@section('title')
    articles
@stop

@section('content')
    <h1 class="text-center"> Articles History </h1>
    <br>
    <div class="row">
        @include('includes.messages')
        @foreach($articles as $article)
         @if($article->is_rejected == 0)
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <p class="caption">
                    <h3 class="text-center">{{ $article->title }} <h1>{{ $article->is_rejected }}</h1> </h3>
                </p>
                <hr>
                <p>
                    {{ $article->description }}
                </p>
                <hr>
                <strong>created_at : {{ $article->created_at }}</strong>
                    <p>
                        <div class="col-md-6">
                        <a href="{{ App\Libraries\UploadManager::getFile('user_article', $article->id, $article->path)}}" target="_blank"> download article </a>
                </div>
                    <div class="col-md-3"><a href="{{ url('/user/article/read/'.$article->id) }}" class="btn btn-default" role="button">read</a></div>
                        <div class="col-md-3"><a href="#" class="btn btn-default" role="button">delete</a></div>
                </p>
                <br>
                <br>
                </p>
            </div>
        </div>
             @endif
        @endforeach
    </div>
@stop