@extends('layouts.app')

@section('title')
    articles
@stop

@section('content')

    @include('sweet::alert')
    <div class="col-md-offset-3 col-md-6">
        @include('includes.messages') @include('includes.error_messages')
        @if ($errors->any())
            <div class="col-md-offset-3 col-md-6">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>

            </div>
        @endif
        <br>
        <form action="{{ url('/user/article/send/'.$article->id.'/file') }}" method="POST" enctype="multipart/form-data">
        <div class="panel panel-primary">
            <div class="panel-heading">  <h1> {{ $article->title }}</h1> </div>
            <div class="panel-body">
                <p>
                    <label for="">Description:</label>
                    {{ $article->description }}
                </p>
                <hr>
                <p>
                    <label for="">article Users:</label>
                <hr>
                    @foreach($article->authors as $authors)
                        <strong> users : {{ $authors->fio }}</strong>
                <br>
                    @endforeach
                </p>
                    <hr>
                        <p>
                            <label for="">created date:</label>
                            {{ $article->created_at }}
                        </p>
                    <hr>
                        <p class="alert alert-danger">
                            <label for="">  upload latest version file:</label>
                            <input type="file" name="file" accept=".docx, .rtf, .pdf" value="{{ Request::old('file') }}">
                    <p class="help-block">fayl yuklanadigan joy</p>
                        </p>
            </div>
            <div class="panel-footer">
                <input type="submit" class="btn btn-success" value="send article file">
            </div>
        </div>
            {{ csrf_field() }}
        </form>
    </div>
@stop