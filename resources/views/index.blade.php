@extends('layouts.app')

@section('title')

@stop

@section('content')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('app/dist/img/slider-1.jpg') }}">
                <div class="container">
                    <div class="carousel-caption text-left">
                        {!!  \App\Libraries\BlockManager::get('slider_1') !!}
                        <a class="" href="{{lang_url('archive')}}" role="button">Подписка</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="article_titles">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 article_title_items">
                    <div class="article_title_items_img float-left">
                        <img src="app/dist/icons/ic_search_article.png" alt="" title="">
                    </div>
                    <div class="article_title_items_text float-left">
                        <a href="{{lang_url('articles')}}"> {{__('app.search_articles')}} </a>
                        <div class="border_top"> </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 article_title_items">
                    <div class="article_title_items_img float-left">
                        <img src="app/dist/icons/ic_index.png" alt="" title="">
                    </div>
                    <div class="article_title_items_text float-left">
                        <a href="{{lang_url('page/6')}}"> {{__('app.index_citation')}} </a>
                        <div class="border_top"> </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 article_title_items">
                    <div class="article_title_items_img float-left">
                        <img src="app/dist/icons/ic_post_article.png" alt="" title="">
                    </div>
                    <div class="article_title_items_text float-left">
                        <a href="{{lang_url('publish')}}"> {{__('app.publish_article')}} </a>
                        <div class="border_top"> </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- article titles end -->
        <div class="other_articles">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 other_articles_all">
                    <h4 class="all_titles">{{__('app.most_citated_articles')}}</h4>
                    <div class="border_top">&nbsp;</div>
                    <div class="other_articles_all_items">
                        <div class="row">
                            @foreach($most_cited_articles as $article)
                                <div class="col-md-6 post-item">
                                    <a href="{{lang_url('articles/'.$article->id)}}">
                                        <img src="{{ App\Libraries\UploadManager::getPhoto('journal_edition_article', $article->id, 'original', $article->photo)}}" class="article-thumb"/>
                                    </a>
                                    <div class="article_title_items_all_text">
                                        <p><i class="fas fa-calendar-alt"></i> {!! $article->edition->published_at !!}</p>
                                        <a class="" href="{{'news/'.$article->id}}">{!! $article->title !!} </a>
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-xl-6 col-lg-6 col-md-6 other_articles_items ">
                                <div class="col-xl-5 col-lg-7 col-md-12 other_articles_items_img float-left">
                                    <img src="app/dist/img/article-2.jpg" alt="" title="">
                                </div>
                                <div class="col-xl-7 col-lg-5 col-md-12 article_title_items_all_text float-left">
                                    <p class="article_title_items-date"><span>Дата издания:</span> 11.11.2017</p>
                                    <a href="#" class="">Гелиоустановки и их применение</a>
                                    <p class="article_title_items_text">Реализованные в натуре гелиотехнические
                                        установки
                                        различного
                                        назначения, как экспериментального, так и практического
                                        характера, результаты измерений их параметров или расчетов.
                                        Характеристики выпускаемого гелиотехнического оборудования,
                                        рекомендации по его применению. Установки и системы,
                                        имеющие реальное практическое применение, их характеристики.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 other_articles_items">
                                <div class="col-xl-5 col-lg-7 col-md-12 other_articles_items_img float-left">
                                    <img src="app/dist/img/article-1.jpg" alt="" title="">
                                </div>
                                <div class="col-xl-7 col-lg-5 col-md-12 article_title_items_all_text float-left">
                                    <p class="article_title_items-date"><span>Дата издания:</span> 11.11.2017</p>
                                    <a href="#" class="">Гелиоустановки и их применение</a>
                                    <p class="article_title_items_text">Реализованные в натуре гелиотехнические
                                        установки
                                        различного
                                        назначения, как экспериментального, так и практического
                                        характера, результаты измерений их параметров или расчетов.
                                        Характеристики выпускаемого гелиотехнического оборудования,
                                        рекомендации по его применению. Установки и системы,
                                        имеющие реальное практическое применение, их характеристики.
                                    </p>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 other_articles_items">
                                <div class="col-xl-5 col-lg-7 col-md-12 other_articles_items_img float-left">
                                    <img src="app/dist/img/article-2.jpg" alt="" title="">
                                </div>
                                <div class="col-xl-7 col-lg-5 col-md-12 article_title_items_all_text float-left">
                                    <p class="article_title_items-date"><span>Дата издания:</span> 11.11.2017</p>
                                    <a href="#" class="">Гелиоустановки и их применение</a>
                                    <p class="article_title_items_text">Реализованные в натуре гелиотехнические
                                        установки
                                        различного
                                        назначения, как экспериментального, так и практического
                                        характера, результаты измерений их параметров или расчетов.
                                        Характеристики выпускаемого гелиотехнического оборудования,
                                        рекомендации по его применению. Установки и системы,
                                        имеющие реальное практическое применение, их характеристики.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12  all_article_title">
                                <a href="{{lang_url('articles')}}"> {{__('app.All_articles')}} <i class="fas fa-angle-double-right"></i></a>
                            </div>
                            <div class="col-xl-11 col-lg-11 col-md-11 bottom_line"></div>
                        </div>
                    </div>

                    <h4 class="all_titles">{{__('app.News')}}</h4>
                    <div class="border_top">&nbsp;</div>
                    @foreach($news as $item)
                        <div class="news">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-7">
                                    <div class="news_item_img">
                                        <img src="{{ App\Libraries\UploadManager::getPhoto('blog_post', $item->id, 'original', $item->photo)}}" title="{!! $item->title_ru !!}"/>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-5 col-md-5">
                                    <div class="news_item_text">
                                        <a href="{{lang_url('news/'.$item->id)}}">{!! $item->title !!}</a>
                                        <p>{!! $item->anons !!}</p>
                                    </div>
                                    <div class="news_item_date">
                                        <i class="fas fa-calendar-alt"></i> <span> {{ date('H:i / Y-m-d', strtotime($item->publish_at)) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="all_article_title">
                        <a href="{{lang_url('news')}}"> {{__('app.All_news')}} <i class="fas fa-angle-double-right"></i></a>
                    </div>

                    <div class="col-xl-11 col-lg-11 col-md-11 bottom_line"></div>
                    <div class="rewards">
                        <h4 class="all_titles">{{__('app.Awards')}}</h4>
                        <div class="border_top">&nbsp;</div>
                        <div class="rewards_items">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-7">
                                    <div class="rewards_item_img">
                                        <img src="app/dist/img/certificate-1.jpg" alt="" title="">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-5 col-md-5">
                                    <div class="rewards_item_text">
                                        <a href="#">Лучший автор по рейтинге Springer </a>
                                        <p>В 2017 году компанией Clarivate Analytics, журнал
                                            «Applied Solar Energy» был признан лучшим научным журналом Средней Азии и
                                            получил награду Web of
                                            Science Awards 2017.</p>
                                    </div>
                                    <div class="rewards_item_date">
                                        <i class="fas fa-calendar-alt"></i> &nbsp; <span>11.10.2017</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rewards_items">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-7">
                                    <div class="rewards_item_img">
                                        <img src="app/dist/img/certificate-2.jpg" alt="" title="">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-5 col-md-5">
                                    <div class="rewards_item_text">
                                        <a href="#">Лучший научный журналом Средней Азии </a>
                                        <p>В 2017 году компанией Clarivate Analytics, журнал
                                            «Applied Solar Energy» был признан лучшим научным журналом Средней Азии и
                                            получил награду Web of
                                            Science Awards 2017.</p>
                                    </div>
                                    <div class="rewards_item_date">
                                        <i class="fas fa-calendar-alt"></i> &nbsp; <span>11.10.2017</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tematika col-xl-4 col-lg-4 col-md-12">
                    <h4 class="all_titles">@lang('app.Thematic_coverage')</h4>
                    <div class="border_top">&nbsp;</div>
                    @foreach($categories as $category)
                        <div class="tematika_item">
                            <div class="col-xl-5 col-lg-5 col-md-12 tematika_img float-left">
                                <a href="{{route('articles', ['category'=>$category->id])}}"><img width="145" height="200" src="{{ App\Libraries\UploadManager::getPhoto('categories', $category->id, 'original', $category->photo)}}"/></a>
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-12 tematika_text float-left">
                                <a class="title" href="{{route('articles', ['category'=>$category->id])}}">{{ $category->name }}</a>
                            </div>
                        </div>

                    @endforeach
                    <div class="all_article_title">
                        <a href="{{lang_url('categories')}}"> @lang('app.View_more') <i class="fas fa-angle-double-right"></i></a>
                    </div>
                </div>

            </div>
            <div class="col-xl-12 bottom_line"></div>
            <div class="events">
                <h4 class="all_titles">{{__('app.Events')}}</h4>
                <div class="border_top">&nbsp;</div>
                <div class="row">
                    @foreach($events as $item)
                        <div class="col-xl-4 col-lg-4 col-md-4">
                            <div class="card">
                                <div class="card-block">
                                <span class="card-block-date">
                                    <h4 class="card-block-date-num"> {{ date('d', strtotime($item->publish_at)) }} </h4>
                                    <month> {{ date('m', strtotime($item->publish_at)) }} </month>
                                    <year> {{ date('Y', strtotime($item->publish_at)) }} </year>
                                </span>
                                    <h6 class="card-title">
                                        <p class="events_location"><i class="fas fa-map-marker-alt"></i> {!! $item->anons !!}</p>
                                        <a href="{{lang_url('news/'.$item->id)}}">{!! $item->title !!}</a>
                                    </h6>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="all_events">
                    <a href="{{lang_url('events')}}"> {{__('app.All_events')}} <i class="fas fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="col-xl-12 bottom_line"></div>
            <div class="partners">
                <h4 class="all_titles">{{__('app.Partners')}}</h4>
                <div class="border_top">&nbsp;</div>
                <div class="row">
                    <div class="col-xl-1 col-lg-1 col-md-1  partners-item">&nbsp;</div>
                    <div class="col-xl-10 col-lg-10 col-md-10 partners-item">
                        <div class="partners-item-img float-left">
                            <a href="#">
                                <img src="app/dist/img/p_e_line.png" alt="" title="">
                            </a>
                        </div>
                        <div class="partners-item-img float-left">
                            <a href="#">
                                <img src="app/dist/img/p_springert.png" alt="" title="">
                            </a>
                        </div>
                        <div class="partners-item-img float-left">
                            <a href="#">
                                <img src="app/dist/img/p_e_library.png" alt="" title="">
                            </a>
                        </div>
                        <div class="partners-item-img float-left">
                            <a href="#">
                                <img src="app/dist/img/p_scopus.png" alt="" title="">
                            </a>
                        </div>
                    </div>

                    <div class="col-xl-1 col-lg-1 col-md-1 partners-item">&nbsp;</div>
                </div>
            </div>
            <div class="col-xl-12 bottom_line"></div>
            <div class="region">
                <h4 class="all_titles">Регионы распространения нашего журнала</h4>
                <div class="border_top">&nbsp;</div>
                <div class="region_in">
                    <p class="map_in_text">В Республике Узбекистан, в РФ, в США издательством Allerton Press Inс.
                        переиздается на английском языке
                        и распространяется в более 30-х странах мира.</p>
                    <p class="region_in_map">
                        <img src="app/dist/img/map.png" alt="" title="">
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop