<div style="margin-right:10px;margin-left:10px;">

    @if(session()->has('success-message'))

        <p class="alert alert-success">{{session('success-message')}}</p>

    @endif


    @if(session()->has('danger-message'))

        <p class="alert alert-danger">{{session('danger-message')}}</p>

    @endif


    @if(session()->has('warning-message'))

        <p class="alert alert-warning">{{session('warning-message')}}</p>

    @endif


    @if(session()->has('info-message'))

        <p class="alert alert-info">{{session('info-message')}}</p>

    @endif
</div>

    @section('script')
        $(".alert").fadeTo(5000, 2000).slideUp(500, function(){
            $(".alert").slideUp(2000);
        });
    @stop