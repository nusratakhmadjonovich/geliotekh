@extends('layouts.admin')

@section('title')
   User
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/journals') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/journal/edition/show/'.$journal->id) }}" title="Edit Models\Admin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                    <form method="POST" action="{{ url('admin/journals' . '/' . $journal->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Admin" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                    </form>
                </div>

                <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td class="text-center">{{ $journal->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td class="text-center"> {{ $journal->name }} </td></tr>
                                    @if($journal->status == "0")
                                    <tr><th> Status </th><td class="bg bg-danger text-center"> passive </td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                    <div class="box-header">
                        <a href="{{ url('/admin/journals') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/journal/edition/'.$journal->id) }}" class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> New Create </a>
                    </div>

                    <br>

                </div>

                    <div class="box-body">

                        <div class="table-responsive">
                            <table class="table table-bordered" id="users-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Year</th>
                                    <th>Receiving Journals</th>
                                    <th>Edtion Journals</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($journal->journalEditions as $journal_edition)
                                    @if($journal_edition->is_deleted == 0)
                                        <tr>
                                            <td>{{$journal_edition->id}}</td>
                                            <td>{{ $journal_edition->year }}</td>
                                            <td>{{ $journal_edition->receiving_articles }}</td>
                                            <td>{{ $journal_edition->editing_journal}}</td>
                                            <td>

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>

    </div>

@endsection
