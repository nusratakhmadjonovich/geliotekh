@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Добавить журнал @else Редактировать журнал @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')

                    <form method="POST" action="{{ url('/admin/journals/'.$journal->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12 {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class="control-label">{{ 'Название' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="name" type="text" id="name" value="{{ $journal->name or ''}}">
                            </div>
                            {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/journals') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
