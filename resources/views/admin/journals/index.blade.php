@extends('layouts.admin')

@section('title')
    Журнал
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/journals/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Создать
                    </a>
                </div>

                @include('includes.messages') @include('includes.error_messages')
                <div class="box-body">
                    <div class="search-box">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Название">
                            </div>
                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </form>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th width="20">#</th>
                                <th>Название</th>
                                <th width="20">Действие</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.journals.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection
