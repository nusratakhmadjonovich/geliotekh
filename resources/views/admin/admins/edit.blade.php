@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Добавить @else Редактировать @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">
                    @include('includes.messages')

                    <form method="POST" action="{{ url('/admin/admins/'.$admin->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : ''}}">
                                    <label for="email" class="control-label">{{ 'Email' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                        <input class="form-control" name="email" type="text" id="email" value="{{ old('email', $admin->email) }}">
                                    </div>
                                    {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('password') ? 'has-error' : ''}}">
                                    <label for="password" class="control-label">{{ 'Пароль' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input class="form-control" name="password" type="password" id="password">
                                    </div>
                                    {!! $errors->first('password', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                    <label for="password_confirmation" class="control-label">{{ 'Пов. пароль' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input class="form-control" name="password_confirmation" type="password" id="password_confirmation" >
                                    </div>
                                    {!! $errors->first('password_confirmation', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12">

                                    <label for="status" class="control-label">{{ 'Статус' }}</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-success btn-rounded {{ (old('status', $admin->status) == 1 || $isNewRecord) ? 'active' : '' }} btn-md form-check-label">
                                            <input class="form-check-input" name="status"  value="1" type="radio" {{ (old('status', $admin->status) == 1 || $isNewRecord) ? 'checked' : '' }}> Актив
                                        </label>

                                        <label class="btn btn-danger btn-rounded {{ old('status', $admin->status) == 0 ? 'active' : '' }} btn-md form-check-label">
                                            <input class="form-check-input" name="status" value="0" type="radio" {{ old('status', $admin->status) == 0 ? 'checked' : '' }}> Неактив
                                        </label>
                                    </div>
                                    {!! $errors->first('status', '<p class="error-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12 {{ $errors->has('fio') ? 'has-error' : ''}}">
                                    <label for="fio" class="control-label">{{ 'Ф.И.О' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                        <input class="form-control" name="fio" type="text" id="fio" value="{{ $admin->fio or ''}}">
                                    </div>
                                    {!! $errors->first('fio', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('group_id') ? 'has-error' : ''}}">
                                    <label for="group_id" class="control-label">{{ 'Группа' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                        <select  class="form-control select2"  name="group_id" id="group_id">
                                            @foreach($groups as $group)
                                                <option value="{{$group->id}}" >{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {!! $errors->first('group_id', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('photo') ? 'has-error' : ''}}">
                                    <label for="photo" class="control-label">Фото</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-file glyphicon glyphicon-file"></i></div>
                                        <input class="form-control" name="photo" type="file" id="photo">
                                    </div>
                                    {!! $errors->first('photo', '<p class="error-block">:message</p>') !!}
                                    <div class="img-input-show">
                                        @if($admin->photo)
                                            <img src="{{ App\Libraries\UploadManager::getPhoto('profile', $admin->id, 'original', $admin->photo)}}" class="img-thumbnail" width="150"/>
                                            <a href="{{ url('admin/admins/removePhoto/'.$admin->id) }}" >удалить фото</a>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/admins') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
