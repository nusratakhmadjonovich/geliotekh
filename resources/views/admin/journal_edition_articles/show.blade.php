@extends('layouts.admin')

@section('title')
    Show Article
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/journal/edition/articles') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/journal/edition/articles/'.$find_journal_edition_articles->id.'/edit') }}" title="Edit Models\Admin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                    <form method="POST" action="{{ url('/admin/journal/edition/articles/'.$find_journal_edition_articles->id) }}" accept-charset="UTF-8" style="display:inline">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="0dhbvqU9g44vKlrS0p6cX8M5xShELeDMpvmzdvZK">
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Admin" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                    </form>
                </div>
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="row">
                        <p>
                        <div class="col-md-3">
                            <label for="">Загаловок:</label>
                        </div>
                        <div class="col-md-6">
                            {{ $find_journal_edition_articles->title_ru}}
                        </div>
                        </p>
                    </div>
                    <div class="row">
                        <p>
                            <div class="col-md-3">
                                    <label for="">Описание:</label>
                            </div>
                            <div class="col-md-6">
                                {{ $find_journal_edition_articles->description_ru }}
                            </div>
                        </p>
                    </div>
                    <div class="row">
                        <p>
                            <div class="col-md-3">
                            <label for="">Файл:</label>
                        </div>
                            <div class="col-md-6">
                                <a href="{{ App\Libraries\UploadManager::getFile('journal_edition_article', $find_journal_edition_articles->id, $find_journal_edition_articles->file)}}" target="_blank"> download article file </a>
                            </div>
                        </p>
                    </div>
                    <div class="row">
                        <p>
                            <div class="col-md-3">
                            <label for="">фото:</label>
                        </div>
                            <div class="col-md-6">
                                <img src="{{ App\Libraries\UploadManager::getPhoto('journal_edition_article', $find_journal_edition_articles->id, 'original', $find_journal_edition_articles->photo)}}" class="img-thumbnail" width="100"/>
                            </div>
                        </p>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div><!-- /.box-footer-->
            </div><!-- /.box -->

        </div>

        <section class="content">
        </section><!-- /.content -->

    </div>
@endsection
