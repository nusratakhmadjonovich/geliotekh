@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Добавить статьи @else Редактировать статьи @endif
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ url('/admin/journal/edition/articles/'.$article->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-body">
                        @include('includes.messages') @include('includes.error_messages')
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12">
                            <label for="journal_edition_id" class="control-label">{{ 'Выпуск' }}</label>
                            <select id="journal_edition_id" class="form-control"  name="journal_edition_id">
                                @foreach($editions as $edition)
                                    <option value="{{$edition->id}}">{{$edition->year .' - '.$edition->number}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="article_category_id" class="control-label">{{ 'Рубрика' }}</label>
                            <select id="article_category_id" class="form-control select2"  name="article_category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name_ru}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : ''}}">
                                        <label for="title_ru" class="control-label">{{ 'Загаловок' }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                            <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ old('title_ru', $article->title_ru)}}">
                                        </div>
                                        {!! $errors->first('title_ru', '<p class="error-block">:message</p>') !!}
                                    </div>

                                    <div class="{{ $errors->has('description_ru') ? 'has-error' : ''}}">
                                        <label for="description_ru" class="control-label">{{ 'Описание' }}</label>
                                        <textarea name="description_ru" class="form-control">{{ old('description_ru', $article->description_ru) }}</textarea>
                                        {!! $errors->first('description_ru', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group {{ $errors->has('key_words') ? 'has-error' : ''}}">
                                        <label for="keywords_ru" class="control-label">{{ 'ключевые слова' }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                            <input class="form-control" name="keywords_ru" type="text" id="keywords_ru" value="{{ old('keywords_ru', $article->keywords_ru)}}">
                                        </div>
                                        {!! $errors->first('keywords_ru', '<p class="error-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="form-group {{ $errors->has('title_en') ? 'has-error' : ''}}">
                                        <label for="title_en" class="control-label">{{ 'Загаловок' }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                            <input class="form-control" name="title_en" type="text" id="title_en" value="{{ old('title_en', $article->title_en) }}">
                                        </div>
                                        {!! $errors->first('title_en', '<p class="error-block">:message</p>') !!}
                                    </div>

                                    <div class="{{ $errors->has('description_en') ? 'has-error' : ''}}">
                                        <label for="description_en" class="control-label">{{ 'Description' }}</label>
                                        <textarea name="description_en" class="form-control">{{ old('description_en', $article->description_en) }}</textarea>
                                        {!! $errors->first('description_en', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group {{ $errors->has('keywords_en') ? 'has-error' : ''}}">
                                        <label for="keywords_en" class="control-label">{{ 'key words' }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                            <input class="form-control" name="keywords_en" type="text" id="keywords_en" value="{{ old('keywords_en', $article->keywords_en)}}">
                                        </div>
                                        {!! $errors->first('keywords_en', '<p class="error-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('authors') ? 'has-error' : ''}}">
                            <label for="authors" class="control-label">{{ 'Автори' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="authors" type="text" id="authors" value="{{ old('authors', $article->authors)}}">
                            </div>
                            {!! $errors->first('authors', '<p class="error-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/journal/edition/articles') }}" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="status" class="control-label">{{ 'Опубликовать' }}</label>
                            <input type="checkbox" name="status" class="js-switch" value="1" {{ $article->status === 1 ? 'checked' : '' }} />
                            {!! $errors->first('status', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12">
                            <label for="is_chargerable" class="control-label">{{ 'Платный' }}</label>
                            <input type="checkbox" name="is_chargerable" class="js-switch" value="1" {{ $article->is_chargerable === 1 ? 'checked' : '' }} />
                            {!! $errors->first('is_chargerable', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12">
                            <label for="most_cited" class="control-label">{{ 'Наиболее цитируемые статьи' }}</label>
                            <input type="checkbox" name="most_cited" class="js-switch" value="1" {{ $article->most_cited === 1 ? 'checked' : '' }} />
                            {!! $errors->first('most_cited', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12 {{ $errors->has('file') ? 'has-error' : ''}}">
                            <label for="file" class="control-label">{{ 'Файл' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="file" type="file" id="name" value="{{ $article->file or ''}}">
                            </div>
                            {!! $errors->first('file', '<p class="error-block">:message</p>') !!}
                            @if($article->file)
                                <a href="{{ App\Libraries\UploadManager::getFile('journal_edition_article', $article->id, $article->file)}}" target="_blank"> {{$article->file}} </a>
                                <a href="{{ url('admin/journal/edition/articles/removeFile/'.$article->id) }}" target="_blank" >удалить файл</a>
                            @endif
                        </div>
                        <div class="form-group col-md-12 {{ $errors->has('photo') ? 'has-error' : ''}}">
                            <label for="photo" class="control-label">{{ 'фото' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="photo" type="file" id="photo" value="{{ $article->photo or ''}}">
                            </div>
                            {!! $errors->first('photo', '<p class="error-block">:message</p>') !!}
                            <div class="img-input-show">
                                @if($article->photo)
                                    <img src="{{ App\Libraries\UploadManager::getPhoto('journal_edition_article', $article->id, 'original', $article->photo)}}" class="img-thumbnail" width="150"/>
                                    <a href="{{ url('admin/journal/edition/articles/removePhoto/'.$article->id) }}"  >удалить фото</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace('description_ru', options);
        CKEDITOR.replace('description_en', options);
    </script>
@stop