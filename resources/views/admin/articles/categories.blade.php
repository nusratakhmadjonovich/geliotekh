@extends('layouts.admin')

@section('title')
    create categories
@stop

@section('content')
    @include('sweet::alert')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="#" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
                <div class="box-body">
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ route('add.new.category') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="language" class="col-md-4 control-label">{{ 'Language' }}</label>
                            <div class="col-md-6">
                                <select  class="form-control"  name="lang" id="language">
                                    <option value="RU">RU</option>
                                    <option value="EN">EN</option>
                                    <option value="UZ">UZ</option>
                                </select>
                                {!! $errors->first('Language', '<p class="error-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class="col-md-4 control-label">{{ 'Category Name' }}</label>
                            <div class="col-md-6">
                                <input class="form-control" name="name" type="text" id="name" value="" required>
                                {!! $errors->first('Category Name', '<p class="error-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <input class="btn btn-primary" type="submit" value="Create Category">
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
