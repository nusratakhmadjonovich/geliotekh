@extends('layouts.admin')

@section('title')
    Показать статью
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/articles') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> назад</button></a>
                </div>

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <div class="table-responsive">
                        <h1 class="pull-right">.....</h1>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>ID</th><td>{{ $articles->id }}</td>
                            </tr>
                            <tr>
                                <th> Ф.И.О </th>
                                <td> {{ $articles->title }} </td>
                            </tr>
                            <tr>
                                <th>описание</th>
                                <td>{{ $articles->description}}</td>
                            </tr>
                            <tr>
                                <th> файл </th>
                                <td>
                                    <a href="{{ App\Libraries\UploadManager::getFile('user_article', $articles->id, $articles->path)}}" target="_blank"> download article file </a>
                                </td>
                            </tr>
                            <tr>
                                <th> имя пользователя </th>
                                <td> <strong>{{ $articles->users_articles->fio}}</strong> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
