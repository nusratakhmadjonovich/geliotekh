
<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 control-label">{{ 'Title' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="title" type="text" id="title" disabled value="{{ $edit_reviewer->title or ''}}" required>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="article_meta_id" class="col-md-4 control-label">{{ 'last File' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="article_meta_id" type="text" id="article_meta_id" value="{{ $latest_meta_file->comment }}" required>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="article_meta_id" class="col-md-4 control-label">{{ 'last Uploaded File' }}</label>
    <div class="col-md-6">
        <a href="{{ asset($latest_meta_file->file) }}" target="_blank"> {{$latest_meta_file->comment}} </a>

    </div>
</div>



<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="cheking_date" class="col-md-4 control-label">{{ 'Checking and Date' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="cheking_date" type="text" id="datepicker" value="@foreach($edit_reviewer->article_file as $title){{ $title->cheking_and_date or ''}}@endforeach" required>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 control-label">{{ 'Send Reviewer' }}</label>
    <div class="col-md-3">
        @foreach($find_users as $users)
                            <p>
                                <label for="">Username:</label> {{ $users->fio }}
                                <br>
                                <label for="">Email:</label>     <input type="checkbox" @foreach($reviewer as $reviewerlar) @if($users->id == $reviewerlar->user_id) checked @endif @endforeach id="check{{$users->id}}" name="users[]" value="{{$users->id}}" > <label for="check{{$users->id}}">{{ $users->id }}.{{ $users->email }}</label>
            <hr>
                            </p>
        @endforeach
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="update">
    </div>
</div>
