@extends('layouts.admin')

@section('title')
    Show Article
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('admin/reviewers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>ID</th><td>{{ $find_reviewer->id }}</td>
                            </tr>
                            <tr>
                                <th> Title </th>
                                <td> {{ $find_reviewer->title  }} </td>
                            </tr>
                            <tr>
                                <th> Checking and Date </th>
                                <td> {{ $find_reviewer->cheking_and_date  }} </td>
                            </tr>
                            <tr>
                                <th> article meta id </th>
                                <td> {{ $find_reviewer->article_meta_id  }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
