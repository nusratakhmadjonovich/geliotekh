@extends('layouts.admin')

@section('title')
    User Articles
@stop

@section('content')
    @include('sweet::alert')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <form method="POST" id="search-form" class="form-inline" role="form"  style="background-color:#EAEAEA;padding: 17px;">

                        <div class="form-group">
                            <input type="text" class="form-control" name="id" id="id" placeholder="id">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" id="title" placeholder="title">
                        </div>
                        {{--<div class="form-group">
                            <input type="text" class="form-control" name="description" id="description" placeholder="description">
                        </div>--}}
                        <div class="form-group">
                            <input type="text" class="form-control" name="location" id="location" placeholder="location">
                        </div>

                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>
                    <br>
                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                {{--<th>Description</th>--}}
                                <th>Username</th>
                                <th>Location</th>
                                {{--<th>File</th>--}}
                                <th width="240px">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.articles.get.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },

            columns: [
                {data: 'id', name: 'id'},
                {data:'title',name:'title'},
                /*{data:'description',name:'description'},*/
                {data: 'users_articles.fio',name:'username'},
                {data:'created_at',name:'create_article'},
                /*{data:'path',name:'myPath',
                    "render":function (myPath) {
                    return '<a class="btn btn-link btn-sm" style="font-size:15px;" target="_blank" href='+myPath+'>'+"download articles"+'</a>';
                }},*/
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop


