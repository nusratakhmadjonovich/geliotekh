@extends('layouts.admin')

@section('title')
    Show Article
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/articles/rejected') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <div class="table-responsive">
                        <h1 class="pull-right">.....</h1>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>ID</th><td>{{ $articles->id }}</td>
                            </tr>
                            <tr>
                                <th> Fio </th>
                                <td> {{ $articles->title }} </td>
                            </tr>
                            <tr>
                                <th>description</th>
                                <td>{{ $articles->description}}</td>
                            </tr>
                            <tr>
                                <th> username </th>
                                <td> {{ $articles->user_id}} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
