@extends('layouts.admin')

@section('title')
    User Articles
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <form method="POST" id="search-form" class="form-inline" role="form"  style="background-color:#EAEAEA;padding: 17px;">

                        <div class="form-group">
                            <input type="text" class="form-control" name="id" id="id" placeholder="id">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" id="title" placeholder="title">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="location" id="location" placeholder="location">
                        </div>

                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>
                    <br>
                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>username</th>
                                <th>location</th>
                                <th>created_at</th>
                                <th>rejected</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@stop
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.articles.rejected.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data:'title',name:'title'},
                {data: 'user_id',name:'user_id'},
                {data:'created_at',name:'create_article'},
                {data:'path',name:'location'},
                {data:'is_rejected',name:'rejected'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop


