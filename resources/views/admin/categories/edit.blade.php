@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Создать рубкика @else Редактировать рубрика @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    @include('includes.messages') @include('includes.error_messages')
                    <form method="POST" action="{{ url('/admin/categories/'.$category->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12">
                            <label for=""></label>
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="{{ $errors->has('name_ru') ? 'has-error' : ''}}">
                                            <label for="name_ru" class="control-label required">{{ 'Загаловок' }}</label>
                                            <input class="form-control" name="name_ru" type="text" id="name_ru" value="{{ $category->name_ru or ''}}">
                                            {!! $errors->first('name_ru', '<p class="error-block">:message</p>') !!}
                                        </div>
                                        <div class="tab-pane active" id="tab_3">
                                            <div class="{{ $errors->has('description_ru') ? 'has-error' : ''}}">
                                                <label for="description_ru" class="control-label required">{{ 'Oписание' }}</label>
                                                <textarea name="description_ru" id="description_ru" cols="30" class="form-control" placeholder="Название" rows="10">{{ $category->description_ru }}</textarea>
                                                {!! $errors->first('description_ru', '<p class="error-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="{{ $errors->has('name_en') ? 'has-error' : ''}}">
                                            <label for="name_en" class="control-label required">{{ 'Title' }}</label>
                                            <input class="form-control" name="name_en" type="text" id="name_en" value="{{ $category->name_en or ''}}">
                                            {!! $errors->first('name_en', '<p class="error-block">:message</p>') !!}
                                        </div>
                                        <div class="tab-pane" id="tab_4">
                                            <div class="{{ $errors->has('description_en') ? 'has-error' : ''}}">
                                                <label for="description_en" class="control-label required">{{ 'Description' }}</label>
                                                <textarea name="description_en" id="description_en" class="form-control" cols="30" rows="10">{{ $category->description_en }}</textarea>
                                                {!! $errors->first('description_en', '<p class="error-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="photo" class="control-label">{{ 'Фото' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="photo" type="file" id="photo">
                            </div>
                            <div class="img-input-show">
                                @if($category->photo)
                                    <img src="{{ App\Libraries\UploadManager::getPhoto('categories', $category->id, 'original', $category->photo)}}" class="img-thumbnail" width="150"/>
                                    <a href="{{ url('/admin/categories/removeFile/'.$category->id) }}" >удалить фото</a>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/categories') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
