@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Добавить журнал @else Редактировать журнал @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')

                    <form method="POST" action="{{ url('/admin/setting/'.$find_setting->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12 {{ $errors->has('title') ? 'has-error' : ''}}">
                            <label for="title" class="control-label">{{ 'Название' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="title" type="text" id="title" value="{{ $find_setting->title or ''}}">
                            </div>
                            {!! $errors->first('title', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12 {{ $errors->has('setting_key') ? 'has-error' : ''}}">
                            <label for="setting_key" class="control-label">{{ 'Ключ' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
                                <input class="form-control" name="setting_key" type="text" id="setting_key" value="{{ $find_setting->setting_key or ''}}">
                            </div>
                            {!! $errors->first('setting_key', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12 {{ $errors->has('setting_value') ? 'has-error' : ''}}">
                            <label for="setting_value" class="control-label">{{ 'Значение' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
                                <input class="form-control" name="setting_value" type="text" id="setting_value" value="{{ $find_setting->setting_value or ''}}">
                            </div>
                            {!! $errors->first('setting_value', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/setting') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
