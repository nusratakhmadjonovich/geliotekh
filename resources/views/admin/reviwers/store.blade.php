@extends('layouts.admin')

@section('title')
    send reviever
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/reviewers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
                <div class="box-body">
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{url('admin/reviewers/'.$find_review->id.'/update')}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
                            <label for="name" class="col-md-4 control-label">{{ 'Title' }}</label>
                            <div class="col-md-6">
                                <input class="form-control" name="title" type="text" id="title" disabled value="{{ $find_review->title or ''}}" required>
                                {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">{{ 'Last Article File' }}</label>
                            <div class="col-md-6">
                                <select name="article_meta_id" class="form-control" id="article_meta_id">
                                    <option value="{{ $find_review->id }}">{{ $find_review->article_file->comment }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">{{ 'Last Article File upload' }}</label>
                            <div class="col-md-6">
                                <a href="{{ asset($find_review->article_file->file) }}" target="_blank">{{ $find_review->article_file->comment }}</a>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
                            <label for="name" class="col-md-4 control-label">{{ 'Check and Date' }}</label>
                            <div class="col-md-6">
                                <input class="form-control" name="check_and_date" type="text"  id="datepicker2" value="{{ $find_review->cheking_and_date or ''}}" required>
                                {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
                            <label for="name" class="col-md-4 control-label">{{ 'Send Reviewer' }}</label>
                            <div class="col-md-3">
                               @foreach($find_users as $users)
                                        <p>
                                            <input type="checkbox" id="user_id{{$users->id}}" @foreach($find_review_file as $reviewer) @if($users->id == $reviewer->user_id) checked @endif @endforeach name="users[]" value="{{ $users->id }}" >
                                            <label for="user_id{{ $users->id }}">{{ $users->email }}</label>
                                        </p>
                                @endforeach
                                {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <input class="btn btn-primary" type="submit" value="send">
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
