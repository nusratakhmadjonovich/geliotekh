
<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 control-label">{{ 'Title' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="title" type="text" id="title" disabled value="{{ $edit_reviewer->title or ''}}" required>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="article_meta_id" class="col-md-4 control-label">{{ 'last File' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="article_meta_id" type="text" id="article_meta_id" value="{{ $edit_reviewer->article_file->comment or ''}}" required>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="article_meta_id" class="col-md-4 control-label">{{ 'last Uploaded File' }}</label>
    <div class="col-md-6">
        <a href="{{ asset($edit_reviewer->article_file->file) }}" target="_blank"> {{$edit_reviewer->article_file->comment}} </a>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="cheking_date" class="col-md-4 control-label">{{ 'Checking and Date' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="cheking_date" type="text" id="datepicker2" value="{{ $edit_reviewer->article_file->reviewer_article->cheking_and_date or ''}}" required>
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fio') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 control-label">{{ 'Send Reviewer' }}</label>
    <div class="col-md-3">
        @foreach($find_users as $users)
                            <p>
                                <input type="checkbox" @foreach($reviewer as $reviewerlar) @if($users->id == $reviewerlar->user_id) checked @endif @endforeach id="check{{$users->id}}" name="users[]" value="{{$users->id}}" > <label for="check{{$users->id}}">{{ $users->id }}.{{ $users->email }}</label>
                            </p>
        @endforeach
        {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="update">
    </div>
</div>
