@extends('layouts.admin')

@section('title')
    Reviewer Articles
@stop

@section('content')
    @include('sweet::alert')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('admin/reviewers/create') }}" class="btn btn-success btn-sm" title="Add New Models\Admin">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </div>

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <form method="POST" id="search-form" class="form-inline" role="form">

                        <div class="form-group">
                            <input type="text" class="form-control" name="id" id="id" placeholder="id">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" id="name" placeholder="name">
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>

                    <br>
                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th> Title </th>
                                <th> Created At </th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.reviewers.index.get.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data:'title',name:'title'},
                {data:'created_at',name:'created_at'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection
