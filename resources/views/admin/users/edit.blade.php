@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Создать пользователь @else Редактировать пользователь @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')

                    <form method="POST" action="{{ url('/admin/users/'.$user->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : ''}}">
                                    <label for="email" class="control-label">{{ 'Email' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                        <input class="form-control" name="email" type="text" id="email" value="{{ old('email', $user->email) }}">
                                    </div>
                                    {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('password') ? 'has-error' : ''}}">
                                    <label for="password" class="control-label">{{ 'Пароль' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input class="form-control" name="password" type="password" id="password">
                                    </div>
                                    {!! $errors->first('password', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                    <label for="password_confirmation" class="control-label">{{ 'Пов. пароль' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input class="form-control" name="password_confirmation" type="password" id="password_confirmation" >
                                    </div>
                                    {!! $errors->first('password_confirmation', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="status" class="control-label">{{ 'Статус' }}</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-success btn-rounded {{ old('status', $user->status) == 1 || $isNewRecord ? 'active' : '' }} btn-md form-check-label">
                                            <input class="form-check-input" name="status"  value="1" type="radio" {{ old('status', $user->status) == 1  || $isNewRecord ? 'checked' : '' }}> Актив
                                        </label>

                                        <label class="btn btn-danger btn-rounded {{ old('status', $user->status) == 0 ? 'active' : '' }} btn-md form-check-label">
                                            <input class="form-check-input" name="status" value="0" type="radio" {{ old('status', $user->status) == 0 ? 'checked' : '' }}> Неактив
                                        </label>

                                        <label class="btn btn-info btn-rounded {{ old('status', $user->status) == -2 ? 'active' : '' }} btn-md form-check-label">
                                            <input class="form-check-input" name="status" value="-2" type="radio" {{ old('status', $user->status) == -2 ? 'checked' : '' }}> Не потдвержден
                                        </label>
                                    </div>
                                    {!! $errors->first('status', '<p class="error-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="row">

                                <div class="form-group col-md-12 {{ $errors->has('fio') ? 'has-error' : ''}}">
                                    <label for="fio" class="control-label">{{ 'Ф.И.О' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                        <input class="form-control" name="fio" type="text" id="fio" value="{{ old('fio', $user->fio) }}" >
                                    </div>
                                    {!! $errors->first('fio', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="is_editor" name="is_editor"  value="1" @if(old('is_editor')) checked @endif {{ $user->is_editor ? 'checked':''}}> Ред. коллега
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="is_reviewer" value="1" @if(old('is_reviewer')) checked @endif {{$user->is_reviewer ? 'checked':''}}> Рецензент
                                        </label>
                                    </div>
                                </div>
                                <div id="editor_form" style="display:{{$user->is_editor ? 'show':'none'}};">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_editors_chairman" @if(old('is_editors_chairman')) checked @endif {{$user->is_editors_chairman ? 'checked':''}}> Председатель редакционной коллегии
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 {{ $errors->has('editor_title') ? 'has-error' : ''}}">
                                        <label for="editor_title" class="control-label">{{ 'Title' }}</label>
                                        <input class="form-control" name="editor_title" type="text" id="editor_title" value="{{ old('editor_title', $user->editor_title) }}" placeholder="кандидат философских наук, доцент, доцент кафедры философии КУбГАУ, г. Краснодар">
                                        {!! $errors->first('editor_title', '<p class="error-block">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="photo" class="control-label">Фото</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-file glyphicon glyphicon-file"></i></div>
                                        <input class="form-control" name="photo" type="file" id="photo"  style="width: 305px;">
                                    </div>

                                    <div class="img-input-show">
                                        @if($user->photo)
                                            <img src="{{ App\Libraries\UploadManager::getPhoto('profile', $user->id, 'original', $user->photo)}}" class="img-thumbnail" width="150"/>
                                            <a href="{{ url('admin/users/removePhoto/'.$user->id) }}"  >удалить фото</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/users') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('public/js/bootstrap-fancyfile.js') }}"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-fancyfile.css') }}">
@endpush

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#is_editor').change(function(){
                if($(this).prop("checked")) {
                    $('#editor_form').show();
                } else {
                    $('#editor_form').hide();
                }
            });

            @if($user->picture)
                picture_html  = '<span class="picture">';
                picture_html += '    <a href="{{ url('uploads/' . $user->picture->id . '/download') }}">';
                picture_html += '        <span id="download-picture" class="text-primary">';
                picture_html += '            <i class="fa fa-file-{{ $user->picture->aggregate_type }}-o"></i> {{ $user->picture->basename }}';
                picture_html += '        </span>';
                picture_html += '    </a>';
                picture_html += '    {!! Form::open(['id' => 'picture-' . $user->picture->id, 'method' => 'DELETE', 'url' => [url('uploads/' . $user->picture->id)], 'style' => 'display:inline']) !!}';
                picture_html += '    <a id="remove-picture" href="javascript:void();">';
                picture_html += '        <span class="text-danger"><i class="fa fa fa-times"></i></span>';
                picture_html += '    </a>';
                picture_html += '    {!! Form::close() !!}';
                picture_html += '</span>';ssssssss

                $(document).on('click', '#remove-picture', function (e) {
                    confirmDelete("#picture-{!! $user->picture->id !!}", "{!! trans('general.attachment') !!}", "{!! trans('general.delete_confirm', ['name' => '<strong>' . $user->picture->basename . '</strong>', 'type' => strtolower(trans('general.attachment'))]) !!}", "{!! trans('general.cancel') !!}", "{!! trans('general.delete')  !!}");
                });
            @endif
        });
    </script>
@endsection