@extends('layouts.admin')

@section('title')
    Пользователи
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/users/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Создать
                    </a>
                </div>

                @include('includes.messages') @include('includes.error_messages')
                <div class="box-body">
                    <div class="search-box">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                <select  class="form-control"  name="status">
                                    <option value=""></option>
                                    <option value="1">Актив</option>
                                    <option value="0">Неактив</option>
                                    <option value="-2">Не подтвержден</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="fio" id="name" placeholder="Ф.И.О">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input class="form-check-input" name="is_editor" value="1" type="checkbox"> Ред. коллега
                            </div>
                            <div class="form-group">
                                <input class="form-check-input" name="is_reviewer" value="1" type="checkbox"> Рецензент
                            </div>

                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </form>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Статус</th>
                                <th>Ф.И.О</th>
                                <th>Email</th>
                                <th>Редколлега</th>
                                <th>Рецензент</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  url('admin/users/list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'status', name: 'status'},
                {data: 'fio', name: 'fio'},
                {data: 'email', name: 'email'},
                {data: 'is_editor', name: 'is_editor'},
                {data: 'is_reviewer', name: 'is_reviewer'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection

