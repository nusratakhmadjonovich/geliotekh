@extends('layouts.admin')

@section('title')
    Выпуски
@stop

@section('content')
    @include('sweet::alert')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/journal/editions/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                    </a>
                </div>

                @include('includes.messages') @include('includes.error_messages')
                <div class="box-body">
                    <div class="search-box">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                <input type="text" class="form-control" name="year" id="name" placeholder="Год">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="number" id="number" placeholder="Номер выпуска">
                            </div>
                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </form>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table" id="users-table">
                            <thead>
                            <tr>
                                <th width="20">#</th>
                                <th>Журнал</th>
                                <th>Выпуск</th>
                                <th>Прием статей</th>
                                <th width="10%">Действие</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.journal.edition.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data:'journal.name',name:'journal_id'},
                {data:'null', name:'edition', 'render':function ( data, type, row ) {
                    return row.year +' ('+ row.number + ')';
                }},
                {data: 'receiving_articles', name: 'receiving_articles'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection
