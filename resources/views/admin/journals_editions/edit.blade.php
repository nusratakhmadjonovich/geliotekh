@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Добавить выпуск @else Редактировать выпуск @endif
@stop


@section('content')
    @include('sweet::alert')
    <div class="row">
        <form method="POST" action="{{url('/admin/journal/editions/'.$journal_edition->id)}}" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @include('includes.messages') @include('includes.error_messages')
                        <div class="col-md-8">
                            <div class="row">
                                <div class="form-group col-md-12 {{ $errors->has('journal_id') ? 'has-error' : ''}}">
                                    <label for="journal_id" class="control-label">{{ 'Журнал' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa fa-book"></i></div>
                                        <select  class="form-control"  name="journal_id" id="journal_id">
                                            @foreach($journals as $journal)
                                                <option value="{{$journal->id}}">{{$journal->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {!! $errors->first('journal_id', '<p class="error-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('year') ? 'has-error' : ''}}">
                                    <label for="year" class="control-label">{{ 'Год' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" id="year" class="form-control yearpicker" name="year" value="{{ old('year', $journal_edition->year) }}">
                                    </div>
                                    {!! $errors->first('year', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('number') ? 'has-error' : ''}}">
                                    <label for="number" class="control-label">{{ 'Выпуск' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-angle-double-up"></i></div>
                                        <input type="number" id="number" class="form-control" name="number" value="{{ old('number', $journal_edition->number)  }}" >
                                    </div>
                                    {!! $errors->first('number', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <div class="{{ $errors->has('description_ru') ? 'has-error' : ''}}">
                                                <label for="description_ru" class="control-label">{{ 'Описание' }}</label>
                                                <textarea name="description_ru" class="form-control">{{ old('description_ru', $journal_edition->description_ru) }}</textarea>
                                                {!! $errors->first('description_ru', '<p class="error-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_2">
                                            <div class="{{ $errors->has('description_en') ? 'has-error' : ''}}">
                                                <label for="description_en" class="control-label">{{ 'Description' }}</label>
                                                <textarea name="description_en" class="form-control">{{ old('description_en', $journal_edition->description_en) }}</textarea>
                                                {!! $errors->first('description_en', '<p class="error-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                        <a href="{{ url('/admin/journal/editions') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                        <a href="{{ url('/admin/journal/editions') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 {{ $errors->has('receiving_articles') ? 'has-error' : ''}}">
                                    <label for="receiving_articles" class="control-label">{{ 'Прием статей' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" id="receiving_articles" class="form-control datepicker" name="receiving_articles" value="{{ old('receiving_articles', $journal_edition->receiving_articles) }}">
                                    </div>
                                    {!! $errors->first('receiving_articles', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('date_edition') ? 'has-error' : ''}}">
                                    <label for="date_edition" class="control-label">{{ 'Дата издания' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" id="date_edition" class="form-control datepicker" name="date_edition" value="{{ old('date_edition', $journal_edition->date_edition) }}" >
                                    </div>
                                    {!! $errors->first('date_edition', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('placement_electronic_monographs') ? 'has-error' : ''}}">
                                    <label for="placement_electronic_monographs" class="control-label">{{ 'Размещение журнала в электронном виде' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" id="placement_electronic_monographs" class="form-control datepicker" name="placement_electronic_monographs" value="{{ old('placement_electronic_monographs', $journal_edition->placement_electronic_monographs) }}" >
                                    </div>
                                    {!! $errors->first('placement_electronic_monographs', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('distribution_print_copies_monographs') ? 'has-error' : ''}}">
                                    <label for="distribution_print_copies_monographs" class="control-label">{{ 'Рассылка печатных журналов, сертификатов и оттисков' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" id="distribution_print_copies_monographs" class="form-control datepicker" name="distribution_print_copies_monographs" value="{{ old('distribution_print_copies_monographs', $journal_edition->distribution_print_copies_monographs) }}" >
                                    </div>
                                    {!! $errors->first('distribution_print_copies_monographs', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('mailing_editions') ? 'has-error' : ''}}">
                                    <label for="mailing_editions" class="control-label">{{ 'Рассылка номеров почтовых отправлений' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" id="mailing_editions" class="form-control datepicker" name="mailing_editions" value="{{ old('mailing_editions', $journal_edition->mailing_editions) }}" >
                                    </div>
                                    {!! $errors->first('mailing_editions', '<p class="error-block">:message</p>') !!}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('file') ? 'has-error' : ''}}" >
                                    <label for="file" class="control-label">{{ 'Файл' }}</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                        <input class="form-control" name="file" type="file" id="file" value="{{ $article->file or ''}}">
                                    </div>
                                    {!! $errors->first('file', '<p class="error-block">:message</p>') !!}
                                    @if($journal_edition->file)
                                        <a href="{{ App\Libraries\UploadManager::getFile('journal_edition', $journal_edition->id, $journal_edition->file)}}" target="_blank"> {{$journal_edition->file}} </a>
                                        <a href="{{ url('/admin/journal/editions/removeDoc/'.$journal_edition->id) }}" target="_blank" >удалить файл</a>
                                    @endif
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('photo') ? 'has-error' : ''}}">
                                    <label for="photo" class="control-label">Фото</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-file glyphicon glyphicon-file"></i></div>
                                        <input class="form-control" name="photo" type="file" id="photo">
                                    </div>
                                    <div class="img-input-show">
                                        @if($journal_edition->photo)
                                            <img src="{{ App\Libraries\UploadManager::getPhoto('journal_edition', $journal_edition->id, 'original', $journal_edition->photo)}}" class="img-thumbnail" width="150"/>
                                            <a href="{{ url('/admin/journal/editions/removeFile/'.$journal_edition->id) }}" >удалить фото</a>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <script>
         var options = {
             filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
             filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
             filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
             filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
         };
         CKEDITOR.replace('description_ru', options);
         CKEDITOR.replace('description_en', options);
    </script>
@stop