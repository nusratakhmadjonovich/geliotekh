@extends('layouts.admin')

@section('title')
    Show Article
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/journal/edition') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/journal/edition/'.$journal_edition->id.'/edit') }}" title="Edit Models\Admin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                    <form method="POST" action="{{ url('/admin/journal/edition/'.$journal_edition->id) }}" accept-charset="UTF-8" style="display:inline">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="0dhbvqU9g44vKlrS0p6cX8M5xShELeDMpvmzdvZK">
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Admin" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                    </form>
                </div>


            @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <div class="table-responsive">
                        <h1 class="pull-right">username</h1>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>ID</th><td>{{ $journal_edition->id }}</td>
                            </tr>
                            <tr>
                                <th> Year </th>
                                <td> {{ $journal_edition->year }} </td>
                            </tr>
                            <tr>
                                <th> Journal Number </th>
                                <td> {{ $journal_edition->number }} </td>
                            </tr>
                            <tr>
                                <th>description</th>
                                <td>{{ $journal_edition->receiving_articles}}</td>
                            </tr>
                            <tr>
                                <th> Receiving Article </th>
                                <td> {{ $journal_edition->receiving_articles }} </td>
                            </tr>
                            <tr>
                                <th> editing journal </th>
                                <td> {{ $journal_edition->editing_journal}} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
