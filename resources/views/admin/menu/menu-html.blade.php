@extends('layouts.admin')

@section('content')
<?php
	$currentUrl = url()->current();
?>
<div id="hwpwrap">
	<div class="custom-wp-admin wp-admin wp-core-ui js   menu-max-depth-0 nav-menus-php auto-fold admin-bar">
		<div id="wpwrap">
			<div id="wpcontent">
				<div id="wpbody">
					<div id="wpbody-content">

						<div class="wrap">

							<div class="manage-menus">
								<form method="get" action="{{ $currentUrl }}">
									<label for="menu" class="selected-menu">Выберите меню, которое вы хотите отредактировать:</label>

									{!! Menu::dropdown('menu', $menulist) !!}

									<span class="submit-btn">
										<input type="submit" class="button-secondary" value="Выбрать">
									</span>
									<span class="add-new-menu-action"> or <a href="{{ $currentUrl }}?action=edit&menu=0">Создать новое меню</a>. </span>
								</form>
							</div>
							<div id="nav-menus-frame">

								@if(request()->has('menu')  && !empty(request()->input("menu")))
								<div id="menu-settings-column" class="metabox-holder">
									<div class="clear"></div>
									<form id="nav-menu-meta" action="" class="nav-menu-meta" method="post" enctype="multipart/form-data">
										<div id="side-sortables" class="accordion-container">
											<ul class="outer-border">
												<li class="control-section accordion-section  open add-page" id="add-page">
													<h3 class="accordion-section-title hndle" tabindex="0"> Пользовательская ссылка <span class="screen-reader-text">Press return or enter to expand</span></h3>
													<div class="accordion-section-content ">
														<div class="inside">
															<div class="customlinkdiv" id="customlinkdiv">
																<p id="menu-item-url-wrap">
																	<label class="howto" for="custom-menu-item-url"> <span>URL</span>&nbsp;&nbsp;&nbsp;
																		<input id="custom-menu-item-url" name="url" type="text" class="code menu-item-textbox" value="">
																	</label>
																</p>

																<p id="menu-item-name-wrap">
																	<label class="howto" for="custom-menu-item-name_ru"> <span> Label</span>&nbsp;
																		<input id="custom-menu-item-name_ru" name="label_ru" type="text" class="regular-text menu-item-textbox input-with-default-title" title="Ру">
																	</label>
																</p>
																<p id="menu-item-name-wrap">
																	<label class="howto" for="custom-menu-item-name_en"> <span>Label</span>&nbsp;
																		<input id="custom-menu-item-name_en" name="label_en" type="text" class="regular-text menu-item-textbox input-with-default-title" title="Eng">
																	</label>
																</p>

																<p class="button-controls">
																	<a  href="#" onclick="addcustommenu()"  class="button-secondary submit-add-to-menu right"  >Создать</a>
																	<span class="spinner" id="spincustomu"></span>
																</p>

															</div>
														</div>
													</div>
												</li>

											</ul>
										</div>
									</form>

								</div>
								@endif
								<div id="menu-management-liquid">
									<div id="menu-management">
										<form id="update-nav-menu" action="" method="post" enctype="multipart/form-data">
											<div class="menu-edit ">
												<div id="nav-menu-header">
													<div class="major-publishing-actions">
														<label class="menu-name-label howto open-label" for="menu-name"> <span>Меню</span>
															<input name="menu-name" id="menu-name" type="text" class="menu-name regular-text menu-item-textbox" title="Enter menu name" value="@if(isset($indmenu)){{$indmenu->name}}@endif">
															<input type="hidden" id="idmenu" value="@if(isset($indmenu)){{$indmenu->id}}@endif" />
														</label>

														@if(request()->has('action'))
														<div class="publishing-action">
															<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Сохранить меню</a>
														</div>
														@elseif(request()->has("menu"))
														<div class="publishing-action">
															<a onclick="getmenus()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Сохранить меню</a>
															<span class="spinner" id="spincustomu2"></span>
														</div>

														@else
														<div class="publishing-action">
															<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Сохранить</a>
														</div>
														@endif
													</div>
												</div>
												<div id="post-body">
													<div id="post-body-content">

														@if(request()->has("menu"))
														<h3>Структура</h3>
														<div class="drag-instructions post-body-plain" style="">
															<p>
																Поместите каждый элемент в желаемом порядке. Нажмите на стрелку справа от элемента, чтобы отобразить дополнительные параметры конфигурации
															</p>
														</div>

														@else
														<h3>Menu Creation</h3>
														<div class="drag-instructions post-body-plain" style="">
															<p>
																Введите имя и нажмите кнопку «Создать меню»
															</p>
														</div>
														@endif

														<ul class="menu ui-sortable" id="menu-to-edit">
															@if(isset($menus))
															@foreach($menus as $m)
															<li id="menu-item-{{$m->id}}" class="menu-item menu-item-depth-{{$m->depth}} menu-item-page menu-item-edit-inactive pending" style="display: list-item;">
																<dl class="menu-item-bar">
																	<dt class="menu-item-handle">
																		<span class="item-title"> <span class="menu-item-title"> <span id="menutitletemp_{{$m->id}}">{{$m->label_ru}}</span> <span style="color: transparent;">|{{$m->id}}|</span> </span> <span class="is-submenu" style="@if($m->depth==0)display: none;@endif">Subelement</span> </span>
																		<span class="item-controls"> <span class="item-type">Link</span> <span class="item-order hide-if-js">
																			<a href="{{ $currentUrl }}?action=move-up-menu-item&menu-item={{$m->id}}&_wpnonce=8b3eb7ac44" class="item-move-up"><abbr title="Move Up">↑</abbr></a> |
																			<a href="{{ $currentUrl }}?action=move-down-menu-item&menu-item={{$m->id}}&_wpnonce=8b3eb7ac44" class="item-move-down"><abbr title="Move Down">↓</abbr></a> </span>
																		<a class="item-edit" id="edit-{{$m->id}}" title=" " href="{{ $currentUrl }}?edit-menu-item={{$m->id}}#menu-item-settings-{{$m->id}}"> </a> </span>
																	</dt>
																</dl>

																<div class="menu-item-settings" id="menu-item-settings-{{$m->id}}">
																	<input type="hidden" class="edit-menu-item-id" name="menuid_{{$m->id}}" value="{{$m->id}}" />
																	<p class="description description-thin">
																		<label for="edit-menu-item-title-ru-{{$m->id}}"> Label Ру
																			<br>
																			<input type="text" id="idlabelmenu_ru_{{$m->id}}" class="widefat edit-menu-item-title edit-menu-item-title_ru" name="idlabelmenu__ru_{{$m->id}}" value="{{$m->label_ru}}">
																		</label>
																	</p>
																	<p class="description description-thin">
																		<label for="edit-menu-item-title-en-{{$m->id}}"> Label Eng
																			<br>
																			<input type="text" id="idlabelmenu_en_{{$m->id}}" class="widefat edit-menu-item-title edit-menu-item-title_en" name="idlabelmenu__en_{{$m->id}}" value="{{$m->label_en}}">
																		</label>
																	</p>
																	<p class="field-css-classes description description-thin">
																		<label for="edit-menu-item-classes-{{$m->id}}"> Class CSS (optional)
																			<br>
																			<input type="text" id="clases_menu_{{$m->id}}" class="widefat code edit-menu-item-classes" name="clases_menu_{{$m->id}}" value="{{$m->class}}">
																		</label>
																	</p>

																	<p class="field-css-url description description-wide">
																		<label for="edit-menu-item-url-{{$m->id}}"> Url
																			<br>
																			<input type="text" id="url_menu_{{$m->id}}" class="widefat code edit-menu-item-url" id="url_menu_{{$m->id}}" value="{{$m->link}}">
																		</label>
																	</p>

																	<p class="field-move hide-if-no-js description description-wide">
																		<label> <span>Move</span> <a href="{{ $currentUrl }}" class="menus-move-up" style="display: none;">Move up</a> <a href="{{ $currentUrl }}" class="menus-move-down" style="display: inline;">Move Down</a> <a href="{{ $currentUrl }}" class="menus-move-left" style="display: none;"></a> <a href="{{ $currentUrl }}" class="menus-move-right" style="display: none;"></a> <a href="{{ $currentUrl }}" class="menus-move-top" style="display: none;">Top</a> </label>
																	</p>

																	<div class="menu-item-actions description-wide submitbox">

																		<a class="item-delete submitdelete deletion" id="delete-{{$m->id}}" href="{{ $currentUrl }}?action=delete-menu-item&menu-item={{$m->id}}&_wpnonce=2844002501">Удалить</a>
																		<span class="meta-sep hide-if-no-js"> | </span>
																		<a class="item-cancel submitcancel hide-if-no-js button-secondary" id="cancel-{{$m->id}}" href="{{ $currentUrl }}?edit-menu-item={{$m->id}}&cancel=1424297719#menu-item-settings-{{$m->id}}">Отменить</a>
																		<span class="meta-sep hide-if-no-js"> | </span>
																		<a onclick="updateitem({{$m->id}})" class="button button-primary updatemenu" id="update-{{$m->id}}" href="javascript:void(0)">Редактировать</a>

																	</div>

																</div>
																<ul class="menu-item-transport"></ul>
															</li>
															@endforeach
															@endif
														</ul>
														<div class="menu-settings">

														</div>
													</div>
												</div>
												<div id="nav-menu-footer">
													<div class="major-publishing-actions">

														@if(request()->has('action'))
														<div class="publishing-action">
															<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Создать меню</a>
														</div>
														@elseif(request()->has("menu"))
														<span class="delete-action"> <a class="submitdelete deletion menu-delete" onclick="deletemenu()" href="javascript:void(9)">Удалить меню</a> </span>
														<div class="publishing-action">

															<a onclick="getmenus()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Сохранить меню</a>
															<span class="spinner" id="spincustomu2"></span>
														</div>

														@else
														<div class="publishing-action">
															<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Создать меню</a>
														</div>
														@endif
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>

						<div class="clear"></div>
					</div>

					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="clear"></div>
		</div>
	</div>
</div>
@endsection

@section('styles')
	<link href="{{asset('/backend/menu/style.css')}}" rel="stylesheet">
@endsection

@section('scripts')
	<script>
		var menus = {
			"oneThemeLocationNoMenus" : "",
			"moveUp" : "Move up",
			"moveDown" : "Mover down",
			"moveToTop" : "Move top",
			"moveUnder" : "Move under of %s",
			"moveOutFrom" : "Out from under  %s",
			"under" : "Under %s",
			"outFrom" : "Out from %s",
			"menuFocus" : "%1$s. Element menu %2$d of %3$d.",
			"subMenuFocus" : "%1$s. Menu of subelement %2$d of %3$s."
		};
		var arraydata = [];
		var addcustommenur= '{{ route("admin.menu.addcustommenu") }}';
		var updateitemr= '{{ route("admin.menu.updateitem")}}';
		var generatemenucontrolr= '{{ route("admin.menu.generatemenucontrol") }}';
		var deleteitemmenur= '{{ route("admin.menu.deleteitemmenu") }}';
		var deletemenugr= '{{ route("admin.menu.deletemenug") }}';
		var createnewmenur= '{{ route("admin.menu.createnewmenu") }}';
		var csrftoken="{{ csrf_token() }}";
		var menuwr = "{{ url()->current() }}";

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': csrftoken
			}
		});
	</script>
	<script type="text/javascript" src="{{asset('/backend/menu/scripts.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/menu/scripts2.js')}}"></script>
	<script type="text/javascript" src="{{asset('/backend/menu/menu.js')}}"></script>
@endsection