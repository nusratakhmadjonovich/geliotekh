@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Создать @else Редактировать @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')

                    <form method="POST" action="{{ url('/admin/blocks/'.$block->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12 {{ $errors->has('block_key') ? 'has-error' : ''}}">
                            <label for="block_key" class="control-label">{{ 'Ключ' }}</label>
                            <input class="form-control" name="block_key" type="text" id="block_key" value="{{  old('blcok_key', $block->block_key) }}" >
                            {!! $errors->first('block_key', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="{{ $errors->has('block_data_ru') ? 'has-error' : ''}}">
                                        <label for="block_data_ru" class="control-label">{{ 'Информация' }}</label>
                                        <div id="editor_ru" style="width: 100%; height: 200px">{{ old('block_data_ru', $block->block_data_ru) }}</div>
                                        <textarea  id="block_data_ru" name="block_data_ru" style="display:none;">{{ old('block_data_ru', $block->block_data_ru) }}</textarea>
                                        {!! $errors->first('block_data_ru', '<p class="error-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="{{ $errors->has('block_data_en') ? 'has-error' : ''}}">
                                        <label for="block_data_en" class="control-label">{{ 'Information' }}</label>
                                        <div id="editor_en" style="width: 100%; height: 200px">{{ old('block_data_en', $block->block_data_en) }}</div>
                                        <textarea  id="block_data_en" name="block_data_en" style="display:none;">{{ old('block_data_en', $block->block_data_en) }}</textarea>
                                        {!! $errors->first('block_data_en', '<p class="error-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <button type="submit" id="block_submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/blocks') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.3.3/ace.js"></script>

    <script>
        var editor_ru = ace.edit('editor_ru');
        editor_ru.setTheme("ace/theme/monokai");
        editor_ru.session.setMode("ace/mode/html");
        editor_ru.getSession().on('change', function(){
            $('#block_data_ru').val(editor_ru.getSession().getValue());
        });

        var editor_en = ace.edit('editor_en');
        editor_en.setTheme("ace/theme/monokai");
        editor_en.session.setMode("ace/mode/html");
        editor_en.getSession().on('change', function(){
            $('#block_data_en').val(editor_en.getSession().getValue());
        });
    </script>
@stop
