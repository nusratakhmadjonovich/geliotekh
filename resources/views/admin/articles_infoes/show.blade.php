@extends('layouts.admin')

@section('title')

@endsection

@section('content')
    <br>
<div class="row">
    <div class="col-md-offset-1 col-md-10">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">About Article Meta Reviewers</h3>
                <a href="{{ url('/admin/article/meta/info/index') }}" class="btn btn-success pull-right"><i class="fa  fa-arrow-circle-o-left"></i>  back </a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt><strong class="bg bg-success" style="padding: 25px;">Username</strong></dt>
                    <dd><strong>:   {{ $find_user->fio }}</strong></dd>
                    <hr>
                    <br>
                    <dt><strong class="bg bg-success" style="padding: 25px;">Comment</strong></dt>
                    <dd> {{ $reviewer_response->comment }}</dd>
                    <hr>
                    <dt><strong class="bg bg-success" style="padding: 25px;">Created date </strong></dt>
                    <dd><strong>{{ $reviewer_response->created_at }}</strong></dd>
                    <br>
                    <hr>
                    <dt><h3><strong class="bg bg-success" style="padding-top: 10px;padding-left:15px; padding-right:15px;padding-bottom: 20px;">Result</strong></h3></dt>
                    <dd><h3><strong>{{ $reviewer_response->result }}</strong></h3></dd>
                    <hr>
                </dl>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- ./col -->
</div><!-- /.row -->

@endsection