@extends('layouts.admin')

@section('title')

@stop

@section('content')

    <h1 class="text-center" style="background-color: #CCE9F7; padding: 15px;border-radius: 7px;">User Reviewer Articles</h1>
        <!-- Main content -->
        @foreach($article_meta as $user_article_meta)
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> {{ $user_article_meta->comment }}
                        <small class="pull-right">Date: <strong class="bg bg-info" style="margin: 10px;padding:10px; border-radius: 5px;">{{ $user_article_meta->created_at }}</strong></small>
                    </h2>
                </div><!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    Article File parameters
                    <address>
                    </address>
                </div><!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>created ad:</b> {{ $user_article_meta->created_at }} <br>
                    <address>
                    </address>
                </div><!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <a href="{{ asset($user_article_meta->file) }}" class="btn btn-success" target="_blank"><i class="fa fa-download pull-right"></i> download article file </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
            <hr>
            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>comment</th>
                            <th>result</th>
                            <th>created at</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user_article_meta->article_meta_reviewer_response as $reviewer_response)
                        <tr>
                            <td>{{ $reviewer_response->id }}</td>
                            @if(str_word_count($reviewer_response->comment) < 3 )
                                <td>{{ $reviewer_response->comment }}...</td>
                            @else

                                <td>{{
                                         Illuminate\Support\Str::substr($reviewer_response->comment,0, 25)
                                    }}...
                                </td>
                             @endif
                            <td>{{ $reviewer_response->result }}</td>
                            <td>{{ $reviewer_response->created_at }}</td>
                            <td class="pull-right"><a href="{{ url('admin/article/meta/info/'.$reviewer_response->id) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> show  </a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.col -->
            </div><!-- /.row -->
            <!-- this row will not appear when printing -->
        </section>
        @endforeach
        <!-- /.content -->
        <div class="clearfix"></div>
    <!-- /.content-wrapper -->
@stop