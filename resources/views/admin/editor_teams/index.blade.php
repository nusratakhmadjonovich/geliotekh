@extends('layouts.admin')

@section('title')
    Editors Teams
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/editors/team/create') }}" class="btn btn-success btn-sm" title="Add New Models\Admin">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </div>

                @if(\Session::has('success'))
                    <div class="alert alert-success">
                        {{\Session::get('success')}}
                    </div>
                @endif
                <div class="box-body">
                    <form method="POST" id="search-form" class="form-inline" role="form">

                        <div class="form-group">
                            <input type="text" class="form-control" name="id" id="id" placeholder="Id">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="fio" id="fio" placeholder="Fio">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="image" id="image" placeholder="Image">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="description" id="desription" placeholder="Description">
                        </div>

                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>

                    <br>
                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>FIO</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.editors.teams.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'fio', name: 'fio'},
                {data: 'image', name: 'image'},
                {data:'description',name:'descroiption'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection
