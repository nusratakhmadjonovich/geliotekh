@extends('layouts.admin')

@section('title')
    Edit Journal Teams
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('admin/journal/editors/team') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')
                    <form method="POST" action="{{ url('/admin/editors/team/'.$editor_team_edit->id.'/update') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-offset-2 col-md-8 {{ $errors->has('fio') ? 'has-error' : ''}}">
                            <label for="fio" class="control-label">{{ 'FIO' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                                <input class="form-control" name="fio" type="text" id="fio" value="{{ $editor_team_edit->fio or ''}}"  placeholder="fio..." required>
                            </div>
                            {!! $errors->first('fio', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-offset-2 col-md-8 {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class="control-label bg bg-success">{{ 'Image:' }}</label>
                            {{ $editor_team_edit->image or ''}}
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-file"></i></div>
                                <input class="form-control" name="image" type="file" id="image" value="{{ $editor_team_edit->image or ''}}" accept=".png, .bmp, .jpg , .psd" placeholder="image..." required>

                            </div>
                            {!! $errors->first('image', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-offset-2 col-md-8 {{ $errors->has('description') ? 'has-error' : ''}}">
                            <label for="description" class="control-label">{{ 'description' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-title"></i></div>
                                <input class="form-control" name="description" type="text" id="description" value="{{ $editor_team_edit->description or '' }}" placeholder="description..." required>
                            </div>
                            {!! $errors->first('description', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="col-md-offset-2 col-md-8">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Save</button>
                                <a href="{{ url('admin/journal/editors/team') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
