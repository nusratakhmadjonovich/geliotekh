@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Создать @else Редактировать @endif
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')

                    <form method="POST" action="{{ url('/admin/groups/'.$group->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12 {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class="control-label">{{ 'Название' }}</label>
                            <input class="form-control" name="name" type="text" id="name" value="{{ $group->name or ''}}" required>
                            {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="col-md-12">

                            @foreach($permissions as $module => $items)
                                <div class="form-group">
                                    <h4 class="control-label">{{ $module }}</h4>
                                    <div class="checkbox">
                                        @foreach($items as $k => $item)
                                            <label class="label label-info" style="margin:0px 20px 0px 0px">
                                                <input type="checkbox" name="permissions[]" value="{{$item['id']}}"
                                                    @foreach($selected_permissions as $sp)
                                                        @if($sp->permission_id == $item['id']) {{'checked'}} @endif
                                                    @endforeach
                                                >
                                                {{$item['name']}}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/groups') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
