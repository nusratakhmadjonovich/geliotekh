@extends('layouts.admin')

@section('title') Страницы @stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/pages/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Создать
                    </a>
                </div>

                @include('includes.messages') @include('includes.error_messages')
                <div class="box-body">
                    <div class="search-box">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name_ru" placeholder="Название">
                            </div>
                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </form>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table" id="users-table">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Урл</th>
                                <th width="15%">Действие</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.pages.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'title_ru', name: 'title_ru'},
                {data: 'url', name: 'url'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection