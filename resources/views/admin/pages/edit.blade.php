@extends('layouts.admin')

@section('title') Страницы @stop
@section('title-child') @if($isNewRecord) Создать страница @else Редактировать @endif @stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ url('/admin/pages/'.$post->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-body">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12">
                            <h3>Урл: page/{{$post->id}}</h3>
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active {{$errors->has('title_ru') ? 'required' : '' }}"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                    <li class="{{$errors->has('title_ru') ? 'required' : '' }}"><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="{{ $errors->has('title_ru') ? 'has-error' : ''}}">
                                            <label for="title_ru" class="control-label required">{{ 'Название' }} </label>
                                            <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ old('title_ru', $post->title_ru)}}">
                                            {!! $errors->first('title_ru', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_ru') ? 'has-error' : ''}}">
                                            <label for="description_ru" class="control-label">{{ 'Описание' }}</label>
                                            <textarea name="description_ru" class="form-control">{{ old('description_ru', $post->description_ru) }}</textarea>
                                            {!! $errors->first('description_ru', '<p class="error-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="{{ $errors->has('title_en') ? 'has-error' : ''}}">
                                            <label for="title_en" class="control-label required">{{ 'Title' }}</label>
                                            <input class="form-control" name="title_en" type="text" id="title_en" value="{{ old('title_en', $post->title_en) }}">
                                            {!! $errors->first('title_en', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_en') ? 'has-error' : ''}}">
                                            <label for="description_en" class="control-label">{{ 'Description' }}</label>
                                            <textarea name="description_en" class="form-control">{{ old('description_en', $post->description_en) }}</textarea>
                                            {!! $errors->first('description_en', '<p class="error-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Сохранить </button>
                            <a href="{{ url('/admin/pages/') }}" class="btn btn-default pull-right"><span class="fa fa-times-circle"></span> Cancel</a>
                        </div>
                        <br>
                        <br>
                        <div class="form-group col-md-12 {{ $errors->has('publish_at') ? 'has-error' : ''}}">
                            <label for="publish_at" class="control-label">{{ 'Дата публикации' }}</label>
                            <div class="input-group">
                                <input type="text" id="publish_at" class="form-control datetimepicker" name="publish_at" value="{{ old('published_at', $post->published_at ? $post->published_at : date('Y-m-d H:i')) }}">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                            {!! $errors->first('publish_at', '<p class="error-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/full-all/ckeditor.js"></script>
    <script src="{{asset('backend/js/ckeditor_options')}}"></script>
    <script src="{{ asset('/backend/js/ckeditor_options.js') }}"></script>
    <script>
        CKEDITOR.replace('description_ru', options);
        CKEDITOR.replace('description_en', options);
    </script>
@stop