@extends('layouts.admin')

@section('title') Страницы # <?=$post->id?> @stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/pages') }}"><button class="btn btn-desfault btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/pages/'.$post->id.'/edit/') }}"><button class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редактировать</button></a>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th> Название </th>
                                    <td> {{ $post->title_ru }} </td>
                                </tr>
                                <tr>
                                    <th>Title</th>
                                    <td>{{ $post->title_en}}</td>
                                </tr>
                                <tr>
                                    <th>Описание</th>
                                    <td>{{ strip_tags($post->description_ru)}}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{{ strip_tags($post->description_en) }}</td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
