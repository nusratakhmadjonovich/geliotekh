@extends('layouts.admin')

@section('title')
    @if($isNewRecord) Добавить @else Редактировать @endif
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{url('/admin/company/'.$company->id)}}" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        @include('includes.messages') @include('includes.error_messages')
                       <div class="row">
                           <div class="col-md-8">
                               <div class="row">
                                   <div class="form-group col-md-12 {{ $errors->has('name') ? 'has-error' : ''}}">
                                       <label for="name" class="control-label">{{ 'Название' }}</label>
                                       <div class="input-group">
                                           <div class="input-group-addon"><i class="fa fa-list-alt"></i></div>
                                           <input class="form-control" name="name" type="text" id="name" value="{{ old('Название', $company->name) }}">
                                       </div>
                                       {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                                   </div>
                                   <div class="form-group col-md-12 {{ $errors->has('description') ? 'has-error' : ''}}">
                                       <textarea name="description" class="form-control">{{ old('description', $company->description) }}</textarea>
                                       {!! $errors->first('description', '<p class="error-block">:message</p>') !!}
                                   </div>

                                   <div class="form-group col-md-12">
                                       <label for="status" class="control-label">{{ 'Статус' }}</label>
                                       <div class="btn-group" data-toggle="buttons">
                                           <label class="btn btn-success btn-rounded {{ (old('status', $company->status) == 1 || $isNewRecord) ? 'active' : '' }} btn-md form-check-label">
                                               <input class="form-check-input" name="status"  value="1" type="radio" {{ (old('status', $company->status) == 1 || $isNewRecord) ? 'checked' : '' }}> Актив
                                           </label>
                                           <label class="btn btn-danger btn-rounded {{ old('status', $company->status) == 0 ? 'active' : '' }} btn-md form-check-label">
                                               <input class="form-check-input" name="status" value="0" type="radio" {{ old('status', $company->status) == 0 ? 'checked' : '' }}> Неактив
                                           </label>
                                       </div>
                                       {!! $errors->first('status', '<p class="error-block">:message</p>') !!}
                                   </div>
                                   <div class="col-md-12">
                                       <div class="form-group">
                                           <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                           <a href="{{ url('/admin/journal/editions') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-4">
                               <div class="row">
                                   <div class="form-group col-md-12" style="margin-top: 5px;">
                                       <br>
                                       <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                       <a href="{{ url('/admin/company') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                                   </div>
                                   <div class="form-group col-md-12 {{ $errors->has('ip_address') ? 'has-error' : ''}}">
                                       <label for="ip_address" class="control-label">{{ 'IP-адрес' }}</label>
                                       <div class="input-group">
                                           <div class="input-group-addon"><i class="fa fa-barcode"></i></div>
                                           <input type="text" id="ip_address" class="form-control" name="ip_address" value="{{ old('ip_address', $company->ip_address) }}" >
                                       </div>
                                       {!! $errors->first('start_date', '<p class="error-block">:message</p>') !!}
                                   </div>

                                   <div class="form-group col-md-12 {{ $errors->has('start_date') ? 'has-error' : ''}}">
                                       <label for="date_edition" class="control-label">{{ 'Дата начала' }}</label>
                                       <div class="input-group">
                                           <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                           <input type="text" id="start_date" class="form-control datepicker" name="start_date" value="{{ old('start_date', $company->start_date) }}" >
                                       </div>
                                       {!! $errors->first('start_date', '<p class="error-block">:message</p>') !!}
                                   </div>

                                   <div class="form-group col-md-12 {{ $errors->has('end_date') ? 'has-error' : ''}}">
                                       <label for="date_edition" class="control-label">{{ 'Дата окончания' }}</label>
                                       <div class="input-group">
                                           <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                           <input type="text" id="end_date" class="form-control datepicker" name="end_date" value="{{ old('end_date', $company->end_date) }}" >
                                       </div>
                                       {!! $errors->first('end_date', '<p class="error-block">:message</p>') !!}
                                   </div>
                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/full-all/ckeditor.js"></script>
    <script src="{{asset('backend/js/ckeditor_options')}}"></script>
    <script src="{{ asset('/backend/js/ckeditor_options.js') }}"></script>
    <script>
        CKEDITOR.replace('description', options);
    </script>
@stop