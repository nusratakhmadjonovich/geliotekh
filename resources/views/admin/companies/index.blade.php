@extends('layouts.admin')

@section('title')
    Компания
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/company/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Создать
                    </a>
                </div>

                @include('includes.messages') @include('includes.error_messages')
                <div class="box-body">
                    <div class="search-box">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                <select  class="form-control"  name="status">
                                    <option value=""></option>
                                    <option value="1">Актив</option>
                                    <option value="0">Неактив</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Название">
                            </div>
                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </form>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Статус</th>
                                    <th>Название</th>
                                    <th>IP-адрес</th>
                                    <th>Дата начала</th>
                                    <th>Дата окончания</th>
                                    <th>Действие</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.companies.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'status', name: 'status'},
                {data: 'name', name: 'name'},
                {data:'ip_address',name:'ip_address'},
                {data: 'start_date', name: 'start_date'},
                {data: 'end_date',name:'end_date'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@stop

@endsection
