@extends('layouts.admin')

@section('title') Блог @stop
@section('title-child') @if($isNewRecord) Создать категория @else Редактировать категория @endif @stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @include('includes.messages') @include('includes.error_messages')

                    <form method="POST" action="{{ url('/admin/blog/categories/'.$category->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="form-group col-md-12">
                            <label for="slug" class="control-label">{{ 'Slug' }}</label>
                            <input class="form-control" name="slug" type="text" id="slug" value="{{ $category->slug or ''}}">
                            {!! $errors->first('slug', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-12">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="{{ $errors->has('name_ru') ? 'has-error' : ''}}">
                                           <label for="name_ru" class="control-label">{{ 'Название' }}</label>
                                            <input class="form-control" name="name_ru" type="text" id="name_ru" value="{{ $category->name_ru or ''}}">
                                            {!! $errors->first('name_ru', '<p class="error-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="{{ $errors->has('name_ru') ? 'has-error' : ''}}">
                                            <label for="name_en" class="contro  l-label">{{ 'Title' }}</label>
                                            <input class="form-control" name="name_en" type="text" id="name_en" value="{{ $category->name_en or ''}}">
                                            {!! $errors->first('name_en', '<p class="error-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> &nbsp;Сохранить</button>
                                <a href="{{ url('/admin/blog/categories') }}" title="Back" class="btn btn-default"><span class="fa fa-times-circle"></span> Отменить</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection