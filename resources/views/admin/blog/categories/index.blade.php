@extends('layouts.admin')

@section('title') Блог @stop

@section('title-child') Категория @stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/blog/categories/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Создать
                    </a>
                </div>
                @include('includes.messages') @include('includes.error_messages')
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="users-table">
                            <thead>
                            <tr>
                                <th>Slug</th>
                                <th>Название</th>
                                <th>Name</th>
                                <th width="20">Действия</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

    @section('scripts')
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var oTable = $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                bLengthChange: false,
                bFilter: false,
                ajax: {
                    url: "{{  route('admin.blog.categories.list') }}",
                    method: 'GET',
                    data: function (d) {
                        var filter_data = $('#search-form').serializeArray();
                        $.each(filter_data, function(key, item) {
                            d[item.name] = item.value;
                        });
                    }
                },
                columns: [
                    {data: 'slug', name: 'slug'},
                    {data: 'name_ru', name: 'name_ru'},
                    {data: 'name_en', name: 'name_en'},
                    {data: 'actions', name: 'actions', orderable: false}
                ]
            });

            $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
            });
        </script>
    @stop

@endsection
