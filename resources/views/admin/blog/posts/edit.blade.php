@extends('layouts.admin')

@section('title') Блог @stop
@section('title-child') @if($isNewRecord) Создать пость @else Редактировать пость @endif @stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ url('/admin/blog/posts/'.$post->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-body">
                        @include('includes.messages') @include('includes.error_messages')
                        <div class="form-group col-md-12">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="{{ $errors->has('title_ru') ? 'has-error' : ''}}">
                                            <label for="title_ru" class="control-label required">{{ 'Название' }} </label>
                                            <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ old('title_ru', $post->title_ru)}}">
                                            {!! $errors->first('title_ru', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('anons_ru') ? 'has-error' : ''}}">
                                            <label for="anons_ru" class="control-label required">{{ 'Анонс' }} </label>
                                            <textarea name="anons_ru" class="form-control">{{ old('anons_ru', $post->anons_ru) }}</textarea>
                                            {!! $errors->first('anons_ru', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_ru') ? 'has-error' : ''}}">
                                            <label for="description_ru" class="control-label">{{ 'Описание' }}</label>
                                            <textarea name="description_ru" class="form-control">{{ old('description_ru', $post->description_ru) }}</textarea>
                                            {!! $errors->first('description_ru', '<p class="error-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="{{ $errors->has('title_en') ? 'has-error' : ''}}">
                                            <label for="title_en" class="control-label">{{ 'Title' }}</label>
                                            <input class="form-control" name="title_en" type="text" id="title_en" value="{{ old('title_en', $post->title_en) }}">
                                            {!! $errors->first('title_en', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('anons_en') ? 'has-error' : ''}}">
                                            <label for="anons_en" class="control-label">{{ 'Anons' }}</label>
                                            <textarea name="anons_en" class="form-control">{{ old('anons_en', $post->anons_en) }}</textarea>
                                            {!! $errors->first('anons_en', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_en') ? 'has-error' : ''}}">
                                            <label for="description_en" class="control-label">{{ 'Description' }}</label>
                                            <textarea name="description_en" class="form-control">{{ old('description_en', $post->description_en) }}</textarea>
                                            {!! $errors->first('description_en', '<p class="error-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Сохранить </button>
                            <a href="{{ url('/admin/blog/posts/') }}" class="btn btn-default pull-right"><span class="fa fa-times-circle"></span> Cancel</a>
                        </div>
                        <br>
                        <br>

                        <div class="form-group col-md-12 ">
                            <label for="status" class="control-label">{{ 'Опубликовать' }}</label>
                            <input type="hidden" name="status" value="0"/>
                            <input type="checkbox" name="status" id="status" class="js-switch" value="1" {{ $post->status === 1 || $isNewRecord ? 'checked' : '' }} />
                            {!! $errors->first('status', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('publish_at') ? 'has-error' : ''}}">
                            <label for="publish_at" class="control-label">{{ 'Дата публикации' }}</label>
                            <div class="input-group">
                                <input type="text" id="publish_at" class="form-control datetimepicker" name="publish_at" value="{{ old('published_at', $post->published_at ? $post->published_at : date('Y-m-d H:i')) }}">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                            {!! $errors->first('publish_at', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('category_id') ? 'has-error' : ''}}">
                            <label for="category_id" class="control-label">{{ 'Категория' }}</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $ky => $category)
                                    <option value="{{ $category->id }}" {{ $post->category_id === $category->id ? 'selected' : '' }}>{{ $category->name_ru }}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('category_id', '<p class="error-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('photo') ? 'has-error' : ''}}">
                            <label for="photo" class="control-label">Фото</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="icon-file glyphicon glyphicon-file"></i></div>
                                <input class="form-control" name="photo" type="file" id="photo">
                            </div>
                            {!! $errors->first('photo', '<p class="error-block">:message</p>') !!}
                            <div class="img-input-show">
                                @if($post->photo)
                                    <img src="{{ App\Libraries\UploadManager::getPhoto('blog_post', $post->id, 'original', $post->photo)}}" class="img-thumbnail" width="150"/>
                                    <a href="{{ url('admin/blog/posts/removePhoto/'.$post->id) }}" >удалить фото</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace('description_ru', options);
        CKEDITOR.replace('description_en', options);
    </script>
@stop