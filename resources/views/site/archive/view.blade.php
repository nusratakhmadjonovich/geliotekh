@extends('layouts.page')

@section('title')
    @lang('app.Edition') {!! $edition->title !!}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('archive_item', $edition) }}
@stop


@section('content_inner')
    <div class="row">
        <div class="col-md-4">
            <img src="{{ App\Libraries\UploadManager::getPhoto('journal_edition', $edition->id, 'original', $edition->photo)}}" title="{!! $edition->title !!}"/>
        </div>
        <div class="col-md-8">
            <div class="edition-view-top"></div>
            <div class="edition-view-desc">{!! $edition->description !!}</div>
        </div>
        <br><br>
        <div class="col-md-12"><h4>Рубрики журнала</h4></div>
        <ul>
            @foreach($categories as $category)
                <li>{{$category->name}}</li>
            @endforeach
        </ul>
        <br><br>
        <div class="col-md-12"><h4>@lang('app.Articles')</h4></div>
        @foreach($articles as $article)
            <div class="col-md-12">
                <p>@if($article->is_chargerable)<i class="fas fa-lock" title="@lang('app.locked')"></i>@endif <a href="{{lang_url('articles/'.$article->id)}}">{!! $article->title !!}</a></p>
                <div class="article-view-parametrs">
                    <p> <i class="fas fa-users"></i> {!! $article->authors !!}</p>
                    <p>  <i class="fas fa-file-alt"></i> {!! $article->edition->title !!} &nbsp;&nbsp; <i class="fas fa-folder-open"></i> {!! $article->category->name !!}</p>
                </div>
            </div>
        @endforeach

    </div>
@stop

@section('content_footer')
    <div class="pull-right">
        <!-- AddToAny BEGIN -->
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_telegram"></a>
            <a class="a2a_button_vk"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
        <!-- AddToAny END -->
    </div>
@stop