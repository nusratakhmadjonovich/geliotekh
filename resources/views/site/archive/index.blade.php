@extends('layouts.full_page')

@section('title')
    {{__('app.Archive')}}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('archive') }}
@stop

@section('content_inner')
    <div class="col-xl-3 col-lg-3 col-md-3">
        <div class="archive_search">
            <div class="col-md-12"><h4>{{__('app.Year')}}</h4></div>
            <div class="col-md-12"><a href="{{route('archive',['year'=>''])}}">{{__('app.All')}}</div>
            @foreach($years as $year)
                <div class="col-md-12"><a href="{{route('archive',['year'=>$year->year])}}">{{$year->year}}</div>
            @endforeach
        </div>
    </div>

    <div class="col-xl-9 col-lg-9 col-md-9">
        <div class="row">
            @foreach($editions as $edition)
                <div class="col-xl-4 col-lg-4 col-md-4 archive_journal_items">
                    <div class="archive_journal_items_img">
                        <a href="{{lang_url('archive/'.$edition->id)}}"><img src="{{ App\Libraries\UploadManager::getPhoto('journal_edition', $edition->id, 'original', $edition->photo)}}" title="{!! $edition->title !!}"/></a>
                    </div>
                    <div class="archive_journal_items_date">
                        <p><span>{{__('app.Date_edition')}}</span> {{date('d.m.Y', strtotime($edition->date_edition))}}</p>
                    </div>
                </div>
            @endforeach
            {{ $editions->links( "pagination::bootstrap-4") }}
        </div>
    </div>

@stop