@extends('layouts.page')

@section('title')
    {!! $article->title !!}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('article_item', $article) }}
@stop


@section('content_inner')

    <h3 class="article-view-title">{!! $article->title_ru !!}</h3>
    <p class="article-item-subtitle"> {!! $article->title_en !!}</p>
    <div class="article-view-parametrs">
        <p> <i class="fas fa-users"></i> {!! $article->authors !!}</p>
        <p> <i class="fas fa-folder-open"></i> {!! $article->category->name !!}  <i class="fas fa-file-alt"></i> {!! $article->edition->title !!}</p>
    </div>
    <div class="row">
        <div class="col-md-10 article-view-sitirovat">
            <p><i class="fa fa-quote-right"></i> Цитировать:</p>
            <p>{{$article->authors.' '. $article->title_ru}}</p>
        </div>
        <div class="col-md-2 article-view-file">
            <i class="far fa-file-pdf"></i>
        </div>
    </div>

    <div  class="article-view-abstract">
        <h6>АННОТАЦИЯ</h6>
        {!!  $article->description_ru !!}
    </div>

    <div  class="article-view-abstract">
        <h6>ABSTRACT</h6>
        {!!  $article->description_en !!}
    </div>

    <div class="article-view-keywords">
        Ключевые слова: {!!  $article->keywords_ru !!}
    </div>

    <div class="article-view-keywords">
        Keywords: {!!  $article->keywords_en !!}
    </div>
@stop

@section('content_footer')
    <div class="pull-right">
        <!-- AddToAny BEGIN -->
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_telegram"></a>
            <a class="a2a_button_vk"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
        <!-- AddToAny END -->
    </div>
@stop