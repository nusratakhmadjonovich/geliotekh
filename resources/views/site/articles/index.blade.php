@extends('layouts.full_page')

@section('title')
    {{__('app.Articles')}}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('articles') }}
@stop

@section('content_inner')
    <div class="col-xl-3 col-lg-3 col-md-3">
        <div class="archive_search">
            <form action="{{route('articles')}}" method="get">
                <div class="archive_year">
                    <select id="filter_edition" name="edition" class="form-control col-xl-8 col-lg-8 col-md-8">
                        <option value="">@lang('app.Edition')</option>
                        @foreach($editions as $edition)
                            <option value="{{$edition->id}}">{{$edition->year.' ('.$edition->number.')'}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="archive_rubric">
                    <select id="filter_category" name="category" class="form-control col-xl-12 col-lg-12 col-md-12">
                        <option value="">@lang('app.Rubric')</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="archive_article_name">
                    <input type="text" name="title" class="form-control" value="{{request('title')}}" placeholder="@lang('app.Name_of_article')">
                </div>
                <div class="archive_article_author">
                    <input type="text"  name="author" class="form-control" value="{{request('author')}}" placeholder="@lang('app.FIO_author')">
                </div>
                <button type="submit" class="btn btn-primary btn-block">@lang('app.Show_result')</button>
            </form>
        </div>
    </div>

    <div class="col-xl-9 col-lg-9 col-md-9">
        <div class="row">
            @foreach($articles as $article)
                <div class="col-md-12">
                    <p>@if($article->is_chargerable)<i class="fas fa-lock" title="@lang('app.locked')"></i>@endif <a href="{{lang_url('articles/'.$article->id)}}">{!! $article->title !!}</a></p>
                    <div class="article-view-parametrs">
                        <p> <i class="fas fa-users"></i> {!! $article->authors !!}</p>
                        <p>  <i class="fas fa-file-alt"></i> {!! $article->edition->title !!} &nbsp;&nbsp; <i class="fas fa-folder-open"></i> {!! $article->category->name !!}</p>
                    </div>
                </div>
            @endforeach
            {{ $articles->links( "pagination::bootstrap-4") }}
        </div>
    </div>

@stop

@section('scripts')
    <script>
    $(document).ready(function(){
        $('#filter_edition').select2();
        $('#filter_category').select2();
        $('#filter_edition').val({{request('edition')}}).change();
        $('#filter_category').val({{request('category')}}).change();
    });
    </script>
@stop