@extends('layouts.page')

@section('title')
    {{__('app.Events')}}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('events') }}
@stop

@section('content_inner')
    @include('site.events._list')
@stop