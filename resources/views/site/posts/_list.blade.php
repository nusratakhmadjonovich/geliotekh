@foreach ($posts as $post)
    <div class="post-item">
        <a href="{{lang_url('news/'.$post->id)}}">
            <img src="{{ App\Libraries\UploadManager::getPhoto('blog_post', $post->id, 'original', $post->photo)}}" class="article-thumb" title="{!! $post->title_ru !!}"/>
        </a>

        <a class="post-title" href="{{'news/'.$post->id}}">{!! $post->title !!} </a>
        <div class="post-item-text">
            {!! $post->anons !!}
        </div>

        <div class="post-date"><i class="fas fa-calendar-alt"></i> {{ date('H:i / Y-m-d', strtotime($post->publish_at)) }}</div>
    </div>
@endforeach

{{ $posts->links( "pagination::bootstrap-4") }}