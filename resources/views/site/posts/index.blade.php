@extends('layouts.page')

@section('title')
    {{__('app.News')}}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('news') }}
@stop

@section('content_inner')
    @include('site.posts._list')
@stop