@extends('layouts.page')

@section('title')
    {!! $post->title !!}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('news_item', $post) }}
@stop


@section('content_inner')
    {!!  $post->description !!}
@stop

@section('content_header')
    <i class="fas fa-calendar-alt"></i> {{ date('H:i / Y-m-d', strtotime($post->publish_at)) }}
@stop

@section('content_footer')
    <div class="pull-right">
        <!-- AddToAny BEGIN -->
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_telegram"></a>
            <a class="a2a_button_vk"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
        <!-- AddToAny END -->
    </div>
@stop