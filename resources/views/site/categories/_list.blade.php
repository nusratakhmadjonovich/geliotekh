@foreach ($categories as $category)
    <div class="col-md-4">
        <div class="tematika_item">
            <div class="col-xl-5 col-lg-5 col-md-12 tematika_img float-left">
                <a href="{{url('articles')}}"><img width="145" height="200" src="{{ App\Libraries\UploadManager::getPhoto('categories', $category->id, 'original', $category->photo)}}"/></a>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-12 tematika_text float-left">
                <a class="title" href="{{url('articles')}}">{{ $category->name_ru }}</a>
            </div>
        </div>
    </div>
@endforeach

{{ $categories->links( "pagination::bootstrap-4") }}