@extends('layouts.full_page')

@section('title')
    {{__('app.Thematic_coverage')}}
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('categories') }}
@stop

@section('content_inner')
    <div class="col-md-12">
        <div class="row">
            @include('site.categories._list')
        </div>
    </div>
@stop