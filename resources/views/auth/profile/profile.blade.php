@extends('layouts.page')

@section('title')
    @lang('auth.profile')
@stop

@section('breadcrumbs')

    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}" style="padding-left: 7px;">@lang('passwords.home')</a></li>
        <li style="padding-left: 7px;"> / @lang('auth.profile')</li>
    </ul>

@stop
@section('content_header')

@stop

@section('content_inner')

    <form action="{{ route('update.profile') }}" method="post" style="" enctype="multipart/form-data">
        {{ csrf_field() }}
        <br>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    @include('includes.messages')
                    @include('includes.error_messages')
                </div>
            </div>
        </div>
        <div class="form-group {{ $errors->has('fio') ? ' has-error' : '' }}" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <label for="fio"><strong>@lang('auth.fio')</strong></label>
                    <input type="text" name="fio"  value="{{ old('fio', $user->fio)}}" class="form-control">
                    {!! $errors->first('fio', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <label for="fio"><strong>@lang('auth.email')</strong></label>
                    <input type="email" name="email" value="{{ old('email', $user->email)}}" class="form-control">
                    {!! $errors->first('email', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('lang') ? ' has-error' : '' }}" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <label for="lang"><strong>@lang('auth.language')</strong></label>
                    <select name="lang" class="form-control">
                        <option value="ru" @if($user->lang == 'ru') selected @endif>RU</option>
                        <option value="en" @if($user->lang == 'en') selected @endif>EN</option>
                    </select>
                    {!! $errors->first('lang', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9 {{ $errors->has('photo') ? ' has-error' : '' }}">
                    <label for="fio"><strong>@lang('auth.photo')</strong></label>
                    <input type="file" name="photo" class="form-control">
                    {!! $errors->first('photo', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                        @if($user->photo)
                            <img src="{{ App\Libraries\UploadManager::getPhoto('profile', $user->id, 'original', $user->photo)}}" class="img-thumbnail" width="150"/>
                            <a href="{{ url('auth/profile/removePhoto/'.$user->id) }}" >@lang('auth.delete_photo')</a>
                        @endif
                </div>
            </div>
        </div>

        <div class="form-group" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-1x"></i> @lang('auth.save_button') </button>
                </div>
            </div>
        </div>
    </form>

@stop