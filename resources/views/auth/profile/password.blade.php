@extends('layouts.page')

@section('title')

    @lang('passwords.password_page_header')

@stop

@section('breadcrumbs')

    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}" style="padding-left: 7px;"> @lang('passwords.home') </a></li>
        <li  style="padding-left: 7px;"> / @lang('passwords.change_password') </li>
    </ul>

@stop


@section('content_header')

@stop

@section('content_inner')

    <form action="{{ route('update.password') }}" method="post" style="" enctype="multipart/form-data">
        {{ csrf_field() }}
        <br>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    @include('includes.messages')
                    @include('includes.error_messages')
                </div>
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <label for="password"><strong>@lang('passwords.current_password')</strong></label>
                    <input type="password" name="password"  class="form-control">
                    {!! $errors->first('password', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('new_password') ? ' has-error' : '' }}" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <label for="new_password"><strong>@lang('passwords.new_password')</strong></label>
                    <input type="password" name="new_password"  class="form-control">
                    {!! $errors->first('new_password', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('confirm_password') ? ' has-error' : '' }}" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <label for="confirm_password"><strong>@lang('passwords.confirm_password')</strong></label>
                    <input type="password" name="confirm_password"  class="form-control">
                    {!! $errors->first('confirm_password', '<p style="color:red;" class="error-block">:message</p>') !!}
                </div>
            </div>
        </div>


        <div class="form-group" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit fa-1x"></i> @lang('passwords.change_password') </button>
                </div>
            </div>
        </div>
    </form>

@stop