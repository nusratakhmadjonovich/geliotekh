@extends('layouts.auth')

@section('content')
    <div class="auth-header">
        <a href="{{url('/')}}"><img src="{{ asset('app/dist/img/logo.png')}}"></a>
    </div>
    <div class="auth-panel">
        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-12 control-label">@lang('auth.email')</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">
                        @lang('auth.send_reset_email')
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
