@extends('layouts.auth')

@section('content')
    <div class="auth-header">
        <a href="{{url('/')}}"><img src="{{ asset('app/dist/img/logo.png')}}"></a>
    </div>
    <div class="auth-panel">
        <form class="form-horizontal" method="POST" action="{{ route('post.login') }}">
            {{ csrf_field() }}
            @include('includes.messages')
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-12 control-label">@lang('auth.email')</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  autofocus>
                    {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-12 control-label">@lang('auth.password')</label>
                <div class="col-md-12">
                    <input id="password" type="password" class="form-control" name="password" >
                    {!! $errors->first('password', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('auth.remember_me')
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">
                        @lang('auth.login')
                    </button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-outline-secondary" href="{{ route('password.request') }}">
                                @lang('auth.forgot_password')
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-outline-secondary pull-right" href="{{ route('registration') }}">
                                @lang('auth.registration')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
