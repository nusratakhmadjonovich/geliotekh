@extends('layouts.auth')

@section('content')
    <div class="auth-header">
        <a href="{{url('/')}}"><img src="{{ asset('app/dist/img/logo.png')}}"></a>
    </div>
    <div class="auth-panel">
        <form class="form-horizontal" method="POST" action="{{ route('post.registration') }}">
            {{ csrf_field() }}
            @include('includes.messages')
            <div class="form-group{{ $errors->has('fio') ? ' has-error' : '' }}">
                <label for="fio" class="col-md-12 control-label required">@lang('auth.fio')</label>

                <div class="col-md-12">
                        <input id="fio" type="text" class="form-control" name="fio" value="{{ old('fio') }}">

                    {!! $errors->first('fio', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-12 control-label required">@lang('auth.email')</label>

                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                    {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-12 control-label required">@lang('auth.password')</label>

                <div class="col-md-12">
                    <input id="password" type="password" class="form-control" name="password" >

                    {!! $errors->first('password', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                <label for="password_confirmation" class="col-md-12 control-label required">@lang('auth.confirm_password')</label>

                <div class="col-md-12">
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" >
                </div>
            </div>
            <div class="form-group{{ $errors->has('terms_of_firm') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <div class="checkbox">
                        <label><input name="terms_of_firm" type="checkbox" value="1"> @lang('auth.terms_of_firm')</label>
                    </div>
                    {!! $errors->first('terms_of_firm', '<p class="error-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary  btn-block">
                        @lang('auth.registration')
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
