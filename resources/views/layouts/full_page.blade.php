@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="content_header_title">
            <div class="container">
                <h4>@yield('title')</h4>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @yield('breadcrumbs')
                </div>

                <div class="col-md-12">
                    @yield('content_header')
                </div>
            </div>

            <div class="row">
                @yield('content_inner')
            </div>

            <div class="col-md-12"  style="margin-top:15px;">
                @yield('content_footer')
            </div>
        </div>

        <div class="archive_breadcrumb">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="list-unstyled">
                        <li class="breadcrumb-item d-inline-block"><a href="#">Home</a></li>
                        <li class="breadcrumb-item d-inline-block" aria-current="page">Library</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@stop

