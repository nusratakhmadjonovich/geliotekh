<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('/backend/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/bower_components/select2/dist/css/select2.min.css') }}">
    <link href="{!! asset('/backend/css/datatables.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/backend/css/sweetAlert.css') }}">
    <!-- Blueimp Jquery File Upload -->
    <link href="{!! asset('/backend/css/jquery.fileupload.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/css/jquery.fileupload-ui.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/css/switchery.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('/backend/css/bootstrap-datetimepicker.min.css') !!}" rel="stylesheet" />

    @yield('styles')
</head>

<body  class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="index2.html" class="logo">
            <span class="logo-mini"><b>СУЖ</b></span>
            <span class="logo-lg"><b>СУЖ </b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            @if ($user->photo)
                                <img src="{{ App\Libraries\UploadManager::getPhoto('profile', $user->id, '32x32', $user->photo) }}" class="user-image" alt="User Image">
                            @else
                                <i class="fa fa-4 fa-user-o" style="color: #fff;"></i>
                            @endif
                            <span class="hidden-xs">{{ $user->fio }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                @if ($user->photo)
                                    <img src="{{ App\Libraries\UploadManager::getPhoto('profile', $user->id, '200x200', $user->photo) }}" class="img-circle" alt="User Image">
                                @else
                                    <i class="fa fa-4 fa-user-o" style="color: #fff; font-size: 7em;"></i>
                                @endif

                                <p>
                                    {{ $user->fio }}
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('admin/admins/' . \Illuminate\Support\Facades\Auth::user()->id . '/edit/') }}" class="btn btn-default btn-flat">{{ 'Профиль' }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{route('logout')}}" class="btn btn-default btn-flat">{{ 'Выйти' }}</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    @if ($user->photo)
                        <img src="{{ App\Libraries\UploadManager::getPhoto('profile', $user->id, '200x200', $user->photo) }}" class="img-circle" alt="User Image">
                    @else
                        <i class="fa fa-4 fa-user-o" style="color: #fff;  font-size: 3em;"></i>
                    @endif
                </div>
                <div class="pull-left info">
                    <p> {{ $user->fio}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <ul class="sidebar-menu" data-widget="tree">
                <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> <span>Админ панель</span></a></li>

                <li><a href="{{ url('/admin/company') }}"><i class="fa fa-list-alt"></i> <span>Компания</span></a></li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i> <span>Архив журнал</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('admin/journals') }}"><i class="fa fa-circle-o"></i>Журнал</a></li>
                        <li><a href="{{ url('admin/journal/editions') }}"><i class="fa fa-circle-o"></i>Выпуски</a></li>
                        <li><a href="{{ url('admin/journal/edition/articles') }}"><i class="fa fa-circle-o"></i>Статьи</a></li>
                        <li><a href="{{ url('admin/categories') }}"><i class="fa fa-circle-o"></i>Рубрики</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i> <span>Статьи</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('/admin/articles')}}"><i class="fa fa-circle-o"></i> <span>Статьи на проверку</span></a></li>
                        <li><a href="{{ url('/admin/admins') }}"><i class="fa fa-circle-o"></i> <span>На рецензию</span></a></li>
                        <li><a href="{{ url('/admin/admins') }}"><i class="fa fa-circle-o"></i> <span>На ред. коллег</span></a></li>
                    </ul>
                </li>
               {{-- <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Articles</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('/admin/articles/index')}}"><i class="fa fa-circle-o"></i>Users articles</a></li>
                        <li><a href="{{ route('admin.articles.rejected') }}"><i class="fa fa-circle-o"></i> Rejected articles</a></li>
                        <li><a href="{{ url('/admin/articles/accept') }}"><i class="fa fa-circle-o"></i> Accept articles</a></li>
                        <li><a href="{{ url('/admin/articles/all') }}"><i class="fa fa-circle-o"></i> All</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span>Article Info</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('/admin/article/meta/info') }}"><i class="fa fa-circle-o"></i>infoes</a></li>
                        <li><a href="{{ url('/admin/article/meta/info') }}"><i class="fa fa-circle-o"></i> extra menu </a></li>
                    </ul>
                </li>--}}
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cubes"></i> <span>CMS</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-comments"></i>Блог
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li><a href="{{ url('/admin/blog/categories')}}"><i class="fa fa-circle-o"></i>Категории</a></li>
                                <li><a href="{{ url('/admin/blog/posts') }}"><i class="fa fa-circle-o"></i>Посты</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('/admin/pages') }}"><i class="fa fa-circle-o"></i>Страницы</a></li>
                        <li><a href="{{ url('/admin/blocks') }}"><i class="fa fa-circle-o"></i>Статические блоки</a></li>
                        <li><a href="{{ url('admin/menu') }}"><i class="fa fa-circle-o"></i>Меню</a></li>
                        <li><a href="{{ url('/admin/filemanager') }}"><i class="fa fa-circle-o"></i>Файл менеджер</a></li>
                    </ul>
                </li>

                <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> <span>Пользователи</span></a></li>
                <li><a href="{{ url('/admin/admins') }}"><i class="fa fa-users"></i> <span>Администратор</span></a></li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cogs"></i> <span>Настройки</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('/admin/groups')}}"><i class="fa fa-circle-o"></i> Группы</a></li>
                        <li><a href="{{url('/admin/permissions')}}"><i class="fa fa-circle-o"></i> Доступы</a></li>
                        <li><a href="{{url('/admin/setting')}}"><i class="fa fa-circle-o"></i> Общий</a></li>
                    </ul>
                </li>
            </ul>
        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header content-center">
            <h1>@yield('title')
            <small>@yield('title-child')</small></h1>
        </section>

        <section class="content content-center">
            @yield('content')
        </section>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Max IT Journal platform</b> 1.0.
        </div>
        <strong>Copyright &copy; 2018.</strong>
    </footer>
</div>

<script src="{{ asset('/adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script> $.widget.bridge('uibutton', $.ui.button); </script>
<script src="{{ asset('/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('/adminlte/dist/js/adminlte.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('/backend/js/sweetAlert.js') }}"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{!! asset('/backend/js/app.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/backend/js/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/backend/js/bootstrap-datetimepicker.min.js') !!}"></script>

@yield('scripts')

<script>
    $(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.datetimepicker').datetimepicker({
            format: 'yyyy-mm-dd hh:ii'
        });

        $('.yearpicker').datepicker({
            autoclose: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });
    });
</script>

</body>
</html>