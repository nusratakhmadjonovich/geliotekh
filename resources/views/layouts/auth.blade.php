<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Journal && by FXasanov</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/app/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/app/dist/css/auth.css') }}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>

<body>
    @yield('content')
</div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="{{ asset('/app/dist/js/bootstrap.min.js') }}"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('app/assets/js/vendor/jquery-slim.min.js')}}"><\/script>')</script>
    <script src="{{ asset('/app/assets/js/vendor/popper.min.js') }}"></script>
    <script src="{{ asset('/app/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/app/assets/js/vendor/holder.min.js') }}"></script>
    @yield('scripts')
</body>
</html>