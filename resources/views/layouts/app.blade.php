<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | Гелиотехника</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/app/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('/app/dist/css/carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('/app/dist/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/app/dist/css/style_m.css') }}" rel="stylesheet">
    <link href="{{ asset('/app/dist/css/style_p.css') }}" rel="stylesheet">
    <link href="{{ asset('/app/dist/css/fontawesome-all.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
</head>

<body>
<div class="general">
    <div class="container">
        <header>
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-md-12 header_logo">
                    <a href="{{url('/')}}">
                        <img src="{{asset('/app/dist/img/logo.png')}}" alt="" title="">
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-12 header-right">
                    <div class="lang float-left">
                        <ul class="list-unstyled">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li class="d-inline">
                                    <a rel="alternate" class="@if(LaravelLocalization::getCurrentLocale() == $localeCode) {{'lang_active'}} @endif"
                                       hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="login float-right">
                        <ul class="list-unstyled">
                            @guest
                                <li class="d-inline">
                                    <a href="{{ route('registration') }}">Регистрация</a>
                                </li>
                                <li class="d-inline">
                                    <a href="{{ route('login') }}">Вход</a>
                                </li>
                                        @else
                                <li class="d-inline dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                        {{ \Illuminate\Support\Facades\Auth::user()->fio }} <span class="caret"></span>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ url('auth/profile') }}">{{ __('Profile') }}</a>
                                        <a class="dropdown-item" href="{{ url('auth/password') }}">{{ __('Change Password') }}</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ url('auth/logout') }}">{{__('Logout')}}</a>
                                    </div>
                                </li>
                             @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div class="top_menu">
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars_top"
                            aria-controls="navbars_top" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse row" id="navbars_top">
                        {!! $menu !!}
                    </div>
                </div>
            </nav>
        </div>

    </div>
    @yield('content')
    <footer>
        <div class="footer container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 footer-1">
                    {!!  \App\Libraries\BlockManager::get('footer_1') !!}
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 footer-2">
                    {!! \App\Libraries\BlockManager::get('footer_2') !!}
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 footer-2">
                    {!! \App\Libraries\BlockManager::get('footer_3') !!}
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 footer_academy_name footer-2">
                    <p class="footer_academy_name_text">
                        {!! \App\Libraries\BlockManager::get('footer_copyright') !!}
                    </p>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12 footer-2">
                    <p class="footer_academy_name_text">
                        {!! \App\Libraries\BlockManager::get('footer_bottom') !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="footer-3 col-xl-12">
            <a target="_blank" href="http://maxit.uz"><p>Powered by "Max IT"</p></a>
        </div>
    </footer>
</div>
</body>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="{{ asset('app/assets/js/vendor/jquery-slim.min.js')}}"><\/script>')</script>
<script src="{{ asset('/app/assets/js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('/app/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/app/assets/js/vendor/holder.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@yield('scripts')
<script>
    Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
    });
</script>
</html>