<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'password' => 'Пароль',
    'registration' => 'Регистрация',
    'remember_me' => 'Запомни меня',
    'login'=>'Авторизоваться',
    'email'=>'Электронной почта',
    'confirm_password'=>'Подтвердить Пароль',
    'forgot_password'=>'Забыли пароль ?',
    'fio'=>'Ф.И.О',
    'terms_of_firm'=>'Условия использования',
    'send_reset_email'=>'Сбросить текущий пароль',
    'confirmation_token_not_found'=>'Token not found.',
    'confirmation_provided'=>'Provided.',
    'photo'=>'Фото',
    'profile'=>'Профиль',
    'delete_photo'=>'удалить Фото',
    'download_photo'=>'Фото',
    'language'=>'Язык',
    'save_button'=>'сохранитъ',
];
