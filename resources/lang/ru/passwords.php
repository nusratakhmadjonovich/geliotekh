<?php
/**
 * Created by PhpStorm.
 * User: Dell-user
 * Date: 22.05.2018
 * Time: 23:10
 */


return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'password_page_header'=>'Изменитъ парол',
    'current_password'=>'Текущий пароль',
    'new_password'=>' Новая пароль',
    'confirm_password'=>'Подтвердить пароль',
    'change_password'=>'Изменитъ пароль',
    'home'=>'Главная',

];
