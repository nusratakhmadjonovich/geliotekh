<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'password' => 'Password',
    'registration' => 'Registration',
    'remember_me' => 'Remember me',
    'login'=>'Login',
    'email'=>'Email address',
    'confirm_password'=>'Confirm password',
    'forgot_password'=>'Forgot password ? ',
    'fio'=>'Full name',
    'terms_of_firm'=>'Term of firm',
    'photo'=>'Photo',
    'profile'=>'Profile',
    'delete_photo'=>'delete photo',
    'download_photo'=>'downloaded photo',
    'language'=>'Language',
    'save_button'=>'save',
];
