<?php
/**
 * Created by PhpStorm.
 * User: Dell-user
 * Date: 18.04.2018
 * Time: 23:47
 */

return [
    'login'=>'Login',
    'email'=>'Please enter your Email address',
    'password'=>'Password',
    'confirm_password'=>'Confirm Password',
    'remember_me'=>'Remember Me',
    'forgot_password'=>'Forgot Your Password?',
    'register'=>'Register',
    'fio'=>'FIO',
    'terms_of_firm'=>'Terms of Service',
    'data_message'=>'information was update successfully !',
    'password_message'=>'Your password was update successfully !',
    'error_password'=>'Current password does not match the original !'
];