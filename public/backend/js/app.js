$(document).ready(function () {
    // Switchery
    if (document.querySelector('.js-switch') != null) {
        new Switchery(document.querySelector('.js-switch'), {color: '#2281C3'});
    }
});