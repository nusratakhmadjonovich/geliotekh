<?php

use Illuminate\Database\Seeder;

class UsersGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\Models\Admin\UsersGroups();
        $data->group_id = 1;
        $data->user_id = 1;
        $data->timestamps = false;
        $data->save();

    }
}
