<?php

use Illuminate\Database\Seeder;

class GroupsPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 1;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 2;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 3;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 4;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 5;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 6;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 7;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 8;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 9;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 10;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 11;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 12;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 13;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 14;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 15;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 16;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 17;
        $gp->timestamps = false;
        $gp->save();

        $gp = new \App\Models\Admin\GroupsPermissions();
        $gp->group_id = 1;
        $gp->permission_id = 18;
        $gp->timestamps = false;
        $gp->save();

    }
}
