<?php

use Illuminate\Database\Seeder;

use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = "admin";
        $admin->description = "Admin";
        $admin->timestamps = false;
        $admin->save();

        $user = new Role();
        $user->name = "user";
        $user->description = "Member";
        $user->timestamps = false;
        $user->save();
    }
}
