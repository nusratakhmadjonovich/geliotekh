<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\Admin\Groups();
        $admin->name = "Super administrator";
        $admin->status = 1;
        $admin->save();
    }
}
