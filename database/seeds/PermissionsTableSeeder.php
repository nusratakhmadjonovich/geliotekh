<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Доступ";
        $permission->code = "access-administrator";
        $permission->module = "Администратор";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Создать";
        $permission->code = "create-administrator";
        $permission->module = "Администратор";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Удалить";
        $permission->code = "delete-administrator";
        $permission->module = "Администратор";
        $permission->timestamps = false;
        $permission->save();


        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Доступ";
        $permission->code = "access-user";
        $permission->module = "Пользователь";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Создать";
        $permission->code = "create-user";
        $permission->module = "Пользователь";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Удалить";
        $permission->code = "delete-user";
        $permission->module = "Пользователь";
        $permission->timestamps = false;
        $permission->save();


        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Доступ";
        $permission->code = "access-group";
        $permission->module = "Группа";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Создать";
        $permission->code = "create-group";
        $permission->module = "Группа";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Удалить";
        $permission->code = "delete-group";
        $permission->module = "Группа";
        $permission->timestamps = false;
        $permission->save();


        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Доступ";
        $permission->code = "access-journal";
        $permission->module = "Журнал";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Создать";
        $permission->code = "create-journal";
        $permission->module = "Журнал";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Удалить";
        $permission->code = "delete-journal";
        $permission->module = "Журнал";
        $permission->timestamps = false;
        $permission->save();


        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Доступ";
        $permission->code = "access-edition";
        $permission->module = "Выпуск";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Создать";
        $permission->code = "create-edition";
        $permission->module = "Выпуск";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Удалить";
        $permission->code = "delete-edition";
        $permission->module = "Выпуск";
        $permission->timestamps = false;
        $permission->save();


        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Доступ";
        $permission->code = "access-article";
        $permission->module = "Статья";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Создать";
        $permission->code = "create-article";
        $permission->module = "Статья";
        $permission->timestamps = false;
        $permission->save();

        $permission = new \App\Models\Admin\Permissions();
        $permission->name = "Удалить";
        $permission->code = "delete-article";
        $permission->module = "Статья";
        $permission->timestamps = false;
        $permission->save();
    }
}
