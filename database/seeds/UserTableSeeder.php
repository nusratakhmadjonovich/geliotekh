<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->fio = "Admin";
        $admin->email = "admin@gmail.com";
        $admin->password = bcrypt('admin');
        $admin->role_id = Role::ROLE_ADMIN;
        $admin->verify_token= "0";
        $admin->status = 1;
        $admin->save();

        $member = new  User();
        $member->fio = 'Member';
        $member->email = 'member@gmail.com';
        $member->password = bcrypt('member');
        $member->role_id = Role::ROLE_USER;
        $member->verify_token= "0";
        $member->status = 1;
        $member->save();
    }
}
