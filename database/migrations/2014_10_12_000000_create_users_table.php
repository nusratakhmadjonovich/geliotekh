<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('fio')->nullable();
            $table->string('photo')->nullable();
            $table->string('verify_token')->nullable();
            $table->tinyInteger('is_editor')->default("0");
            $table->tinyInteger('is_editors_chairman')->default("0");
            $table->string('editor_title')->nullable();
            $table->tinyInteger('is_reviewer')->default("0");
            $table->unsignedInteger('role_id')->default(2);
            $table->tinyInteger('status')->default(0);
            $table->rememberToken();
            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
