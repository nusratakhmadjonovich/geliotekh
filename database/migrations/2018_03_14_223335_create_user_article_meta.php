<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserArticleMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_article_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id');
            $table->string('file');
            $table->string('comment');
            $table->string('is_receive');
            $table->integer('is_deleted')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_article_meta');
    }
}
