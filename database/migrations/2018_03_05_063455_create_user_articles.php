<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->text('description');
            $table->integer('user_id');
            $table->tinyInteger('is_rejected');
            $table->timestamp('rejected_at');
            $table->integer('is_deleted')->default(0);
            $table->integer('status')->default(0);
            $table->string('path');
            $table->integer('is_accept')->default(0);
            $table->integer('accept_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_articles');
    }
}
