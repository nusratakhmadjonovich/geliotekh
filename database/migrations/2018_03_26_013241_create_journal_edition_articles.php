<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalEditionArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_edition_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('journal_edition_id');
            $table->integer('article_category_id');
            $table->text('title_ru');
            $table->text('title_en');
            $table->longText('description_ru')->nullable();
            $table->longText('description_en')->nullable();
            $table->string('file')->nullable();
            $table->string('photo')->nullable();
            $table->tinyInteger('is_chargerable')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_edition_articles');
    }
}
