<?php

// Home > News
Breadcrumbs::register('news', function ($breadcrumbs) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.News'), url('news'));
});

// Home > News > [Post]
Breadcrumbs::register('news_item', function ($breadcrumbs, $post) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.News'), url('news'));
    $breadcrumbs->push($post->title, route('news', $post));
});

// Home > Events
Breadcrumbs::register('events', function ($breadcrumbs) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Events'), url('events'));
});

// Home > Events > [Post]
Breadcrumbs::register('events_item', function ($breadcrumbs, $post) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Events'), url('events'));
    $breadcrumbs->push($post->title, route('events', $post));
});

// Home > Categories
Breadcrumbs::register('categories', function ($breadcrumbs) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Thematic_coverage'));
});

// Home > Archive
Breadcrumbs::register('archive', function ($breadcrumbs) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Archive'));
});

// Home > Archive > Item
Breadcrumbs::register('archive_item', function ($breadcrumbs, $edition) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Archive'), route('archive'));
    $breadcrumbs->push($edition->title);
});


// Home > Articles
Breadcrumbs::register('articles', function ($breadcrumbs) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Articles'));
});

// Home > Articles > Item
Breadcrumbs::register('article_item', function ($breadcrumbs, $article) {
    $breadcrumbs->push(__('app.Home'), url('/'));
    $breadcrumbs->push(__('app.Articles'));
    $breadcrumbs->push($article->title);
});