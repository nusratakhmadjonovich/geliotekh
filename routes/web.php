<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' =>LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function() {
    //Main
    Route::get('/',['uses'=>'AppController@index', 'as'=>'index']);
    Route::get('publish',['uses'=>'Site\PublishController@index', 'as'=>'publish']);
    Route::get('page/{id}',['uses'=>'Site\PageController@index', 'as'=>'page']);

    Route::get('news', ['uses'=>'Site\NewsController@index', 'as'=>'news']);
    Route::get('news/{id}', 'Site\NewsController@view');

    Route::get('categories', ['uses'=>'Site\CategoriesController@index', 'as'=>'categories']);

    Route::get('archive',['uses'=>'Site\ArchiveController@index', 'as'=>'archive']);
    Route::get('archive/{id}', 'Site\ArchiveController@view');

    Route::get('articles', ['uses'=>'Site\ArticlesController@index', 'as'=>'articles']);
    Route::get('articles/{id}', 'Site\ArticlesController@view');

    Route::get('events', ['uses'=>'Site\EventsController@index', 'as'=>'events']);
    Route::get('events/{id}', 'Site\EventsController@view');

    Route::get('search', 'Site\SearchController@index');

    //Auth Routes
    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::group(['middleware' => 'guest'], function () {
            //Login
            Route::get('/login', ['uses' => 'LoginController@showLogin', 'as' => 'login']);
            Route::post('/login', ['uses' => 'LoginController@login', 'as' => 'post.login']);

            //Registration
            Route::get('registration', ['uses' => 'RegisterController@showRegistration', 'as' => 'registration']);
            Route::post('registration', ['uses' => 'RegisterController@register', 'as' => 'post.registration']);

            // Confirmation Routes...
            Route::get('confirm/{token}', 'RegisterController@confirm')->name('confirm');

            // Password Reset Routes...
            Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
            Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
            Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
            Route::post('password/reset', 'ResetPasswordController@reset');
        });

        Route::get('/profile',['uses'=>'ProfileController@profile','as'=>'profile']);
        Route::post('/profile',['uses'=>'ProfileController@update_profile','as'=>'update.profile']);
        Route::get('/profile/removePhoto/{id}',['uses'=>'ProfileController@removePhoto','as'=>'removePhoto']);
        Route::get('/password',['uses'=>'ProfileController@change_password','as'=>'change.password']);
        Route::post('/password',['uses'=>'ProfileController@update_password','as'=>'update.password']);
        Route::get('/logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });
});

//Member routes
Route::group(['middleware'=>['auth', 'user'], 'namespace' => 'Member', 'prefix' => 'member', 'as' => 'member.'], function () {
    Route::get('/create/article',['uses'=>'ArticlesController@articles','as'=>'create.article']);
    Route::post('/user/articles',['uses'=>'ArticlesController@saveFile','as'=>'user.add.articles']);
    Route::get('/user/article/history',['uses'=>'ArticlesController@history','as'=>'user.article.history']);
    Route::get('/user/article/read/{id}',['uses'=>'ArticlesController@read','as'=>'user.article.read']);
    Route::get('/user/article/send/{id}',['uses'=>'ArticlesController@send','as'=>'user.article.send']);
    Route::post('/user/article/send/{id}/file',['uses'=>'ArticlesController@send_file','as'=>'user.article.file.send']);
    /*user reviever*/
    Route::get('/user/article/reviewer',['uses'=>'AppController@user_show_articles','as'=>'user.article.reviewer']);
    Route::get('/reviewer/article/meta',['uses'=>'AppController@user_articles_index','as'=>'reviewer.article.meta']);
    Route::post('/admin/reviewer/user/{user_id}/article/{article_meta_id}',['uses'=>'AppController@user_reviewer_ideas','as'=>'reviewer.user.ideas']);
    Route::get('/user/article/reviewer/{id}',['uses'=>'AppController@list_group_item','as'=>'list.group.article.meta']);
    /*user reviever*/

    Route::group(['middleware'=>'user'],function (){

        Route::get('/user',[
            'uses'=>'UserController@getUser',
            'as'=>'get.user'
        ]);

        Route::get('/articles',['uses'=>'UserController@getArticles', 'as'=>'get.articles' ]);
        Route::post('/saveFile',[ 'uses' => 'ArticlesController@saveFile', 'as' => 'saveFile' ]);
        Route::get('/showArticles',['uses'=>'ArticlesController@getShowArticles', 'as'=>'get.show.articles' ]);
        Route::get('/languages',['uses'=>'UserController@getLanguages','as'=>'get.languages']);
    });

    Route::group(['middleware'=>'author'],function(){
        Route::get('/author',['uses'=>'AuthorController@getAuthor', 'as'=>'get.author']);
    });

    Route::group(['middleware'=>'publisher'],function (){
        Route::get('/publisher',[ 'uses'=>'PublisherController@getPublisher',  'as'=>'get.publisher' ]);
        Route::get('/showUserPublisher',['uses'=>'PublisherController@getShowArticlePuslisher', 'as'=>'get.showArticlePuslisher']);
    });
});

//Admin routes
Route::group(['middleware'=>['auth', 'admin']], function () {
    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
});

Route::group(['middleware'=>['auth', 'admin'],'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function (){
    Route::get('/companies_list',['uses'=>'CompanyController@get_list','as'=>'companies.list']);
    Route::resource('/company','CompanyController');
    Route::get('',['uses'=>'AdminController@getDashboard', 'as'=>'get.admin']);
    Route::get('/articles',['uses'=>'UserArticlesController@index','as'=>'articles.index']);
    Route::get('/articles/list',['uses'=>'UserArticlesController@get_list','as'=>'articles.list']);
    Route::get('/articles/rejected',['uses'=>'UserArticlesController@rejected','as'=>'articles.rejected']);
    Route::get('/articles.rejected.list',['uses'=>'UserArticlesController@rejected_list','as'=>'articles.rejected.list']);
    Route::post('/users/articles/rejected/{id}',['uses'=>'UserArticlesController@article_rejected','as'=>'users.articles.rejected']);
    Route::get('/users/articles/reject/{id}',['uses'=>'UserArticlesController@reject_show','as'=>'reject.show']);
    Route::get('/users/articles/print/{id}',['uses'=>'UserArticlesController@send_print','as'=>'users.articles.print.send']);
    Route::delete('/users/articles/destroy/{id}',['uses'=>'UserArticlesController@destroy','as'=>'articles.destroy']);
    Route::get('/articles/accept',['uses'=>'UserArticlesController@accept_articles','as'=>'accept.articles']);
    Route::get('/articles/accept/list',['uses'=>'UserArticlesController@accept_gel_list','as'=>'articles.accept.list']);
    Route::get('/users/articles/accept/{id}',['uses'=>'UserArticlesController@accept_show','as'=>'article.accept.show']);
    Route::get('/articles/all',['uses'=>'UserArticlesController@all_atricles','as'=>'articles.all']);
    Route::get('/users/articles/all/{id}',['uses'=>'UserArticlesController@all_show','as'=>'articles.all.show']);
    Route::get('/articles/all/list',['uses'=>'UserArticlesController@all_articles_get_list','as'=>'articles.all.list']);
    Route::get('/review/articles/{id}/edit',['uses'=>'ReviewerController@edit','as'=>'reviewer.article.edit']);

    Route::get('/users/articles/{id}',['uses'=>'UserArticlesController@show','as'=>'users.articles']);
    Route::post('/users/articles/destroy/{id}',['uses'=>'UserArticlesController@destroy','as'=>'articles.destroy']);

    // Mobule: Article categories
    Route::get('/categories/removeFile/{id}',['uses'=>'ArticleCategoriesController@removeFile','as'=>'get.remove.file']);
    Route::get('/categories/list', ['uses'=>'ArticleCategoriesController@get_list', 'as'=>'categories.list']);
    Route::resource('/categories', 'ArticleCategoriesController');
    // Mobule: Journal
    Route::get('/journals/list',['uses'=>'JournalsController@get_list','as'=>'journals.list']);
    Route::resource('/journals','JournalsController');
    Route::get('/journal/editions/list',['uses'=>'JournalEditionsController@get_list','as'=>'journal.edition.list']);
    Route::resource('/journal/editions','JournalEditionsController');
    Route::get('/journal/editions/removeFile/{id}',['uses'=>'JournalEditionsController@removeFile','as'=>'get.remove.file']);
    Route::get('/journal/editions/removeDoc/{id}',['uses'=>'JournalEditionsController@removeDoc','as'=>'get.remove.doc']);
    Route::get('/journal/edition/articles/list',['uses'=>'JournalEditionArticlesController@get_list','as'=>'journal.edition.articles.list']);
    Route::resource('/journal/edition/articles','JournalEditionArticlesController');
    Route::get('/journal/edition/articles/removeFile/{id}',['uses'=>'JournalEditionArticlesController@removeFile','as'=>'get.remove.edition.file']);
    Route::get('/journal/edition/articles/removePhoto/{id}',['uses'=>'JournalEditionArticlesController@removePhoto','as'=>'get.remove.edition.photo']);

    /*reviewrs articles*/
    Route::get('/reviewers/index/get/list',['uses'=>'ReviewerController@get_list','as'=>'reviewers.index.get.list']);
    /*reviewers articles*/
    Route::get('/reviewers/index/get/list',['uses'=>'ReviewerController@get_list','as'=>'admin.reviewers.index.get.list']);
    Route::resource('/reviewers','ReviewerController');
    Route::get('/send/revievwer/{id}',['uses'=>'ReviewerController@send','as'=>'send.reviewer']);
    Route::patch('/reviewers/{id}/update',['uses'=>'ReviewerController@patch_update','as'=>'patch.update']);
    /*reviewer articles*/
    /*user reviever*/
    Route::get('/user/article/reviewer',['uses'=>'ReviewerController@user_show_articles','as'=>'user.article.reviewer']);
    Route::get('/reviewer/article/meta',['uses'=>'ReviewerController@user_articles_index','as'=>'reviewer.article.meta']);
    Route::post('/reviewer/user/{user_id}/article/{article_meta_id}',['uses'=>'ReviewerController@user_reviewer_ideas','as'=>'reviewer.user.ideas']);
    /*user reviever*/
    Route::get('/article/meta/info',['uses'=>'ReviewerController@info_index','as'=>'article.info.index']);
    Route::get('/article/meta/info/{id}',['uses'=>'ReviewerController@info_show','as'=>'article.info.show']);
    /*article info*/

    Route::get('admin/info/articles',['uses'=>'ReviewAdminController@info_article','as'=>'info.article']);
    Route::get('/admin/article/info',['ReviewAdminController@get_info','as'=>'get.info']);
    /*article info*/

    //Route::get('/admin/article/meta/info/menu',['uses'=>'ReviewerController@menu','as'=>'menu']);

    Route::get('/journal/edition/articles/list',['uses'=>'JournalEditionArticlesController@get_list','as'=>'journal.edition.articles.list']);
    Route::resource('admin/review/articles','ReviewAdminController');

    Route::resource('journal/categories','ArticleCategories');

    // Module: CMS
    //blog
    Route::group(['prefix' => 'blog', 'as' => 'blog.', 'namespace' => 'Blog'], function () {
        // Categories
        Route::get('categories/list', ['uses'=>'CategoriesController@get_list','as'=>'categories.list']);
        Route::resource('categories', 'CategoriesController');
        // Posts
        Route::get('posts/list', ['uses'=>'PostsController@get_list','as'=>'posts.list']);
        Route::resource('posts', 'PostsController');
        Route::any('posts/upload/{id?}', 'PostsController@upload')->name('posts.upload');
    });
    // pages
    Route::get('/pageslist', ['uses'=>'PagesController@get_list', 'as'=>'pages.list']);
    Route::resource('pages', 'PagesController');
    //menu
    Route::get('menu', array('as' => 'menus', 'uses'=>'MenuController@index'));
    Route::post('/menu/addcustommenu', array('as' => 'menu.addcustommenu', 'uses' => 'MenuController@addcustommenu'));
    Route::post('/menu/deleteitemmenu', array('as' => 'menu.deleteitemmenu', 'uses' => 'MenuController@deleteitemmenu'));
    Route::post('/menu/deletemenug', array('as' => 'menu.deletemenug', 'uses' => 'MenuController@deletemenug'));
    Route::post('/menu/createnewmenu', array('as' => 'menu.createnewmenu', 'uses' => 'MenuController@createnewmenu'));
    Route::post('/menu/generatemenucontrol', array('as' => 'menu.generatemenucontrol', 'uses' => 'MenuController@generatemenucontrol'));
    Route::post('/menu/updateitem', array('as' => 'menu.updateitem', 'uses' => 'MenuController@updateitem'));

    // Static blocks
    Route::get('/blocks/list', ['uses'=>'BlocksController@get_list', 'as'=>'blocks.list']);
    Route::resource('/blocks', 'BlocksController');


    //MODULE: USERS
    Route::get('/users/removePhoto/{id}', ['uses'=>'UsersController@removePhoto', 'as'=>'users.removePhoto']);
    Route::get('/users/list', ['uses'=>'UsersController@get_list', 'as'=>'users.list']);
    Route::resource('/users', 'UsersController');

    //MODULE: ADMINISTRATORS
    Route::get('/admins/removePhoto/{id}', ['uses'=>'AdministratorsController@removePhoto', 'as'=>'admins.removePhoto']);
    Route::get('/admins/list', ['uses'=>'AdministratorsController@get_list', 'as'=>'admins.list']);
    Route::resource('/admins', 'AdministratorsController');

    // Module: SETTINGS
    // User Groups
    Route::get('/groups/list', ['uses'=>'GroupsController@get_list', 'as'=>'groups.list']);
    Route::resource('/groups', 'GroupsController');
    // User permissions
    Route::get('/permissions/list', ['uses'=>'PermissionsController@get_list', 'as'=>'permissions.list']);
    Route::resource('/permissions', 'PermissionsController');
    //general settings
    Route::get('/setting_list',['uses'=>'SettingController@get_list','as'=>'setting.list']);
    Route::resource('/setting','SettingController');

});
